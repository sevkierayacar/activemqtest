﻿using System;
using Common;
using FortyTwo.CF.Core.Messages;

namespace TestProducer
{
    public class CreadorImplementation : ICreadorInterface
    {
        public CreadorObjectTwo ConvertToObjectTwo(TestObjectOne objectOne)
        {
            return new CreadorObjectTwo()
            {
                TestIntTwo = objectOne.TestIntOne,
                TestStringTwo = objectOne.CreadorStringOne
            };
        }

        public void Test(string test)
        {
            Console.WriteLine($"Received: {test}");
        }

        public event EventHandler<string> OnMessage;

        public event EventHandler<Request> OnRequest;

        public event EventHandler<Response> OnResponse;
        public ResponseObj MakeRequest(RequestObj obj)
        {
            return new ResponseObj
            {
                Id = obj.Age,
                Result = obj.Name + " from producer"
            };
        }

        public void SendMessage(string message)
        {
            OnMessage?.Invoke(this, message);
        }

        public void SendRequest(Request request)
        {
            OnRequest?.Invoke(this, request);
        }

        public void SendResponse(Response response)
        {
            OnResponse?.Invoke(this, response);
        }

        public void RaiseEvent(object sender, string e)
        {
            try
            {
                OnMessage(sender, e);
                // throw new Exception("adfasd");
                Console.WriteLine("Event raised");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
        public void InvoqueRequest(object sender, Request e)
        {
            try
            {
                OnRequest(sender, e);

                Console.WriteLine("Request Invoqued");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
        public void InvoqueResponse(object sender, Response e)
        {
            try
            {
                OnResponse(sender, e);

                Console.WriteLine("Response Invoqued");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
