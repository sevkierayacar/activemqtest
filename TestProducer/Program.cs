﻿using Castle.Facilities.Startable;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Common;
using FortyTwo.CastleIntegration;
using FortyTwo.CF.Core.Encoders;
using FortyTwo.CF.Encoders.Protobuf;
using FortyTwo.CF.Infrastructure.ActiveMQ;
using FortyTwo.CF.Infrastructure.ActiveMQ.CastleIntegration;
using System;
using FortyTwo.CF.Core.ChannelFactory.RpcProxies;
using FortyTwo.CF.Core.Communication;
using FortyTwo.CF.Core.Messages;
using FortyTwo.CF.Encoders.Json;

namespace TestProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var container = new WindsorContainer())
            {
                container.AddFacility<StartableFacility>(facility => facility.DeferredStart());
                container.AddFacility<CommunicationFrameworkFacility>();
                container.Install(new ActiveMqChannelInstaller());
                container.Register(
                    Component.For<IConnectionListener>().ImplementedBy<ConnectionListener>(),
                    Component.For<IMessagesEncoder>().ImplementedBy<JsonEncoder>().Named("json"),
                   //Component.For<IMessagesEncoder>().ImplementedBy<ProtoBufMessagesEncoder>().Named("proto"),
                    Component.For<ICreadorInterface>().ImplementedBy<CreadorImplementation>().AsHost(
                        new HostConfiguration
                        {
                            BrokerUri = "failover:(tcp://10.55.0.130:61616)",
                            FileServerUrl = "http://10.55.0.130:8161/fileserver/",
                            Encoder = "json",
                        })
                );
                Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Host(producer) Started..");
                //Console.ReadKey();
                //((CreadorImplementation)container.Resolve<ICreadorInterface>()).RaiseEvent(null, "GetInstrumentInfoes");

                //Console.ReadKey();

                //((CreadorImplementation)container.Resolve<ICreadorInterface>()).InvoqueRequest(null, new Request
                //{
                //    ActionName = "GetInstrumentInfoes",
                //    CorrelationId = Guid.NewGuid().ToString(),
                //    TimeStamp = DateTime.Now,
                //    ActionParameters = new object[0]
                //});

                 //Console.ReadKey();


                //((CreadorImplementation)container.Resolve<ICreadorInterface>()).InvoqueResponse(null, new Response
                //{
                //    Success = true,
                //    CorrelationId = "123",
                //    ErrorMessage = "Nothing",
                //    TimeStamp = DateTime.Now,
                //    Payload = "lol"
                //});

                Console.ReadKey();

                //var messageChannel = container.Resolve<IMessageChannel>();
                //messageChannel.OnRequest += InvoqueRequest;



























                //Console.ReadKey();


                //var messageEncode = (ProtoBufMessagesEncoder)container.Resolve(typeof(IMessagesEncoder));
                //var messageBuffer = messageEncode.Encode(new Request
                //{
                //    ActionName = "GetInstrumentInfoes",
                //    CorrelationId = Guid.NewGuid().ToString(),
                //    TimeStamp = DateTime.Now,
                //    ActionParameters = new object[0]
                //});
                //var encodedMessage = new BytesMessage(messageBuffer);
                //var binaryChannel = new AmqInOutBinaryChannel(
                //    "failover:(tcp://10.55.0.130:61616)", 
                //    "http://10.55.0.130:8161/fileserver/",
                //    "300_TRADING_PLATFORM_RESPONSES", 
                //    "300_TRADING_PLATFORM_REQUESTS");
                //binaryChannel.Start();

                //binaryChannel.SendMessage(encodedMessage);
                //binaryChannel.Stop();


                //var messageEncode = (ProtoBufMessagesEncoder)container.Resolve(typeof(IMessagesEncoder));

                //var channel = ((MessageChannel)container.Resolve<IMessageChannel>());
                //channel.
                //    ((MessageChannel)container.Resolve<IMessageChannel>()).SendMessage(new Request
                //    {
                //        ActionName = "GetInstrumentInfoes",
                //        ActionParameters = new object[0],
                //        CorrelationId = Guid.NewGuid().ToString(),
                //        TimeStamp = DateTime.Now
                //    });


                //((CreadorImplementation)container.Resolve<ICreadorInterface>()).SendMessage("GetInstrumentInfoes");


                //Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Host(producer) Hello sent!!!!!!!!!");

                //Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Host(producer) Stopping...");

            }

        }

    }
}
