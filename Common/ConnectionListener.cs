﻿using FortyTwo.CF.Infrastructure.ActiveMQ;
using System;

namespace Common
{
    public class ConnectionListener : IConnectionListener
    {
        public void OnError(string message, Exception e)
        {
            Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Error {message}. Exception: {e}");
        }

        public void OnInfo(string message)
        {
            Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Info {message}");
        }
    }
}
