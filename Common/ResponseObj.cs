﻿using System;

namespace Common
{
    [Serializable]
    public class ResponseObj
    {
        public string Result { get; set; }
        public int Id { get; set; }
    }
}
