﻿using System;

namespace Common
{
    [Serializable]
    public class RequestObj
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
