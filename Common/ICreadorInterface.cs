﻿using System;
using System.Threading.Tasks;
using FortyTwo.CF.Core.Messages;

namespace Common
{
    public interface ICreadorInterface
    {
        CreadorObjectTwo ConvertToObjectTwo(TestObjectOne objectOne);

        void Test(string test);

        event EventHandler<string> OnMessage;

        event EventHandler<Request> OnRequest;

        event EventHandler<Response> OnResponse;

        ResponseObj MakeRequest(RequestObj obj);
    }
}
