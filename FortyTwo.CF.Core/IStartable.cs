﻿namespace FortyTwo.CF.Core
{
    public interface IStartable
    {
        void Start();
    }
}
