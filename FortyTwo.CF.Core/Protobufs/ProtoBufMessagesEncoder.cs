﻿using FortyTwo.CF.Core.Encoders;
using FortyTwo.CF.Core.Encoders.Exceptions;
using FortyTwo.CF.Core.Messages;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf
{
    public sealed class ProtoBufMessagesEncoder : MessagesEncoder
    {
        private readonly DataContractEncoderFactory _encoderFactory;

        public ProtoBufMessagesEncoder()
        {
            _encoderFactory = new DataContractEncoderFactory();
        }

        protected override byte[] EncodeSingle(MessageBase message)
        {
            if (message is Request)
            {
                return EncodeRequest(message as Request);
            }
            
            if (message is Response)
            {
                return EncodeResponse(message as Response);
            }
            
            if (message is Event)
            {
                return EncodeEvent(message as Event);
            }
            
            throw new InvalidCastException(string.Format("Unknown message type in EncodeSingle {0}", message.GetType().Name));
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        private byte[] EncodeEvent(Event eventmessage)
        {
            if (eventmessage.EventType != "OnQuote")
            {
                
            }
            var protoEventBuilder = Messages.Event.CreateBuilder();

            protoEventBuilder.SetEventType(eventmessage.EventType);
            protoEventBuilder.SetTimeStamp(DateUtils.ToJavaTimeUtc(eventmessage.TimeStamp));

            if (eventmessage.Payload != null)
            {
                var payloadType = eventmessage.Payload.GetType();
                var encoder = _encoderFactory.GetEncoderFor(payloadType);
                var payload = encoder.EncodeDataContract(eventmessage.Payload);

                protoEventBuilder.SetPayload(payload);
            }

            var protoEvent = protoEventBuilder.Build();
            var encodingBuffer = new byte[protoEvent.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var encodingStream = new MemoryStream(encodingBuffer);

            encodingStream.WriteMessageType((long)Messages.EnMessageTypes.EVENT);
            protoEvent.WriteTo(encodingStream);

            return encodingBuffer;
        }

        private byte[] EncodeResponse(Response message)
        {
            var protoResponseBuilder = Messages.Response.CreateBuilder();

            protoResponseBuilder.SetTimeStamp(DateUtils.ToJavaTimeUtc(message.TimeStamp));
            protoResponseBuilder.SetCorrelationId(message.CorrelationId);
            protoResponseBuilder.SetErrorMessage(message.ErrorMessage ?? String.Empty);
            protoResponseBuilder.SetSuccess(message.Success);

            if (message.Payload != null)
            {
                protoResponseBuilder.SetPayload(
                    _encoderFactory.GetEncoderFor(message.Payload.GetType()).EncodeDataContract(message.Payload));
            }

            var protoResponse = protoResponseBuilder.Build();
            var encodingBuffer = new byte[protoResponse.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var encodingStream = new MemoryStream(encodingBuffer);

            encodingStream.WriteMessageType((long)Messages.EnMessageTypes.RESPONSE);
            protoResponse.WriteTo(encodingStream);

            return encodingBuffer;
        }

        private byte[] EncodeRequest(Request message)
        {
            var protoRequestBuilder = Messages.Request.CreateBuilder();

            protoRequestBuilder.SetTimeStamp(DateUtils.ToJavaTimeUtc(message.TimeStamp));
            protoRequestBuilder.SetActionName(message.ActionName);
            protoRequestBuilder.SetCorrelationId(message.CorrelationId);

            foreach (var actionParam in message.ActionParameters)
            {
                var encoder = _encoderFactory.GetEncoderFor(actionParam.GetType());
                var encodedActionParam = encoder.EncodeDataContract(actionParam);

                protoRequestBuilder.AddActionParameters(encodedActionParam);
            }
            
            var protoRequest = protoRequestBuilder.Build();
            var encodingBuffer = new byte[protoRequest.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var encodingStream = new MemoryStream(encodingBuffer);

            encodingStream.WriteMessageType((long)Messages.EnMessageTypes.REQUEST);
            protoRequest.WriteTo(encodingStream);

            return encodingBuffer;
        }

        public static Byte ReadDataContractType2(MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadByte();
        }

        protected override MessageBase DecodeSingle(byte[] messagebuffer, int index, int count)
        {
            var memStream = new MemoryStream(messagebuffer, index, count);
            var messageType = (Messages.EnMessageTypes)ReadDataContractType2(memStream);
            var cis = CodedInputStream.CreateInstance(memStream);
            cis.SetSizeLimit(100 * 1024 * 1024);

            switch (messageType)
            {
                case Messages.EnMessageTypes.REQUEST:
                    return DecodeRequest(cis);
                case Messages.EnMessageTypes.RESPONSE:
                    return DecodeResponse(cis);
                case Messages.EnMessageTypes.EVENT:
                    return DecodeEvent(cis);
                default:
                    throw new InvalidCastException(string.Format("Unknown data contract type in DecodeSingle {0}", messageType));
            }
        }

        private Event DecodeEvent(ICodedInputStream messageStream)
        {
            try
            {
                var protoEvent = Messages.Event.ParseFrom(messageStream);
                return new Event
                {
                    TimeStamp = DateUtils.ToDateTimeUtc(protoEvent.TimeStamp),
                    EventType = protoEvent.EventType,
                    Payload = _encoderFactory.GetEncoderFor(protoEvent.Payload).DecodeDataContract(protoEvent.Payload)
                };
            }
            catch (NotImplementedException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new MessageDecodingException<Event>(e.Message, e);
            }
        }

        private MessageBase DecodeResponse(ICodedInputStream messageStream)
        {
            try
            {
                var protoResponse = Messages.Response.ParseFrom(messageStream);
                return new Response
                {
                    TimeStamp = DateUtils.ToDateTimeUtc(protoResponse.TimeStamp),
                    CorrelationId = protoResponse.CorrelationId,
                    Success = protoResponse.Success,
                    ErrorMessage = protoResponse.ErrorMessage,
                    Payload = _encoderFactory.GetEncoderFor(protoResponse.Payload).DecodeDataContract(protoResponse.Payload)
                };
            }
            catch (NotImplementedException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new MessageDecodingException<Response>(e.Message, e);
            }
        }

        private MessageBase DecodeRequest(ICodedInputStream messageStream)
        {
            try
            {
                var protoRequest = Messages.Request.ParseFrom(messageStream);
                object[] actionParams;
                if (protoRequest.ActionParametersCount > 0)
                {
                    actionParams = new object[protoRequest.ActionParametersCount];
                    var i = 0;
                    foreach (var actionParameter in protoRequest.ActionParametersList)
                    {
                        var payload =
                            _encoderFactory
                                .GetEncoderFor(actionParameter)
                                .DecodeDataContract(actionParameter);

                        actionParams[i++] = payload;
                    }
                }
                else
                {
                    actionParams = new object[0];
                }

                return new Request
                {
                    CorrelationId = protoRequest.CorrelationId,
                    ActionParameters = actionParams,
                    ActionName = protoRequest.ActionName,
                    TimeStamp = DateUtils.ToDateTimeUtc(protoRequest.TimeStamp)
                };
            }
            catch (Exception e)
            {
                throw new MessageDecodingException<Response>(e.Message, e);
            }
        }
    }
}
