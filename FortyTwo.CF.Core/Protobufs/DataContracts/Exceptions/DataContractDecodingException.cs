﻿using System;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions
{
    public class DataContractDecodingException<TMessage> : Exception
    {
        public DataContractDecodingException(string msg) : base(msg) { }
        public DataContractDecodingException(string message, Exception innerException) : base(message, innerException) { }
    }

}
