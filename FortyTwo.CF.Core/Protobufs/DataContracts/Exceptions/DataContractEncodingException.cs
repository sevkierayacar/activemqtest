﻿using System;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions
{
    public class DataContractEncodingException<TMessage> : Exception
    {
        public DataContractEncodingException(string msg) : base(msg) { }
        public DataContractEncodingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
