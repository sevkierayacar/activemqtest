﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using System.Linq;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class AccountStatementDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var accState = dataContract as SharedContracts.DataContracts.Account.AccountStatement;
            if (accState == null)
            {
                return ByteString.Empty;
            }

            var protoAccStateBuilder = AccountStatement.CreateBuilder()
                .SetTradingPlatform(accState.TradingPlatform)
                .SetAccountNumber(accState.Login)
                .SetBalance(accState.Balance)
                .SetClosedPnl(accState.ClosedPnl)
                .SetEquity(accState.Equity)
                .SetFloatingPnl(accState.FloatingPnl)
                .SetCredit(accState.Credit)
                .SetDateTime(DateUtils.ToJavaTimeUtc(accState.DateTime))
                .SetAccountCurrency((EnCurrency)accState.AccountCurrency)
                .SetUserGroup(accState.Group)
                .SetGuid(accState.Guid);

            if (accState.Positions != null)
            {
                foreach (var position in accState.Positions)
                {
                    var protoPosition = Position.CreateBuilder()
                        .SetLogin(position.Login)
                        .SetPositionType((EnPositionType)position.PositionType)
                        .SetPrice(position.Price)
                        .SetCurrentPrice(position.CurrentPrice)
                        .SetProfit(position.Profit)
                        .SetProfitCurrency(EnCurrency.USD)
                        .SetSymbol(position.Symbol)
                        .SetTradingPlatform(position.TradingPlatform)
                        .SetUserGroup(accState.Group)
                        .SetVolume(position.Volume)
                        .SetSwap(position.Swap)
                        .SetCommission(position.Commision)
                        .SetVolumeLots(position.VolumeLots)
                        .SetGuid(position.Guid);

                    if (position.PositionId.HasValue)
                    {
                        protoPosition.SetPositionId(position.PositionId.Value);
                    }
                    if (position.OpenTime.HasValue)
                    {
                        protoPosition.SetOpenTime(DateUtils.ToJavaTimeUtc(position.OpenTime.Value));
                    }
                    protoPosition.Build();
                    protoAccStateBuilder.AddPositions(protoPosition);
                }
            }

            var proto = protoAccStateBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Account.AccountStatement>("Contract type is not a AccountStatement!");
            }

            var proto = AccountStatement.ParseFrom(memStream);

            return new SharedContracts.DataContracts.Account.AccountStatement
            {
                TradingPlatform = proto.TradingPlatform,
                Login = proto.AccountNumber,
                DateTime = DateUtils.ToDateTimeUtc(proto.DateTime),
                Balance = proto.Balance,
                ClosedPnl = proto.ClosedPnl,
                AccountCurrency = (SharedContracts.Enums.EnCurrency)proto.AccountCurrency,
                Credit = proto.Credit,
                Equity = proto.Equity,
                FloatingPnl = proto.FloatingPnl,
                Group = proto.UserGroup, 
                Guid = proto.Guid,
                Positions = proto.PositionsList.Select(protoPosition =>
                    new SharedContracts.DataContracts.TradingPlatform.Position
                    {
                        Symbol = protoPosition.Symbol,
                        UserGroup = protoPosition.UserGroup,
                        Profit = protoPosition.Profit,
                        Volume = protoPosition.Volume,
                        VolumeLots = protoPosition.VolumeLots,
                        Price = protoPosition.Price,
                        CurrentPrice = protoPosition.CurrentPrice,
                        PositionType = (SharedContracts.Enums.EnPositionType)protoPosition.PositionType,
                        Swap = protoPosition.Swap,
                        Commision = protoPosition.Commission,
                        TradingPlatform = protoPosition.TradingPlatform,
                        Login = protoPosition.Login,
                        Guid = protoPosition.Guid,
                        PositionId = protoPosition.HasPositionId ? protoPosition.PositionId : (long?)null,
                        OpenTime = protoPosition.HasOpenTime ? DateUtils.ToDateTimeUtc(protoPosition.OpenTime) : (DateTime?)null
                    }).ToList(),
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.ACCOUNTSTATE; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.Account.AccountStatement); }
        }
    }
}
