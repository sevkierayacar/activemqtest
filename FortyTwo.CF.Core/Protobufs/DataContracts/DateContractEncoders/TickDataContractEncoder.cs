﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

// TODO: Uncomment if necessary
namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    //public class TickDataContractEncoder : IDataContractEncoder
    //{
    //    public ByteString EncodeDataContract(object dataContract)
    //    {
    //        var tick = dataContract as FortyTwo.DataContracts.Ticks.Tick;
    //        if (tick == null)
    //        {
    //            return ByteString.Empty;
    //        }

    //        var proto = Tick.CreateBuilder()
    //                        .SetValue(tick.Value)
    //                        .SetTickSource(tick.TickSource)
    //                        .SetTickType((EnTickType)tick.TickType)
    //                        .SetSymbol(tick.Symbol).Build();

    //        var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
    //        var memStream = new MemoryStream(buffer);
    //        memStream.WriteMessageType((long)DataContractType);
    //        proto.WriteTo(memStream);

    //        return ByteString.CopyFrom(buffer);
    //    }

    //    public object DecodeDataContract(ByteString encodedContract)
    //    {
    //        if (encodedContract.Length == 0)
    //        {
    //            return null;
    //        }

    //        var buffer = new byte[encodedContract.Length];
    //        var memStream = new MemoryStream(buffer);

    //        encodedContract.WriteTo(memStream);
    //        memStream.Position = 0;
    //        var contractType = (EnDataContractType)memStream.ReadDataContractType();

    //        if (contractType != DataContractType)
    //        {
    //            throw new DataContractDecodingException<FortyTwo.DataContracts.Ticks.Tick>("Contract type is not a tick!");
    //        }

    //        var proto = Tick.ParseFrom(memStream);

    //        return new FortyTwo.DataContracts.Ticks.Tick
    //        {
    //            Symbol = proto.Symbol,
    //            TickSource = proto.TickSource,
    //            TickType = (Enums.EnTickType)proto.TickType,
    //            Value = proto.Value
    //        };
    //    }

    //    public EnDataContractType DataContractType
    //    {
    //        get { return EnDataContractType.TICK; }
    //    }

    //    public Type PayloadType
    //    {
    //        get
    //        {
    //            return null; // typeof(FortyTwo.DataContracts.Ticks.Tick); 
    //        }
    //    }
    //}
}
