﻿using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class DoubleDataContractEncoder : ValueTypeDataContractEncoderBase<double>
    {
        public override EnDataContractType DataContractType
        {
            get { return EnDataContractType.DOUBLE; }
        }

        public override byte[] GetBytesForValue(object value)
        {
            return BitConverter.GetBytes((double)value);
        }

        public override object GetObjectFromBytes(MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadDouble();
        }
    }
}
