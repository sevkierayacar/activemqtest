﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using SharedContracts.Enums;
using SharedContracts.DataContracts.Ticks;
using System.Linq;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class InstrumentInfoEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var instrumentInfo = dataContract as SharedContracts.DataContracts.Instruments.InstrumentInfo;
            if (instrumentInfo == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = InstrumentInfo.CreateBuilder()
                                        .SetId(0) // TODO what is this? there should not be such field
                                        .SetContractSize(instrumentInfo.ContractSize)
                                        .SetCountry(instrumentInfo.Country)
                                        .SetDigits(instrumentInfo.Digits)
                                        .SetInstrumentType((EnInstrumentType)instrumentInfo.InstrumentType)
                                        .SetSymbol(instrumentInfo.Symbol)
                                        .SetTickSize(instrumentInfo.TickSize)
                                        .SetTickValue(instrumentInfo.TickValue)
                                        .SetTradingPlatform(instrumentInfo.TradingPlatform)
                                        .SetUnderlyingCcy(instrumentInfo.UnderlyingCcy)
                                        .SetTradingPlatformInstrumentType(instrumentInfo.TradingPlatformInstrumentType)
                                        .SetTradingPlatformInstrumentId(instrumentInfo.TradingPlatformInstrumentId)
                                        .SetMarginCalculation((EnSymbolMarginCalculation)instrumentInfo.MarginCalculation)
                                        .SetInitialMargin(instrumentInfo.InitialMargin)
                                        .SetLimitStopLevel(instrumentInfo.LimitStopLevel)
                                        .SetPercentage(instrumentInfo.Percentage)
                                        .SetMarginCurrency(instrumentInfo.MarginCurrency)
                                        .SetProfitCalculation((EnSymbolProfitCalculation)instrumentInfo.ProfitCalculationType)
                                        .SetDescription(instrumentInfo.Description)
                                        .SetSource(instrumentInfo.Source)
                                        .SetTrade(instrumentInfo.Trade)
                                        .SetBackgroundColor(instrumentInfo.BackgroundColor)
                                        .SetCount(instrumentInfo.Count)
                                        .SetCountOriginal(instrumentInfo.CountOriginal)
                                        .SetRealtime(instrumentInfo.Realtime)
                                        .SetStartingDateTime(DateUtils.ToJavaTimeUtc(instrumentInfo.StartingDateTime))
                                        .SetExpirationDateTime(DateUtils.ToJavaTimeUtc(instrumentInfo.ExpirationDateTime))
                                        .SetProfitReserved(instrumentInfo.ProfitReserved)
                                        .SetFilter(instrumentInfo.Filter)
                                        .SetFilterCounter(instrumentInfo.FilterCounter)
                                        .SetFilterLimit(instrumentInfo.FilterLimit)
                                        .SetFilterSmoothing(instrumentInfo.FilterSmoothing)
                                        .SetFilterReserved(instrumentInfo.FilterReserved)
                                        .SetLogging(instrumentInfo.Logging)
                                        .SetSpread(instrumentInfo.Spread)
                                        .SetSpreadBalance(instrumentInfo.SpreadBalance)
                                        .SetExemode(instrumentInfo.Exemode)
                                        .SetSwapEnable(instrumentInfo.SwapEnable)
                                        .SetSwapType(instrumentInfo.SwapType)
                                        .SetSwapLong(instrumentInfo.SwapLong)
                                        .SetSwapShort(instrumentInfo.SwapShort)
                                        .SetSwapRollover3Days(instrumentInfo.SwapRollover3Day)
                                        .SetGtcPendings(instrumentInfo.GtcPendings)
                                        .SetMarginMaintenance(instrumentInfo.MarginMaintenance)
                                        .SetMarginHedged(instrumentInfo.MarginHedged)
                                        .SetPoint(instrumentInfo.Point)
                                        .SetMultiply(instrumentInfo.Multiply)
                                        .SetBidTickvalue(instrumentInfo.BidTickvalue)
                                        .SetAskTickvalue(instrumentInfo.AskTickvalue)
                                        .SetLongOnly(instrumentInfo.LongOnly)
                                        .SetInstantMaxVolume(instrumentInfo.InstantMaxVolumeset)
                                        .SetFreezeLevel(instrumentInfo.FreezeLevel)
                                        .SetMarginHedgedStrong(instrumentInfo.MarginHedgedStrong)
                                        .SetValueDateTime(DateUtils.ToJavaTimeUtc(instrumentInfo.ValueDateTime))
                                        .SetQuotesDelay(instrumentInfo.QuotesDelay)
                                        .SetSwapOpenprice(instrumentInfo.SwapOpenprice)
                                        .SetSwapVariationMargin(instrumentInfo.SwapVariationMargin)
                                        .SetGuid(instrumentInfo.Guid);
            if (instrumentInfo.QuoteSessions.Count >0)
            {
                foreach (var session in instrumentInfo.QuoteSessions)
                {
                    var protoDailySession = DailySession.CreateBuilder()
                        .SetCloseHour(session.CloseHour)
                        .SetCloseMin(session.CloseMin)
                        .SetOpenHour(session.OpenHour)
                        .SetOpenMin(session.OpenMin)
                        .SetDayOfWeek((EnDayOfWeek)session.DayOfWeek);

                    if (session.TimeZoneCorrection.HasValue)
                    {
                        protoDailySession.SetTimeZoneCorrection(session.TimeZoneCorrection.Value);
                    }
                    protoDailySession.Build();
                    protoBuilder.AddQuoteSessions(protoDailySession);
                }
            }
            if (instrumentInfo.TradeSessions.Count > 0)
            {
                foreach (var session in instrumentInfo.TradeSessions)
                {
                    var protoDailySession = DailySession.CreateBuilder()
                        .SetCloseHour(session.CloseHour)
                        .SetCloseMin(session.CloseMin)
                        .SetOpenHour(session.OpenHour)
                        .SetOpenMin(session.OpenMin)
                        .SetDayOfWeek((EnDayOfWeek)session.DayOfWeek);

                    if (session.TimeZoneCorrection.HasValue)
                    {
                        protoDailySession.SetTimeZoneCorrection(session.TimeZoneCorrection.Value);
                    }
                    protoDailySession.Build();
                    protoBuilder.AddTradeSessions(protoDailySession);
                }
            }
            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Instruments.InstrumentInfo>("Contract type is not a InstrumentInfo!");
            }

            var proto = InstrumentInfo.ParseFrom(memStream);

            var result = new SharedContracts.DataContracts.Instruments.InstrumentInfo(
                proto.TradingPlatform,
                proto.Symbol,
                (SharedContracts.Enums.EnInstrumentType)proto.InstrumentType,
                proto.ContractSize,
                proto.UnderlyingCcy,
                proto.Country,
                proto.TickSize,
                proto.TickValue,
                proto.Digits,
                proto.TradingPlatformInstrumentType,
                proto.TradingPlatformInstrumentId,
                (SharedContracts.Enums.EnSymbolMarginCalculation)proto.MarginCalculation,
                proto.InitialMargin,
                proto.LimitStopLevel,
                proto.Percentage,
                proto.MarginCurrency,
                (SharedContracts.Enums.EnProfitCalculationType)proto.ProfitCalculation,
                proto.Description,
                proto.Source,
                proto.Trade,
                proto.BackgroundColor,
                proto.Count,
                proto.CountOriginal,
                proto.Realtime,
                DateUtils.ToDateTimeUtc(proto.StartingDateTime),
                DateUtils.ToDateTimeUtc(proto.ExpirationDateTime),
                proto.QuoteSessionsList.Select(protoDailySession =>
                    new SharedContracts.DataContracts.Ticks.DailySession
                    {
                        OpenHour = protoDailySession.OpenHour,
                        OpenMin = protoDailySession.OpenMin,
                        CloseHour = protoDailySession.CloseHour,
                        CloseMin = protoDailySession.CloseMin,
                        DayOfWeek = (DayOfWeek)protoDailySession.DayOfWeek,
                        TimeZoneCorrection = protoDailySession.HasTimeZoneCorrection ? protoDailySession.TimeZoneCorrection : (int?)null
                    }).ToList(),
                proto.TradeSessionsList.Select(protoDailySession =>
                    new SharedContracts.DataContracts.Ticks.DailySession
                    {
                        OpenHour = protoDailySession.OpenHour,
                        OpenMin = protoDailySession.OpenMin,
                        CloseHour = protoDailySession.CloseHour,
                        CloseMin = protoDailySession.CloseMin,
                        DayOfWeek = (DayOfWeek)protoDailySession.DayOfWeek,
                        TimeZoneCorrection = protoDailySession.HasTimeZoneCorrection ? protoDailySession.TimeZoneCorrection : (int?)null
                    }).ToList(),
                proto.ProfitReserved,
                proto.Filter,
                proto.FilterCounter,
                proto.FilterLimit,
                proto.FilterSmoothing,
                proto.FilterReserved,
                proto.Logging,
                proto.Spread,
                proto.SpreadBalance,
                proto.Exemode,
                proto.SwapEnable,
                proto.SwapType,
                proto.SwapLong,
                proto.SwapShort,
                proto.SwapRollover3Days,
                proto.GtcPendings,
                proto.MarginMaintenance,
                proto.MarginHedged,
                proto.Point,
                proto.Multiply,
                proto.BidTickvalue,
                proto.AskTickvalue,
                proto.LongOnly,
                proto.InstantMaxVolume,
                proto.FreezeLevel,
                proto.MarginHedgedStrong,
                DateUtils.ToDateTimeUtc(proto.ValueDateTime),
                proto.QuotesDelay,
                proto.SwapOpenprice,
                proto.SwapVariationMargin,
                proto.Guid)

            {
                ProfitCalculationType = (EnProfitCalculationType)proto.ProfitCalculation
            };
            return result;
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.INSTRUMENTINFO; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.Instruments.InstrumentInfo); }
        }
    }
}



