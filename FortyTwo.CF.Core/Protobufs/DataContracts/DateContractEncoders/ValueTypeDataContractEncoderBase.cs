﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public abstract class ValueTypeDataContractEncoderBase<TValueType> : IDataContractEncoder where TValueType : struct
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var valueType = dataContract as TValueType?;
            if (!valueType.HasValue)
            {
                throw new InvalidCastException(string.Format("The data contract is not of {0} type", typeof(TValueType).Name));
            }

            var valueBytes = GetBytesForValue(dataContract);
            var buffer = new byte[valueBytes.Length + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);

            memStream.WriteMessageType((long)DataContractType);
            memStream.Write(valueBytes, 0, valueBytes.Length);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                throw new InvalidCastException(string.Format("Expected {0} type contract, but null value passed instead", typeof(TValueType).Name));
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<TValueType>(string.Format("Contract type is not a {0}!", typeof(TValueType).Name));
            }

            return GetObjectFromBytes(memStream);
        }

        public Type PayloadType { get { return typeof(TValueType); } }

        public abstract EnDataContractType DataContractType { get; }
        public abstract byte[] GetBytesForValue(object value);
        public abstract object GetObjectFromBytes(MemoryStream memStream);
    }
}
