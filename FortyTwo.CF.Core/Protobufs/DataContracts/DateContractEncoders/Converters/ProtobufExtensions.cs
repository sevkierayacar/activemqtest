﻿
using System.Linq;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders.Converters
{
    public static class ProtobufExtensions
    {
        public static Deal ToProtobufDeal(this SharedContracts.DataContracts.TradingPlatform.Deal deal)
        {
            return Deal.CreateBuilder()
                .SetTradingPlatform(deal.TradingPlatform)
                .SetTicket(deal.Ticket)
                .SetLogin(deal.Login)
                .SetDealDateTime(DateUtils.ToJavaTimeUtc(deal.TimeStamp))
                .SetDealDateTimeMt4(DateUtils.ToJavaTimeUtc(deal.TimeStampMt4))
                .SetSenderDateTime(DateUtils.ToJavaTimeUtc(deal.SenderDateTime))
                .SetSymbol(deal.Symbol)
                .SetVolume(deal.Volume)
                .SetDealType((EnDealType)deal.DealType)
                .SetPrice(deal.Price)
                .SetProfit(deal.Profit)
                .SetProfitCurrency((EnCurrency)deal.ProfitCurrency)
                .SetBaseCurrencyProfit(deal.BaseCurrencyProfit)
                .SetBaseCurrency((EnCurrency)deal.BaseCurrency)
                .SetDealOperation((EnDealOperation)deal.DealOperation)
                .SetUserGroup(deal.UserGroup)
                .SetSwap(deal.Swap)
                .SetCommission(deal.Commission)
                .SetStopLoss(deal.StopLoss)
                .SetTakeProfit(deal.TakeProfit)
                .AddApiData(deal.MagicNumber)
                .SetComment(deal.Comment ?? string.Empty)
                .SetMarginRate(deal.MarginRate)
                .AddCommentBytes(deal.CommentBytes != null ? ByteString.CopyFrom(deal.CommentBytes) : ByteString.Empty)
                .SetVolumeLots(deal.VolumeLots)
                .SetGuid(deal.Guid)
                .SetDealId(deal.DealId)
                .SetOrderReason((EnOrderReason)deal.OrderReason)
                .SetIsFifo(deal.IsFifo)
                .SetClosedByTicket(deal.ClosedByTicket)
                .Build();
        }

        public static SharedContracts.DataContracts.TradingPlatform.Deal ToFortyTwoDeal(this Deal proto)
        {
            return new SharedContracts.DataContracts.TradingPlatform.Deal(
                proto.TradingPlatform,
                proto.Ticket,
                proto.Login,
                DateUtils.ToDateTimeUtc(proto.DealDateTime),
                DateUtils.ToDateTimeUtc(proto.DealDateTimeMt4),
                proto.Symbol,
                proto.UserGroup,
                proto.Volume,
                (SharedContracts.Enums.EnDealType)proto.DealType,
                proto.Price,
                proto.Profit,
                (SharedContracts.Enums.EnCurrency)proto.ProfitCurrency,
                proto.BaseCurrencyProfit,
                (SharedContracts.Enums.EnCurrency)proto.BaseCurrency,
                (SharedContracts.Enums.EnDealOperation)proto.DealOperation,
                proto.ApiDataCount > 0 ? proto.ApiDataList[0] : 0,
                proto.Comment,
                proto.Swap,
                proto.Commission,
                proto.StopLoss,
                proto.TakeProfit,
                (SharedContracts.Enums.EnOrderReason)proto.OrderReason, 
                proto.Guid, proto.DealId ?? string.Empty, proto.IsFifo, proto.ClosedByTicket)
            {
                MarginRate = proto.MarginRate,
                SenderDateTime = DateUtils.ToDateTimeUtc(proto.SenderDateTime),
                CommentBytes = proto.CommentBytesList.SelectMany(x => x.ToByteArray()).ToArray(),
                VolumeLots = proto.VolumeLots
            };
        }
    }
}
