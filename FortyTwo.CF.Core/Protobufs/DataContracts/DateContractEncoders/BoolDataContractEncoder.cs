﻿using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class BoolDataContractEncoder : ValueTypeDataContractEncoderBase<bool>
    {
        public override EnDataContractType DataContractType
        {
            get { return EnDataContractType.BOOL; }
        }

        public override byte[] GetBytesForValue(object value)
        {
            return BitConverter.GetBytes((bool)value);
        }

        public override object GetObjectFromBytes(MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadBoolean();
        }
    }
}
