﻿using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class LongDataContractEncoder : ValueTypeDataContractEncoderBase<long>
    {
        public override EnDataContractType DataContractType
        {
            get { return EnDataContractType.LONG; }
        }

        public override byte[] GetBytesForValue(object value)
        {
            return BitConverter.GetBytes((long)value);
        }

        public override object GetObjectFromBytes(MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadInt64();
        }
    }
}
