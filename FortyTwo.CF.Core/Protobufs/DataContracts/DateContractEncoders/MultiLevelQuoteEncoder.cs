﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts
{
    class MultiLevelQuoteEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var multiLevelQuote = dataContract as SharedContracts.DataContracts.Ticks.MultiLevelQuote;
            if (multiLevelQuote == null)
            {
                return ByteString.Empty;
            }

            var protoMultiLevelQuoteBuilder = MultiLevelQuote.CreateBuilder()
                .SetSymbol(multiLevelQuote.Symbol)
                .SetBid(multiLevelQuote.Bid)
                .SetAsk(multiLevelQuote.Ask)
                .SetBidVolume(multiLevelQuote.BidVolume)
                .SetAskVolume(multiLevelQuote.AskVolume)
                .SetIssuerID(multiLevelQuote.IssuerID)
                .SetIssuerName(multiLevelQuote.IssuerName)
                .SetLevel(multiLevelQuote.Level)
                .SetAction(multiLevelQuote.Action);

            var proto = protoMultiLevelQuoteBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Ticks.MultiLevelQuote>("Contract type is not a MultiLevelQuote!");
            }

            var proto = MultiLevelQuote.ParseFrom(memStream);

            return new SharedContracts.DataContracts.Ticks.MultiLevelQuote()
            {
                Symbol = proto.Symbol,
                Bid = proto.Bid,
                Ask = proto.Ask,
                BidVolume = proto.BidVolume,
                AskVolume = proto.AskVolume,
                IssuerID = proto.IssuerID,
                IssuerName = proto.IssuerName,
                Level = proto.Level,
                Action = proto.Action
            };

        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MULTILEVELQUOTE; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.Ticks.MultiLevelQuote); }
        }
    }
}
