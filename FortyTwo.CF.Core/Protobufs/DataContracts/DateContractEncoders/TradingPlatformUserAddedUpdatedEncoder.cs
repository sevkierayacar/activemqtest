﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TradingPlatformUserAddedUpdatedEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var tradingPlatformUserAddedUpdated = dataContract as SharedContracts.DataContracts.TradingPlatform.TradingPlatformUserAddedUpdated;
            if (tradingPlatformUserAddedUpdated == null)
            {
                return ByteString.Empty;
            }

            var currUserStateBuilder = TradingPlatformUser.CreateBuilder();
            currUserStateBuilder.SetAccountCcy((EnCurrency) tradingPlatformUserAddedUpdated.CurrState.AccountCcy);
            currUserStateBuilder.SetComment(tradingPlatformUserAddedUpdated.CurrState.Comment);
            currUserStateBuilder.SetEmail(tradingPlatformUserAddedUpdated.CurrState.Email);
            currUserStateBuilder.SetGroup(tradingPlatformUserAddedUpdated.CurrState.Group);
            currUserStateBuilder.SetId(tradingPlatformUserAddedUpdated.CurrState.Id);
            currUserStateBuilder.SetLeverage(tradingPlatformUserAddedUpdated.CurrState.Leverage);
            currUserStateBuilder.SetLogin(tradingPlatformUserAddedUpdated.CurrState.Login);
            currUserStateBuilder.SetName(tradingPlatformUserAddedUpdated.CurrState.Name);
            currUserStateBuilder.SetBalance(tradingPlatformUserAddedUpdated.CurrState.Balance);
            currUserStateBuilder.SetCredit(tradingPlatformUserAddedUpdated.CurrState.Credit);
            currUserStateBuilder.SetEquity(tradingPlatformUserAddedUpdated.CurrState.Equity);
            currUserStateBuilder.SetMargin(tradingPlatformUserAddedUpdated.CurrState.Margin);
            currUserStateBuilder.SetTradingPlatform(tradingPlatformUserAddedUpdated.CurrState.TradingPlatform);
            currUserStateBuilder.SetCountry(tradingPlatformUserAddedUpdated.CurrState.Country);
            currUserStateBuilder.SetPhone(tradingPlatformUserAddedUpdated.CurrState.Phone);
            currUserStateBuilder.SetCity(tradingPlatformUserAddedUpdated.CurrState.City);
            currUserStateBuilder.SetState(tradingPlatformUserAddedUpdated.CurrState.State);
            currUserStateBuilder.SetZipcode(tradingPlatformUserAddedUpdated.CurrState.ZipCode);
            currUserStateBuilder.SetLastip(tradingPlatformUserAddedUpdated.CurrState.LastIp);
            currUserStateBuilder.SetAddress(tradingPlatformUserAddedUpdated.CurrState.Address);
            currUserStateBuilder.SetStatus(tradingPlatformUserAddedUpdated.CurrState.Status);
            currUserStateBuilder.SetRegdate(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.CurrState.RegDate));
            currUserStateBuilder.SetRegdatemt4(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.CurrState.RegDateMt4));
            currUserStateBuilder.SetLastdate(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.CurrState.LastDate));
            currUserStateBuilder.SetEnable(tradingPlatformUserAddedUpdated.CurrState.Enable);
            currUserStateBuilder.SetGuid(tradingPlatformUserAddedUpdated.CurrState.Guid);
            currUserStateBuilder.SetReadonly(tradingPlatformUserAddedUpdated.CurrState.Readonly);
            currUserStateBuilder.SetAgent(tradingPlatformUserAddedUpdated.CurrState.Agent);

            var builder = TradingPlatformUserAddedUpdated.CreateBuilder();
            builder.CurrState = currUserStateBuilder.Build();
            if (tradingPlatformUserAddedUpdated.PrevState != null)
            {
                var prevUserStateBuilder = TradingPlatformUser.CreateBuilder();
                prevUserStateBuilder.SetAccountCcy((EnCurrency)tradingPlatformUserAddedUpdated.PrevState.AccountCcy);
                prevUserStateBuilder.SetComment(tradingPlatformUserAddedUpdated.PrevState.Comment);
                prevUserStateBuilder.SetEmail(tradingPlatformUserAddedUpdated.PrevState.Email);
                prevUserStateBuilder.SetGroup(tradingPlatformUserAddedUpdated.PrevState.Group);
                prevUserStateBuilder.SetId(tradingPlatformUserAddedUpdated.PrevState.Id);
                prevUserStateBuilder.SetLeverage(tradingPlatformUserAddedUpdated.PrevState.Leverage);
                prevUserStateBuilder.SetLogin(tradingPlatformUserAddedUpdated.PrevState.Login);
                prevUserStateBuilder.SetName(tradingPlatformUserAddedUpdated.PrevState.Name);
                prevUserStateBuilder.SetBalance(tradingPlatformUserAddedUpdated.PrevState.Balance);
                prevUserStateBuilder.SetCredit(tradingPlatformUserAddedUpdated.PrevState.Credit);
                prevUserStateBuilder.SetEquity(tradingPlatformUserAddedUpdated.PrevState.Equity);
                prevUserStateBuilder.SetMargin(tradingPlatformUserAddedUpdated.PrevState.Margin);
                prevUserStateBuilder.SetTradingPlatform(tradingPlatformUserAddedUpdated.PrevState.TradingPlatform);
                prevUserStateBuilder.SetCountry(tradingPlatformUserAddedUpdated.PrevState.Country);
                prevUserStateBuilder.SetPhone(tradingPlatformUserAddedUpdated.PrevState.Phone);
                prevUserStateBuilder.SetCity(tradingPlatformUserAddedUpdated.PrevState.City);
                prevUserStateBuilder.SetState(tradingPlatformUserAddedUpdated.PrevState.State);
                prevUserStateBuilder.SetStatus(tradingPlatformUserAddedUpdated.PrevState.Status);
                prevUserStateBuilder.SetZipcode(tradingPlatformUserAddedUpdated.PrevState.ZipCode);
                prevUserStateBuilder.SetLastip(tradingPlatformUserAddedUpdated.PrevState.LastIp);
                prevUserStateBuilder.SetAddress(tradingPlatformUserAddedUpdated.PrevState.Address);
                prevUserStateBuilder.SetRegdate(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.PrevState.RegDate));
                prevUserStateBuilder.SetRegdatemt4(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.PrevState.RegDateMt4));
                prevUserStateBuilder.SetLastdate(DateUtils.ToJavaTimeUtc(tradingPlatformUserAddedUpdated.PrevState.LastDate));
                prevUserStateBuilder.SetEnable(tradingPlatformUserAddedUpdated.PrevState.Enable);
                prevUserStateBuilder.SetGuid(tradingPlatformUserAddedUpdated.PrevState.Guid);
               
                prevUserStateBuilder.SetReadonly(tradingPlatformUserAddedUpdated.PrevState.Readonly);
                prevUserStateBuilder.SetAgent(tradingPlatformUserAddedUpdated.PrevState.Agent);
                builder.PrevState = prevUserStateBuilder.Build();
            }
            var proto = builder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);
            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.TradingPlatformUserAddedUpdated>("Contract type is not a TradingPlatformUserAddedUpdated!");
            }

            var proto = TradingPlatformUserAddedUpdated.ParseFrom(memStream);
            var currUserState =
                new SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser(
                    tradingPlatform: proto.CurrState.TradingPlatform,
                    login: proto.CurrState.Login, group: proto.CurrState.Group, name: proto.CurrState.Name,
                    email: proto.CurrState.Email, comment: proto.CurrState.Comment, id: proto.CurrState.Id,
                    leverage: proto.CurrState.Leverage, accountCcy: (SharedContracts.Enums.EnCurrency) proto.CurrState.AccountCcy, country: proto.CurrState.Country, phone: proto.CurrState.Phone,
                    city:proto.CurrState.City,
                    state:proto.CurrState.State,
                    status:proto.CurrState.Status,
                    zipCode:proto.CurrState.Zipcode,
                    lastIp:proto.CurrState.Lastip,
                    address:proto.CurrState.Address,
                    regDate:DateUtils.ToDateTimeUtc(proto.CurrState.Regdate),
                    regDateMt4: DateUtils.ToDateTimeUtc(proto.CurrState.Regdatemt4),
                    lastDate: DateUtils.ToDateTimeUtc(proto.CurrState.Lastdate),
                    enable: proto.CurrState.Enable,
                    readonlyuser: proto.CurrState.Readonly,
                    agent: proto.CurrState.Agent,
                    guid: proto.CurrState.Guid,
                    senderDateTime: DateUtils.ToDateTimeUtc(proto.CurrState.SenderDateTime));
            SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser prevUserState = null;
            if (proto.HasPrevState)
            {
                prevUserState = new SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser(
                    tradingPlatform: proto.PrevState.TradingPlatform,
                    login: proto.PrevState.Login, group: proto.PrevState.Group, name: proto.PrevState.Name,
                    email: proto.PrevState.Email, comment: proto.PrevState.Comment, id: proto.PrevState.Id,
                    leverage: proto.PrevState.Leverage, accountCcy: (SharedContracts.Enums.EnCurrency)proto.PrevState.AccountCcy, country: proto.PrevState.Country, phone: proto.PrevState.Phone,
                    city: proto.PrevState.City,
                    state: proto.PrevState.State,
                    status: proto.PrevState.Status,
                    zipCode: proto.PrevState.Zipcode,
                    lastIp: proto.PrevState.Lastip,
                    address: proto.PrevState.Address,
                    regDate: DateUtils.ToDateTimeUtc(proto.PrevState.Regdate),
                    regDateMt4: DateUtils.ToDateTimeUtc(proto.PrevState.Regdatemt4),
                    lastDate: DateUtils.ToDateTimeUtc(proto.PrevState.Lastdate),
                    enable:proto.PrevState.Enable,
                    readonlyuser: proto.PrevState.Readonly,
                    agent: proto.PrevState.Agent,
                    guid: proto.CurrState.Guid,
                    senderDateTime: DateUtils.ToDateTimeUtc(proto.PrevState.SenderDateTime));
            }
            return new SharedContracts.DataContracts.TradingPlatform.TradingPlatformUserAddedUpdated(currUserState, prevUserState);
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.USERRECORDADDEDUPDATED; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.TradingPlatformUserAddedUpdated); }
        }
    }
}
