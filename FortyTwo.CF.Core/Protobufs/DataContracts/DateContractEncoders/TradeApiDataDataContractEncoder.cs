﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TradeApiDataDataContractEncoder : IDataContractEncoder
    {
        #region IDataContractEncoder Members

        public ByteString EncodeDataContract(object dataContract)
        {
            var apiData = dataContract as SharedContracts.DataContracts.TradingPlatform.TradeApiData;
            if (apiData == null)
            {
                return ByteString.Empty;
            }

            var proto = TradeApiData.CreateBuilder()
                                        .SetTicket(apiData.Ticket)
                                        .SetLogin(apiData.Login)
                                        .SetTradingPlatform(apiData.TradingPlatform)
                                        .AddApiData(apiData.ApiData)
                                        .SetGuid(apiData.Guid)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.TradeApiData>("Contract type is not a trade api data!");
            }

            var proto = TradeApiData.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.TradeApiData(
                proto.TradingPlatform, 
                proto.Login, 
                proto.Ticket, 
                proto.ApiDataCount > 0 ? proto.ApiDataList[0] : 0,
                proto.Guid);
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.TRADEAPIDATA; }
        }

        public Type PayloadType
        {
            get { return typeof (SharedContracts.DataContracts.TradingPlatform.TradeApiData); }
        }

        #endregion
    }
}
