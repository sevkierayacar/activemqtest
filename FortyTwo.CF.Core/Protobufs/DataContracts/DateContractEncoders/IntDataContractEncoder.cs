﻿using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class IntDataContractEncoder : ValueTypeDataContractEncoderBase<int>
    {
        public override EnDataContractType DataContractType
        {
            get { return EnDataContractType.INT; }
        }

        public override byte[] GetBytesForValue(object value)
        {
            return BitConverter.GetBytes((int)value);
        }

        public override object GetObjectFromBytes(MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadInt32();
        }
    }
}
