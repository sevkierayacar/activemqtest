﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class BalanceChangeDataContractEncoder : IDataContractEncoder
    {
        #region IDataContractEncoder Members

        public ByteString EncodeDataContract(object dataContract)
        {
            var balanceChange = dataContract as SharedContracts.DataContracts.TradingPlatform.BalanceChange;
            if (balanceChange == null)
            {
                return ByteString.Empty;
            }

            var proto = BalanceChange.CreateBuilder()
                                        .SetTicket(balanceChange.Ticket)
                                        .SetLogin(balanceChange.Login)
                                        .SetTradingPlatform(balanceChange.TradingPlatform)
                                        .SetCurBalance(balanceChange.CurrentBalance)
                                        .SetCurMargin(balanceChange.CurrentMargin)
                                        .SetGuid(balanceChange.Guid)
                                        .SetFreeMargin(balanceChange.FreeMargin)
                                        .SetEquity(balanceChange.Equity)
                                        .SetCredit(balanceChange.Credit)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.BalanceChange>("Contract type is not a trade api data!");
            }

            var proto = BalanceChange.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.BalanceChange
            {
                CurrentBalance = proto.CurBalance,
                Login = proto.Login,
                Ticket = proto.HasTicket ? proto.Ticket : 0,
                TradingPlatform = proto.TradingPlatform,
                CurrentMargin = proto.CurMargin,
                Guid = proto.Guid,
                FreeMargin = proto.FreeMargin,
                Equity = proto.Equity,
                Credit = proto.Credit
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.BALANCECHANGE; }
        }

        public Type PayloadType
        {
            get { return typeof (SharedContracts.DataContracts.TradingPlatform.BalanceChange); }
        }

        #endregion
    }
}
