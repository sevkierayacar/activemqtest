﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class SymbolRateInfoEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var symbolRate = dataContract as SharedContracts.DataContracts.TradingPlatform.SymbolRateInfo;
            if (symbolRate == null)
            {
                return ByteString.Empty;
            }

            var proto = SymbolRateInfo.CreateBuilder()
                            .SetSymbol(symbolRate.Symbol)
                            .SetRate(symbolRate.Rate).Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.SymbolRateInfo>("Contract type is not a SymbolRateInfo!");
            }

            var proto = SymbolRateInfo.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.SymbolRateInfo(proto.Symbol, proto.Rate);
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.SYMBOLRATEINFO; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.SymbolRateInfo); }
        }
    }
}
