﻿using System;
using System.IO;
using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class MarginCallUsersDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var marginCallUsers = dataContract as SharedContracts.DataContracts.TradingPlatform.MarginCallUsers;
            if (marginCallUsers == null)
            {
                return ByteString.Empty;
            }

            var protoMarginCallUsersBuilder =  MarginCallUsers.CreateBuilder()
                .SetTradingPlatform(marginCallUsers.TradingPlatform)
                .SetGuid(marginCallUsers.Guid);
            if (marginCallUsers.Logins != null)
            {
                foreach (var login in marginCallUsers.Logins)
                {
                    protoMarginCallUsersBuilder.AddLogins(login);
                }
            }

            var proto = protoMarginCallUsersBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.MarginCallUsers>("Contract type is not a MarginCallUsers!");
            }

            var proto = MarginCallUsers.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.MarginCallUsers()
            {
                TradingPlatform = proto.TradingPlatform,
                Logins = proto.LoginsList.ToList(),
                Guid = proto.Guid
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MARGINCALLUSERS; }
        }

        public Type PayloadType
        {
            get { return typeof (SharedContracts.DataContracts.TradingPlatform.MarginCallUsers); }
        }
    }
}