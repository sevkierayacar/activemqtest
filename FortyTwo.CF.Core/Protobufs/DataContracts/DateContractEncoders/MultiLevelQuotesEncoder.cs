﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts
{
    class MultiLevelQuotesEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var multiLevelQuotes = dataContract as SharedContracts.DataContracts.Ticks.MultiLevelQuotes;
            if (multiLevelQuotes == null)
            {
                return ByteString.Empty;
            }

            var protoMultiLevelQuotesBuilder = MultiLevelQuotes.CreateBuilder()
                .SetTickSource(multiLevelQuotes.TickSource)
                .SetOrigTime(DateUtils.ToJavaTimeUtc(multiLevelQuotes.TimeStamp));

            if (multiLevelQuotes.Quotes != null)
            {
                foreach (var multiLevelQuote in multiLevelQuotes.Quotes)
                {
                    var encodedMultiLevelQuotes = MultiLevelQuote
                        .CreateBuilder()
                        .SetSymbol(multiLevelQuote.Symbol)
                        .SetBid(multiLevelQuote.Bid)
                        .SetAsk(multiLevelQuote.Ask)
                        .SetBidVolume(multiLevelQuote.BidVolume)
                        .SetAskVolume(multiLevelQuote.AskVolume)
                        .SetIssuerID(multiLevelQuote.IssuerID)
                        .SetIssuerName(multiLevelQuote.IssuerName)
                        .SetLevel(multiLevelQuote.Level)
                        .SetAction(multiLevelQuote.Action)
                        .Build();

                    protoMultiLevelQuotesBuilder.AddMultiLevelQuote(encodedMultiLevelQuotes);
                }
            }

            var proto = protoMultiLevelQuotesBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Ticks.MultiLevelQuotes>("Contract type is not a MultiLevelQuotes!");
            }

            var proto = MultiLevelQuotes.ParseFrom(memStream);

            return new SharedContracts.DataContracts.Ticks.MultiLevelQuotes()
            {
                TickSource = proto.TickSource,
                Quotes = proto.MultiLevelQuoteList.Select(protoMultiLevelQuote =>
                    new SharedContracts.DataContracts.Ticks.MultiLevelQuote
                    {
                        Symbol = protoMultiLevelQuote.Symbol,
                        Bid = protoMultiLevelQuote.Bid,
                        Ask = protoMultiLevelQuote.Ask,
                        BidVolume = protoMultiLevelQuote.BidVolume,
                        AskVolume = protoMultiLevelQuote.AskVolume,
                        IssuerID = protoMultiLevelQuote.IssuerID,
                        IssuerName = protoMultiLevelQuote.IssuerName,
                        Level = protoMultiLevelQuote.Level,
                        Action = protoMultiLevelQuote.Action
                    }).ToList(),

                TimeStamp = DateUtils.ToDateTimeUtc(proto.OrigTime)
            };

        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MULTILEVELQUOTES; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.Ticks.MultiLevelQuotes); }
        }
    }
}
