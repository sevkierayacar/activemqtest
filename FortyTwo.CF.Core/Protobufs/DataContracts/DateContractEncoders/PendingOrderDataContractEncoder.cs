﻿using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class PendingOrderDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var pendingOrder = dataContract as SharedContracts.DataContracts.TradingPlatform.PendingOrder;
            if (pendingOrder == null)
            {
                return ByteString.Empty;
            }

            var proto = PendingOrder.CreateBuilder()
                .SetTradingPlatform(pendingOrder.TradingPlatform)
                .SetPendingType((EnPendingOrderType) pendingOrder.PendingType)
                .SetTicket(pendingOrder.Ticket)
                .SetLogin(pendingOrder.Login)
                .SetPrice(pendingOrder.Price)
                .SetSymbol(pendingOrder.Symbol)
                .SetVolume(pendingOrder.Volume)
                .SetStopLoss(pendingOrder.StopLoss)
                .SetTakeProfit(pendingOrder.TakeProfit)
                .SetPendingCreation(DateUtils.ToJavaTimeUtc(pendingOrder.PendingCreation))
                .SetPendingExpiration(DateUtils.ToJavaTimeUtc(pendingOrder.PendingExpiration))
                .SetComment(pendingOrder.Comment)
                .AddCommentBytes(pendingOrder.CommentBytes != null ? ByteString.CopyFrom(pendingOrder.CommentBytes) : ByteString.Empty)
                .SetVolumeLots(pendingOrder.VolumeLots)
                .SetGuid(pendingOrder.Guid)
                .SetUserGroup(pendingOrder.UserGroup)
                .SetPendingCreationMt4(DateUtils.ToJavaTimeUtc(pendingOrder.PendingCreationMt4))
                .SetPendingExpirationMt4(DateUtils.ToJavaTimeUtc(pendingOrder.PendingExpirationMt4))
                .SetPendingClose(DateUtils.ToJavaTimeUtc(pendingOrder.PendingClose))
                .SetPendingCloseMt4(DateUtils.ToJavaTimeUtc(pendingOrder.PendingCloseMt4))
                .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<PendingOrder>("Contract type is not a PendingOrder!");
            }

            var proto = PendingOrder.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.PendingOrder
            {
                TradingPlatform = proto.TradingPlatform,
                PendingType = (SharedContracts.Enums.EnPendingOrderType)proto.PendingType,
                Ticket = proto.Ticket,
                Login = proto.Login,
                Price = proto.Price,
                Symbol = proto.Symbol,
                Volume = proto.Volume,
                StopLoss = proto.StopLoss,
                TakeProfit = proto.TakeProfit,
                PendingCreation = DateUtils.ToDateTimeUtc(proto.PendingCreation),
                PendingExpiration = DateUtils.ToDateTimeUtc(proto.PendingExpiration),
                Comment = proto.Comment,
                CommentBytes = proto.CommentBytesList.SelectMany(x => x.ToByteArray()).ToArray(),
                VolumeLots = proto.VolumeLots,
                UserGroup = proto.UserGroup,
                Guid = proto.Guid,
                PendingCreationMt4 = DateUtils.ToDateTimeUtc(proto.PendingCreationMt4),
                PendingExpirationMt4 = DateUtils.ToDateTimeUtc(proto.PendingExpirationMt4),
                PendingClose = DateUtils.ToDateTimeUtc(proto.PendingClose),
                PendingCloseMt4 = DateUtils.ToDateTimeUtc(proto.PendingCloseMt4)
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.PENDINGORDER; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.PendingOrder); }
        }
    }
}
