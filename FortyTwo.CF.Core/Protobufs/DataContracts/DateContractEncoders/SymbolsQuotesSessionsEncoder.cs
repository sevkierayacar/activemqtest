﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using System.Linq;
using SharedContracts.DataContracts.Ticks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class SymbolsQuotesSessionsEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var symbolQuotesSessions = dataContract as SymbolQuotesSessions;
            if (symbolQuotesSessions == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = SymbolQuotesSession.CreateBuilder()
                .SetTicksSource(symbolQuotesSessions.TicksSource)
                .SetSymbol(symbolQuotesSessions.Symbol)
                .SetGuid(symbolQuotesSessions.Guid);
            if (symbolQuotesSessions.DailySessions.Count >0)
            {
                foreach (var session in symbolQuotesSessions.DailySessions)
                {
                    var protoDailySession = DailySession.CreateBuilder()
                        .SetCloseHour(session.CloseHour)
                        .SetCloseMin(session.CloseMin)
                        .SetOpenHour(session.OpenHour)
                        .SetOpenMin(session.OpenMin)
                        .SetDayOfWeek((EnDayOfWeek)session.DayOfWeek);

                    if (session.TimeZoneCorrection.HasValue)
                    {
                        protoDailySession.SetTimeZoneCorrection(session.TimeZoneCorrection.Value);
                    }
                    protoDailySession.Build();
                    protoBuilder.AddDailySession(protoDailySession);
                }
            }

            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SymbolQuotesSessions>("Contract type is not a SymbolQuotesSessions!");
            }

            var proto = SymbolQuotesSession.ParseFrom(memStream);

            return new SymbolQuotesSessions
            {
                TicksSource = proto.TicksSource,
                Symbol = proto.Symbol,
                Guid = proto.Guid,
                DailySessions = proto.DailySessionList.Select(protoDailySession =>
                    new SharedContracts.DataContracts.Ticks.DailySession
                    {
                        OpenHour = protoDailySession.OpenHour,
                        OpenMin = protoDailySession.OpenMin,
                        CloseHour = protoDailySession.CloseHour,
                        CloseMin = protoDailySession.CloseMin,
                        DayOfWeek = (DayOfWeek)protoDailySession.DayOfWeek,
                        TimeZoneCorrection = protoDailySession.HasTimeZoneCorrection ? protoDailySession.TimeZoneCorrection : (int?)null 
                    }).ToList(),
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.SYMBOLQUOTESSESSION; }
        }

        public Type PayloadType
        {
            get { return typeof(SymbolQuotesSessions); }
        }
    }
}
