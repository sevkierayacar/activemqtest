﻿using Google.ProtocolBuffers;
using System;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class EmptyDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            return ByteString.Empty;
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            return null;
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.EMPTY; }
        }

        public Type PayloadType
        {
            get { return typeof(void); }
        }
    }
}
