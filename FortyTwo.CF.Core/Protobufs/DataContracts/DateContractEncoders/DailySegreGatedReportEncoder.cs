﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts
{
    class DailySegregatedReportEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var dailySegregatedReport = dataContract as SharedContracts.DataContracts.TradingPlatform.DailySegregatedReport;
            if (dailySegregatedReport == null)
            {
                return ByteString.Empty;
            }

            var protoDailySegregatedReportBuilder = DailySegregatedReport.CreateBuilder()
                .SetLogin(dailySegregatedReport.Login)
                .SetName(dailySegregatedReport.Name)
                .SetBalance(dailySegregatedReport.Balance)
                .SetCredit(dailySegregatedReport.Credit)
                .SetCommission(dailySegregatedReport.Commission)
                .SetTaxes(dailySegregatedReport.Taxes)
                .SetStorage(dailySegregatedReport.Storage)
                .SetProfit(dailySegregatedReport.Profit)
                .SetInterest(dailySegregatedReport.Interest)
                .SetTax(dailySegregatedReport.Tax)
                .SetPl(dailySegregatedReport.PL)
                .SetEquity(dailySegregatedReport.Equity);

            var proto = protoDailySegregatedReportBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.DailySegregatedReports>("Contract type is not a DailySegregatedReport!");
            }

            var proto = DailySegregatedReport.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.DailySegregatedReport()
            {
                Login = proto.Login,
                Name = proto.Name,
                Balance = proto.Balance,
                Credit = proto.Credit,
                Commission = proto.Commission,
                Taxes = proto.Taxes,
                Storage = proto.Storage,
                Profit = proto.Profit,
                Interest = proto.Interest,
                Tax = proto.Tax,
                PL = proto.Pl,
                Equity = proto.Equity
            };

        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.DAILYSEGREGATEDREPORT; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.DailySegregatedReport); }
        }
    }
}
