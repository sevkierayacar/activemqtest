﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TickChartCandleEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var tickChartCandle = dataContract as SharedContracts.DataContracts.Ticks.TickChartCandle;
            if (tickChartCandle == null)
            {
                return ByteString.Empty;
            }

            var proto = TickChartCandle.CreateBuilder()
                                        .SetTradingPlatform(tickChartCandle.TradingPlatform)
                                        .SetSymbol(tickChartCandle.Symbol)
                                        .SetDate(DateUtils.ToJavaTimeUtc(tickChartCandle.Date))
                                        .SetTotalIndex(tickChartCandle.TotalIndex)
                                        .SetCurIndex(tickChartCandle.CurIndex)
                                        .SetOpenPrice(tickChartCandle.OpenPrice)
                                        .SetClosePrice(tickChartCandle.ClosePrice)
                                        .SetMinPrice(tickChartCandle.MinPrice)
                                        .SetMaxPrice(tickChartCandle.MaxPrice)
                                        .SetVolume(tickChartCandle.Volume)
                                        .SetIsClosed(tickChartCandle.IsClosed)
                                        .SetGuid(tickChartCandle.Guid)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Ticks.TickChartCandle>("Contract type is not a TickChartCandle!");
            }

            var proto = TickChartCandle.ParseFrom(memStream);

            return new SharedContracts.DataContracts.Ticks.TickChartCandle
            {
                TradingPlatform = proto.TradingPlatform,
                Symbol = proto.Symbol,
                Date = DateUtils.ToDateTimeUtc(proto.Date),
                TotalIndex = proto.TotalIndex,
                CurIndex = proto.CurIndex,
                OpenPrice = proto.OpenPrice,
                ClosePrice = proto.ClosePrice,
                MinPrice = proto.MinPrice,
                MaxPrice = proto.MaxPrice,
                Volume = proto.Volume,
                IsClosed = proto.IsClosed,
                Guid = proto.Guid
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.TICKCHARTCANDLE; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.Ticks.TickChartCandle); }
        }
    }
}
