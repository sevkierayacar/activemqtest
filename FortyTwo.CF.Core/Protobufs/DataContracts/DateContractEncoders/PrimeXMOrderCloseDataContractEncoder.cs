﻿using System;
using Google.ProtocolBuffers;
using FortyTwo.CF.Core.Utils;
using System.IO;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    class PrimeXMOrderCloseDataContractEncoder : IDataContractEncoder
    {

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.PrimeXM.PrimeXMOrderClose>("Contract type is not a PrimeXMOrderClose!");
            }

            var proto = PrimeXMOrderClose.ParseFrom(memStream);

            return new SharedContracts.DataContracts.PrimeXM.PrimeXMOrderClose
            {
                orderID = proto.OrderID,
                fillSize = proto.FillSize,
                fillPrice = proto.FillPrice,
                message = proto.Message,
                milli = proto.Milli,
                micro = proto.Micro
            };
        }

        public ByteString EncodeDataContract(object dataContract)
        {
            var primeXMOrderClose = dataContract as SharedContracts.DataContracts.PrimeXM.PrimeXMOrderClose;
            if (primeXMOrderClose == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = PrimeXMOrderClose.CreateBuilder()
                .SetOrderID(primeXMOrderClose.orderID)
                .SetFillSize(primeXMOrderClose.fillSize)
                .SetFillPrice(primeXMOrderClose.fillPrice)
                .SetMessage(primeXMOrderClose.message)
                .SetMilli(primeXMOrderClose.milli)
                .SetMicro(primeXMOrderClose.micro);

            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }
        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.PRIMEXMORDERCLOSE; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.PrimeXM.PrimeXMOrderClose); }
        }
    }
}
