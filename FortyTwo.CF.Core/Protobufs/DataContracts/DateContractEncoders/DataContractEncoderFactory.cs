﻿using FortyTwo.CF.Core.Utils;
using Google.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class DataContractEncoderFactory
    {
        private static readonly Dictionary<EnDataContractType, IDataContractEncoder> DataContractEncoders;
        private static readonly Dictionary<Type, IDataContractEncoder> PayloadEncoders;

        static DataContractEncoderFactory()
        {
            DataContractEncoders = new Dictionary<EnDataContractType, IDataContractEncoder>();
            PayloadEncoders = new Dictionary<Type, IDataContractEncoder>();

            var allDcEncoderTypes = Assembly.GetExecutingAssembly()
                    .GetTypes()
                    .Where(type => typeof(IDataContractEncoder).IsAssignableFrom(type) && !type.IsInterface && !type.IsAbstract);

            foreach (var encoderType in allDcEncoderTypes)
            {
                var encoderInstance = (IDataContractEncoder)Activator.CreateInstance(encoderType);
                DataContractEncoders.Add(encoderInstance.DataContractType, encoderInstance);
                PayloadEncoders.Add(encoderInstance.PayloadType, encoderInstance);
            }
        }

        public IDataContractEncoder GetEncoderFor(ByteString encodedDataContract)
        {
            if (encodedDataContract.IsEmpty)
            {
                return DataContractEncoders[EnDataContractType.EMPTY];
            }

            var memStream = new MemoryStream(encodedDataContract.ToByteArray());
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (DataContractEncoders.ContainsKey(contractType))
            {
                return DataContractEncoders[contractType];
            }

            throw new NotImplementedException(string.Format("Encoder not implemented for data contract type: '{0}'.", contractType));
        }

        public IDataContractEncoder GetEncoderFor(Type payloadType)
        {
            if (PayloadEncoders.ContainsKey(payloadType))
            {
                return PayloadEncoders[payloadType];
            }

            throw new NotImplementedException(string.Format("Encoder not implemented for payload type: '{0}'.", payloadType.Name));
        }
    }
}
