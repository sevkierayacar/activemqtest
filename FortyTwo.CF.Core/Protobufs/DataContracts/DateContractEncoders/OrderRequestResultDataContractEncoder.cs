﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders.Converters;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class OrderRequestResultDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var orderRequestResult = dataContract as SharedContracts.DataContracts.TradingPlatform.OrderRequestResult;
            if (orderRequestResult == null)
            {
                return ByteString.Empty;
            }

            var builder = OrderRequestResult.CreateBuilder()
                .SetLogin(orderRequestResult.Login)
                .SetTradingPlatform(orderRequestResult.TradingPlatform)
                .SetSuccess(orderRequestResult.Success)
                .SetRequestId(orderRequestResult.RequestId)
                .SetTicket(orderRequestResult.Ticket)
                .SetGuid(orderRequestResult.Guid);

            if (orderRequestResult.ErrorMessage != null) builder.SetErrorMessage(orderRequestResult.ErrorMessage);

            var proto = builder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.OrderRequestResult>("Contract type is not a Log Error!");
            }

            var proto = OrderRequestResult.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.OrderRequestResult
            {
                TradingPlatform = proto.TradingPlatform,
                Login =  proto.Login,
                Success = proto.Success,
                RequestId = proto.RequestId,
                ErrorMessage = proto.HasErrorMessage ? proto.ErrorMessage : null,
                Ticket = proto.Ticket,
                Guid = proto.Guid
            };
        }

        public EnDataContractType DataContractType { get { return EnDataContractType.ORDERREQUESTRESULT; } }
        public Type PayloadType { get { return typeof(SharedContracts.DataContracts.TradingPlatform.OrderRequestResult); } }
    }
}
