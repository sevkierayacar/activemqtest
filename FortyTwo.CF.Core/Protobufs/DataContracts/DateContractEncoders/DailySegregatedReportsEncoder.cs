﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts
{
    class DailySegregatedReportsEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var dailySegregatedReports = dataContract as SharedContracts.DataContracts.TradingPlatform.DailySegregatedReports;
            if (dailySegregatedReports == null)
            {
                return ByteString.Empty;
            }

            var protoDailySegregatedReportsBuilder = DailySegregatedReports.CreateBuilder()
                .SetTradingPlatform(dailySegregatedReports.TradingPlatform)
                .SetGuid("null");

            if (dailySegregatedReports.Reports != null)
            {
                foreach (var dailySegregatedReport in dailySegregatedReports.Reports)
                {
                    var encodedDailySegregatedReport = DailySegregatedReport
                        .CreateBuilder()
                        .SetLogin(dailySegregatedReport.Login)
                        .SetName(dailySegregatedReport.Name)
                        .SetBalance(dailySegregatedReport.Balance)
                        .SetCredit(dailySegregatedReport.Credit)
                        .SetCommission(dailySegregatedReport.Commission)
                        .SetTaxes(dailySegregatedReport.Taxes)
                        .SetStorage(dailySegregatedReport.Storage)
                        .SetProfit(dailySegregatedReport.Profit)
                        .SetInterest(dailySegregatedReport.Interest)
                        .SetTax(dailySegregatedReport.Tax)
                        .SetPl(dailySegregatedReport.PL)
                        .SetEquity(dailySegregatedReport.Equity)
                        .Build();
                    protoDailySegregatedReportsBuilder.AddDailySegregatedReport(encodedDailySegregatedReport);
                }
            }

            var proto = protoDailySegregatedReportsBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.DailySegregatedReports>("Contract type is not a DailySegregatedReports!");
            }

            var proto = DailySegregatedReports.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.DailySegregatedReports()
            {
                TradingPlatform = proto.TradingPlatform,
                Reports = proto.DailySegregatedReportList.Select(protoDailyReport =>
                    new SharedContracts.DataContracts.TradingPlatform.DailySegregatedReport
                    {
                        Login = protoDailyReport.Login,
                        Name = protoDailyReport.Name,
                        Balance = protoDailyReport.Balance,
                        Credit = protoDailyReport.Credit,
                        Commission = protoDailyReport.Commission,
                        Taxes = protoDailyReport.Taxes,
                        Storage = protoDailyReport.Storage,
                        Profit = protoDailyReport.Profit,
                        Interest = protoDailyReport.Interest,
                        Tax = protoDailyReport.Tax,
                        PL = protoDailyReport.Pl,
                        Equity = protoDailyReport.Equity,
                    }).ToList(),

                Guid = proto.Guid
            };

        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.DAILYSEGREGATEDREPORTS; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.DailySegregatedReports); }
        }
    }
}
