﻿using System;
using System.IO;
using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class DataContractsListEncoder : IDataContractEncoder
    {
        private readonly DataContractEncoderFactory _encoderFactory;
        private readonly MemoryStream _dataContactsEncodingStream;
        private readonly DataContractEncoderFactory _factory;

        public DataContractsListEncoder()
        {
            _factory = new DataContractEncoderFactory();
            _encoderFactory = new DataContractEncoderFactory();
            _dataContactsEncodingStream = new MemoryStream();

        }

        public ByteString EncodeDataContract(object dataContract)
        {
            var dcList = dataContract as SharedContracts.DataContracts.DataContractsList;
            if (dcList == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = DataContractsList.CreateBuilder();
            foreach (var dc in dcList.DataContracts)
            {
                var encoder = _encoderFactory.GetEncoderFor(dc.GetType());
                var encodedDc = encoder.EncodeDataContract(dc);
                protoBuilder.AddList(encodedDc);
            }
            var proto = protoBuilder.Build();
            _dataContactsEncodingStream.Position = 0;
            _dataContactsEncodingStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(_dataContactsEncodingStream);

            return ByteString.CopyFrom(_dataContactsEncodingStream.GetBuffer(), 0, proto.SerializedSize + StreamExtension.TypeDataContractTypeSize);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);
            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.DataContractsList>("Contract type is not a DataContractsList!");
            }
            var cis = CodedInputStream.CreateInstance(memStream);
            cis.SetSizeLimit(124 * 1024 * 1024);
            var proto = DataContractsList.ParseFrom(cis);
            memStream.Position = cis.Position;
            var dataContracts =
                (from item in proto.ListList
                 let encoder = _factory.GetEncoderFor(item)
                 select encoder.DecodeDataContract(item)).ToList();

            return new SharedContracts.DataContracts.DataContractsList { DataContracts = dataContracts };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.LIST; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.DataContractsList); }
        }
    }
}
