﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class CommisionEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var commision = dataContract as SharedContracts.DataContracts.TradingPlatform.Commision;
            if (commision == null)
            {
                return ByteString.Empty;
            }

            var proto = Commision.CreateBuilder()
                                        .SetAmountCurrency((EnCurrency)commision.AmountCurrency)
                                        .SetSymbol(commision.Symbol)
                                        .SetAmount(commision.Amount)
                                        .SetBaseCurrency((EnCurrency)commision.BaseCurrency)
                                        .SetBaseCurrencyAmount(commision.BaseCurrencyAmount)
                                        .SetCommisionType((EnCommisionType)commision.CommisionType)
                                        .SetCommisionDateTime(DateUtils.ToJavaTimeUtc(commision.TimeStamp))
                                        .SetCommisionDateTimeMt4(DateUtils.ToJavaTimeUtc(commision.TimeStampMt4))
                                        .SetSenderDateTime(DateUtils.ToJavaTimeUtc(commision.SenderDateTime))
                                        .SetLogin(commision.Login)
                                        .SetTicket(commision.Ticket)
                                        .SetDealId(commision.DealId)
                                        .SetUserGroup(commision.UserGroup)
                                        .SetTradingPlatform(commision.TradingPlatform)
                                        .SetIsClosed(commision.IsClosed)

                                        .SetGuid(commision.Guid)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.Commision>("Contract type is not a commission!");
            }

            var proto = Commision.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.Commision(
                            tradingPlatform: proto.TradingPlatform,
                            login: proto.Login,
                            timeStamp: DateUtils.ToDateTimeUtc(proto.CommisionDateTime),
                            timeStampMt4: DateUtils.ToDateTimeUtc(proto.CommisionDateTimeMt4),
                            userGroup: proto.UserGroup,
                            ticket: proto.Ticket,
                            dealId: proto.DealId,
                            commisionType: (SharedContracts.Enums.EnCommisionType)proto.CommisionType,
                            isClosed: proto.IsClosed,
                            amount: proto.Amount,
                            baseCurrencyAmount: proto.BaseCurrencyAmount,
                            amountCurrency: (SharedContracts.Enums.EnCurrency)proto.AmountCurrency,
                            baseCurrency: (SharedContracts.Enums.EnCurrency)proto.BaseCurrency,
                            symbol: proto.Symbol,
                            guid: proto.Guid)
            {
                SenderDateTime = DateUtils.ToDateTimeUtc(proto.SenderDateTime),
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.COMMISION; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.Commision); }
        }
    }
}
