﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using System.Text;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class StringDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            if (dataContract == null)
            {
                return ByteString.Empty;
            }
            //TODO 16 bit
            var stringDC = dataContract.ToString();
            var contractBytes = Encoding.UTF8.GetBytes(stringDC);
            var buffer = new byte[contractBytes.Length + sizeof(UInt16) + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);

            memStream.WriteMessageType((long)DataContractType);
            memStream.Write(BitConverter.GetBytes((UInt16)stringDC.Length), 0, sizeof(UInt16));
            memStream.Write(contractBytes, 0, contractBytes.Length);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<string>("Contract type is not a string!");
            }
            memStream.Position = StreamExtension.TypeDataContractTypeSize + sizeof(UInt16);

            return Encoding.UTF8.GetString(
                new BinaryReader(memStream).ReadBytes((int)memStream.Length - StreamExtension.TypeDataContractTypeSize));
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.STRING; }
        }

        public Type PayloadType
        {
            get { return typeof(string); }
        }
    }
}
