﻿using System;
using System.IO;
using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class MarginCallUsersDetailedDataContractEncoder : IDataContractEncoder
    {

        public ByteString EncodeDataContract(object dataContract)
        {
            var marginCallUsersDetailed = dataContract as SharedContracts.DataContracts.TradingPlatform.MarginCallUsersDetailed;
            if (marginCallUsersDetailed == null)
            {
                return ByteString.Empty;
            }

            var protoMarginCallUsersDetailedBuilder = MarginCallUsersDetailed.CreateBuilder()
                .SetTradingPlatform(marginCallUsersDetailed.TradingPlatform)
                .SetGuid(marginCallUsersDetailed.Guid);

            if (marginCallUsersDetailed.Logins != null)
            {
                foreach (var platformUser in marginCallUsersDetailed.Logins)
                {
                    var userLogin = TradingPlatformUser
                        .CreateBuilder()
                        .SetAccountCcy((EnCurrency)platformUser.AccountCcy)
                        .SetComment(platformUser.Comment)
                        .SetEmail(platformUser.Email)
                        .SetGroup(platformUser.Group)
                        .SetId(platformUser.Id)
                        .SetLeverage(platformUser.Leverage)
                        .SetLogin(platformUser.Login)
                        .SetName(platformUser.Name)
                        .SetBalance(platformUser.Balance)
                        .SetCredit(platformUser.Credit)
                        .SetEquity(platformUser.Equity)
                        .SetMargin(platformUser.Margin)
                        .SetTradingPlatform(platformUser.TradingPlatform)
                        .AddCommentBytes(platformUser.CommentBytes != null ? ByteString.CopyFrom(platformUser.CommentBytes) : ByteString.Empty)
                        .SetCountry(platformUser.Country)
                        .SetPhone(platformUser.Phone)
                        .SetCity(platformUser.City)
                        .SetState(platformUser.State)
                        .SetStatus(platformUser.Status)
                        .SetZipcode(platformUser.ZipCode)
                        .SetLastip(platformUser.LastIp)
                        .SetAddress(platformUser.Address)
                        .SetRegdate(DateUtils.ToJavaTimeUtc(platformUser.RegDate))
                        .SetRegdatemt4(DateUtils.ToJavaTimeUtc(platformUser.RegDateMt4))
                        .SetLastdate(DateUtils.ToJavaTimeUtc(platformUser.LastDate))
                        .SetEnable(platformUser.Enable)
                        .SetGuid(platformUser.Guid)
                        .SetReadonly(platformUser.Readonly)
                        .SetAgent(platformUser.Agent)
                        .SetSenderDateTime(DateUtils.ToJavaTimeUtc(platformUser.SenderDateTime))
                        .Build();
                    protoMarginCallUsersDetailedBuilder.AddLogins(userLogin);
                }
            }

            var proto = protoMarginCallUsersDetailedBuilder.Build();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.MarginCallUsersDetailed>("Contract type is not a MarginCallUsersDetailed!");
            }

            var proto = MarginCallUsersDetailed.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.MarginCallUsersDetailed()
            {
                TradingPlatform = proto.TradingPlatform,
                Logins = proto.LoginsList.Select(protoUserLogin =>
                    new SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser
                    {
                        TradingPlatform = protoUserLogin.TradingPlatform,
                        Login = protoUserLogin.Login,
                        Group = protoUserLogin.Group,
                        Name = protoUserLogin.Name,
                        Email = protoUserLogin.Email,
                        Comment = protoUserLogin.Comment,
                        Balance = protoUserLogin.Balance,
                        Credit = protoUserLogin.Credit,
                        Equity = protoUserLogin.Equity,
                        Margin = protoUserLogin.Margin,
                        Id = protoUserLogin.Id,
                        Leverage = protoUserLogin.Leverage,
                        AccountCcy = (SharedContracts.Enums.EnCurrency)protoUserLogin.AccountCcy,
                        Country = protoUserLogin.Country,
                        Phone = protoUserLogin.Phone,
                        City = protoUserLogin.City,
                        State = protoUserLogin.State,
                        Status = protoUserLogin.Status,
                        ZipCode = protoUserLogin.Zipcode,
                        LastIp = protoUserLogin.Lastip,
                        Address = protoUserLogin.Address,
                        RegDate = DateUtils.ToDateTimeUtc(protoUserLogin.Regdate),
                        LastDate = DateUtils.ToDateTimeUtc(protoUserLogin.Lastdate),
                        RegDateMt4 = DateUtils.ToDateTimeUtc(protoUserLogin.Regdatemt4),
                        SenderDateTime = DateUtils.ToDateTimeUtc(protoUserLogin.SenderDateTime),
                        Guid = protoUserLogin.Guid
                    }).ToList(),

                Guid = proto.Guid
            };

        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MARGINCALLUSERSDETAILED; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.MarginCallUsersDetailed); }
        }

    }
}