﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders.Converters;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class DealDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var deal = dataContract as SharedContracts.DataContracts.TradingPlatform.Deal;
            if (deal == null)
            {
                return ByteString.Empty;
            }

            var proto = deal.ToProtobufDeal();
            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.Deal>("Contract type is not a deal!");
            }

            var proto = Deal.ParseFrom(memStream);
            return proto.ToFortyTwoDeal();
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.DEAL; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.Deal); }
        }
    }
}
