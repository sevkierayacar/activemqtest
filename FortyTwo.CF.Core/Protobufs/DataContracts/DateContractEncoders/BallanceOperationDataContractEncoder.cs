﻿using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class BallanceOperationDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var balop = dataContract as SharedContracts.DataContracts.BackOffice.BalanceOperation;
            if (balop == null)
            {
                return ByteString.Empty;
            }

            var proto = BalanceOperation.CreateBuilder()
                                        .SetAmountCurrency((EnCurrency)balop.AmountCurrency)
                                        .SetAmount(balop.Amount)
                                        .SetBalanceOperationType((EnBalanceOperationType)balop.BallanceOperationType)
                                        .SetBalOpDateTime(DateUtils.ToJavaTimeUtc(balop.BalOpDateTime))
                                        .SetBalOpDateTimeMt4(DateUtils.ToJavaTimeUtc(balop.BalOpDateTimeMt4))
                                        .SetSenderDateTime(DateUtils.ToJavaTimeUtc(balop.SenderDateTime))
                                        .SetExpireDateTime(DateUtils.ToJavaTimeUtc(balop.ExpireDateTime))
                                        .SetExpireDateTimeMt4(DateUtils.ToJavaTimeUtc(balop.ExpireDateTimeMt4))
                                        .SetComment(balop.Comment)
                                        .SetLogin(balop.Login)
                                        .SetTicket(balop.Ticket)
                                        .SetUserGroup(balop.UserGroup)
                                        .SetTradingPlatform(balop.TradingPlatform)
                                        .SetBaseCurrency((EnCurrency)balop.BaseCurrency)
                                        .SetBaseCurrencyAmount(balop.BaseCurrencyAmount)
                                        .AddCommentBytes(balop.CommentBytes != null ? ByteString.CopyFrom(balop.CommentBytes) : ByteString.Empty)
                                        .SetGuid(balop.Guid)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.BackOffice.BalanceOperation>("Contract type is not a balance operation!");
            }

            var proto = BalanceOperation.ParseFrom(memStream);

            return new SharedContracts.DataContracts.BackOffice.BalanceOperation
            {
                Amount = proto.Amount,
                AmountCurrency = (SharedContracts.Enums.EnCurrency)proto.AmountCurrency,
                BallanceOperationType = (SharedContracts.Enums.EnBalanceOperationType)proto.BalanceOperationType,
                BalOpDateTime = DateUtils.ToDateTimeUtc(proto.BalOpDateTime),
                BalOpDateTimeMt4 = DateUtils.ToDateTimeUtc(proto.BalOpDateTimeMt4),
                Comment = proto.Comment,
                Login = proto.Login,
                Ticket = proto.Ticket,
                TradingPlatform = proto.TradingPlatform,
                UserGroup = proto.UserGroup,
                BaseCurrencyAmount = proto.BaseCurrencyAmount,
                BaseCurrency = (SharedContracts.Enums.EnCurrency)proto.BaseCurrency,
                SenderDateTime = DateUtils.ToDateTimeUtc(proto.SenderDateTime),
                CommentBytes = proto.CommentBytesList.SelectMany(x => x.ToByteArray()).ToArray(),
                ExpireDateTime = DateUtils.ToDateTimeUtc(proto.ExpireDateTime),
                ExpireDateTimeMt4 = DateUtils.ToDateTimeUtc(proto.ExpireDateTimeMt4),
                Guid = proto.Guid
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.BALOP; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.BackOffice.BalanceOperation); }
        }
    }
}
