﻿using System;
using Google.ProtocolBuffers;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class PrimeXMOrderOpenDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var primeXMOrderOpen = dataContract as SharedContracts.DataContracts.PrimeXM.PrimeXMOrderOpen;
            if (primeXMOrderOpen == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = PrimeXMOrderOpen.CreateBuilder()
                .SetOrderID(primeXMOrderOpen.orderID)
                .SetConnector(primeXMOrderOpen.connector)
                .SetProtocol(primeXMOrderOpen.protocol)
                .SetAccount(primeXMOrderOpen.account)
                .SetClOrdID(primeXMOrderOpen.clOrdID)
                .SetSubID1(primeXMOrderOpen.subID1)
                .SetSubID2(primeXMOrderOpen.subID2)
                .SetSubID3(primeXMOrderOpen.subID3)
                .SetSide(primeXMOrderOpen.side)
                .SetType(primeXMOrderOpen.type)
                .SetInstrument(primeXMOrderOpen.instrument)
                .SetSymbol(primeXMOrderOpen.symbol)
                .SetDigits(primeXMOrderOpen.digits)
                .SetCurrency(primeXMOrderOpen.currency)
                .SetSize(primeXMOrderOpen.size)
                .SetMinsize(primeXMOrderOpen.minsize)
                .SetPrice(primeXMOrderOpen.price)
                .SetDeviation(primeXMOrderOpen.deviation)
                .SetTtl(primeXMOrderOpen.ttl)
                .SetMid(primeXMOrderOpen.mid)
                .SetBid(primeXMOrderOpen.bid)
                .SetAsk(primeXMOrderOpen.ask)
                .SetPool(primeXMOrderOpen.pool)
                .SetProfile(primeXMOrderOpen.profile)
                .SetMinspread(primeXMOrderOpen.minspread)
                .SetMaxspread(primeXMOrderOpen.maxspread)
                .SetMarkupbid(primeXMOrderOpen.markupbid)
                .SetMarkupask(primeXMOrderOpen.markupask)
                .SetMilli(primeXMOrderOpen.milli)
                .SetMicro(primeXMOrderOpen.micro);

            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.PrimeXM.PrimeXMOrderOpen>("Contract type is not a PrimeXMOrderOpen!");
            }

            var proto = PrimeXMOrderOpen.ParseFrom(memStream);

            return new SharedContracts.DataContracts.PrimeXM.PrimeXMOrderOpen
            {
                orderID = proto.OrderID,
                connector = proto.Connector,
                protocol = proto.Protocol,
                account = proto.Account,
                clOrdID = proto.ClOrdID,
                subID1 = proto.SubID1,
                subID2 = proto.SubID2,
                subID3 = proto.SubID3,
                side = proto.Side,
                type = proto.Type,
                instrument = proto.Instrument,
                symbol = proto.Symbol,
                digits = proto.Digits,
                currency = proto.Currency,
                size = proto.Size,
                minsize = proto.Minsize,
                price = proto.Price,
                deviation = proto.Deviation,
                ttl = proto.Ttl,
                mid = proto.Mid,
                bid = proto.Bid,
                ask = proto.Ask,
                pool = proto.Pool,
                profile = proto.Profile,
                minspread = proto.Minspread,
                maxspread = proto.Maxspread,
                markupbid = proto.Markupbid,
                markupask = proto.Markupask,
                milli = proto.Milli,
                micro = proto.Micro
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.PRIMEXMORDEROPEN; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.PrimeXM.PrimeXMOrderOpen); }
        }
    }
}
