﻿using System;
using System.IO;
using System.Linq;
using Google.ProtocolBuffers;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using SharedContracts.DataContracts.TradingPlatform.TradingGw;


namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TradingGroupEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var tradeGroup = dataContract as SharedContracts.DataContracts.TradingPlatform.TradingGw.TradingGroup;
            if (tradeGroup == null) {
                return ByteString.Empty;
            }

            var protobuilder = TradingGroup.CreateBuilder()
                .SetName(tradeGroup.Name)
                .SetCurrency((EnCurrency) tradeGroup.Currency)
                .SetTradingPlatform(tradeGroup.TradingPlatform)
                .SetMarginCall(tradeGroup.MarginCall)
                .SetMarginStopOut(tradeGroup.MarginStopOut)
                .SetMarginType((EnMarginType) tradeGroup.MarginType)
                .SetGuid(tradeGroup.Guid)
                .SetLeverage(tradeGroup.Leverage)
                .SetEnable(tradeGroup.Enable);
                
            foreach (var securitySetting in tradeGroup.SecuritySettings)
            {
                var setting = SecuritySettings.CreateBuilder()
                    .SetIndex(securitySetting.Index)
                    .SetName(securitySetting.Name)
                    .SetEnable(securitySetting.Enable)
                    .SetCommbase(securitySetting.CommBase)
                    .SetCommtype((EnSecurityCommissionType)securitySetting.CommType)
                    .SetCommlots((EnSecurityCommissionLotMode)securitySetting.CommLots)
                    .SetCommagent(securitySetting.CommAgent)
                    .SetCommagenttype((EnSecurityCommissionType)securitySetting.CommAgentType)
                    .SetCommagentlots((EnSecurityCommissionLotMode)securitySetting.CommAgentLots)
                    .SetSpreaddiff(securitySetting.SpreadDifference)
                    .SetMinlots(securitySetting.MinLots)
                    .SetMaxlots(securitySetting.MaxLots)
                    .SetLotsstep(securitySetting.LotsStep)
                    .AddRangeInstrumentnames(securitySetting.Symbols)
                    .Build();

                protobuilder.SecsettingsList.Add(setting);
            }

            var proto = protobuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);
            
            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        { 
            if(encodedContract.Length == 0)
            {
                return null;
            }
        
            var buffer = new byte[encodedContract.Length];
            var memstream = new MemoryStream(buffer);

            encodedContract.WriteTo(memstream);
            memstream.Position = 0;
            var contractType = (EnDataContractType)memstream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.TradingGw.TradingGroup>("Contract type is not a Trading Group!");
            }

            var proto = TradingGroup.ParseFrom(memstream);

            var securitySettings = proto.SecsettingsList.Select(setting => new SecuritySetting
            {
                Index = setting.Index, 
                Name = setting.Name,
                Enable = setting.Enable,
                CommBase = setting.Commbase, 
                CommType = (SharedContracts.Enums.EnSecurityCommissionType) setting.Commtype,
                CommLots = (SharedContracts.Enums.EnSecurityCommissionLotMode) setting.Commlots, 
                CommAgent = setting.Commagent,
                CommAgentType = (SharedContracts.Enums.EnSecurityCommissionType) setting.Commagenttype, 
                CommAgentLots = (SharedContracts.Enums.EnSecurityCommissionLotMode) setting.Commagentlots, 
                SpreadDifference = setting.Spreaddiff,
                MinLots = setting.Minlots,
                MaxLots = setting.Maxlots,
                LotsStep = setting.Lotsstep,
                Symbols = setting.InstrumentnamesList
            }).ToList();

            return new SharedContracts.DataContracts.TradingPlatform.TradingGw.TradingGroup
            {
                Name = proto.Name,
                Currency = (SharedContracts.Enums.EnCurrency)proto.Currency,
                SecuritySettings = securitySettings,
                TradingPlatform = proto.TradingPlatform,
                MarginCall = proto.MarginCall,
                MarginStopOut = proto.MarginStopOut,
                MarginType = (SharedContracts.Enums.EnMarginType)proto.MarginType,
                Guid = proto.Guid,
                Leverage = proto.Leverage,
                Enable = proto.Enable
            };
        }

        public EnDataContractType DataContractType 
        {
            get { return EnDataContractType.TRADINGGROUP; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.TradingGw.TradingGroup); }
        }
    }
}
