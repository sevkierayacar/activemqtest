﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using SharedContracts.DataContracts.MasterAccountManager;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class MamEventDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var mamEvent = dataContract as MasterAccountManagerEvent;
            if (mamEvent == null)
            {
                return ByteString.Empty;
            }

            var proto = MAMEvent.CreateBuilder()
                                .SetTradingPlatform(mamEvent.TradingPlatform)
                                .SetMasterLogin(mamEvent.MasterLogin)
                                .SetClientLogin(mamEvent.ClientLogin)
                                .SetMasterTicket(mamEvent.MasterTicket)
                                .SetClientTicket(mamEvent.ClientTicket)
                                .SetDate(DateUtils.ToJavaTimeUtc(mamEvent.Date))
                                .SetLog(mamEvent.Log)
                                .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<MasterAccountManagerEvent>("Contract type is not a master account event!");
            }

            var proto = MAMEvent.ParseFrom(memStream);

            return new MasterAccountManagerEvent
            {
                TradingPlatform = proto.TradingPlatform,
                MasterLogin = proto.MasterLogin,
                ClientLogin = proto.ClientLogin,
                MasterTicket = proto.MasterTicket,
                ClientTicket = proto.ClientTicket,
                Date = DateUtils.ToDateTimeUtc(proto.Date),
                Log = proto.Log
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MAMEVENT; }
        }

        public Type PayloadType
        {
            get { return typeof(MasterAccountManagerEvent); }
        }
    }
}
