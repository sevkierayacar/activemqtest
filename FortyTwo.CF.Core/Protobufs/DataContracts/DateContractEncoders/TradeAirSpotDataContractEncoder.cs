﻿
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using System.Linq;
using SharedContracts.DataContracts.Ticks;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TradeAirSpotDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var tradeAirSpot = dataContract as SharedContracts.DataContracts.TradingPlatform.TradeAirSpot;
            if (tradeAirSpot == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = TradeAirSpot.CreateBuilder()
                .SetTimeStamp(DateUtils.ToJavaTimeUtc(tradeAirSpot.TimeStamp))
                .SetTradeReportId(tradeAirSpot.TradeReportId)
                .SetSymbol(tradeAirSpot.Symbol)
                .SetTradeId(tradeAirSpot.TradeId)
                .SetPrice(tradeAirSpot.Price)
                .SetLastSpotPrice(tradeAirSpot.LastSpotPrice)
                .SetTradingParty(
                    PartyInfo.CreateBuilder()
                        .SetDealOperation((EnDealOperation) tradeAirSpot.TradingPartyInfo.DealOperation)
                        .SetPartyId(tradeAirSpot.TradingPartyInfo.PartyId)
                        .SetPartySubId(tradeAirSpot.TradingPartyInfo.PartySubId)
                        .SetOrderId(tradeAirSpot.TradingPartyInfo.OrderId)
                        .SetClientOrderId(tradeAirSpot.TradingPartyInfo.ClientOrderId)
                        .SetPartyRole(tradeAirSpot.TradingPartyInfo.PartyRole)
                        .SetPartyIdSource(tradeAirSpot.TradingPartyInfo.PartyIdSource)
                        .SetSettlCurAmount(tradeAirSpot.TradingPartyInfo.SettlCurAmount)
                        .SetCurrency(tradeAirSpot.TradingPartyInfo.Currency).Build()
                )
                .SetCounterParty(
                    PartyInfo.CreateBuilder()
                        .SetDealOperation((EnDealOperation) tradeAirSpot.CounterPartyInfo.DealOperation)
                        .SetPartyId(tradeAirSpot.CounterPartyInfo.PartyId)
                        .SetPartySubId(tradeAirSpot.CounterPartyInfo.PartySubId)
                        .SetOrderId(tradeAirSpot.CounterPartyInfo.OrderId)
                        .SetClientOrderId(tradeAirSpot.CounterPartyInfo.ClientOrderId)
                        .SetPartyRole(tradeAirSpot.CounterPartyInfo.PartyRole)
                        .SetPartyIdSource(tradeAirSpot.CounterPartyInfo.PartyIdSource)
                        .SetSettlCurAmount(tradeAirSpot.CounterPartyInfo.SettlCurAmount)
                        .SetCurrency(tradeAirSpot.CounterPartyInfo.Currency).Build()
                );
            
            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.TradeAirSpot>("Contract type is not a TradeAirSpot!");
            }

            var proto = TradeAirSpot.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.TradeAirSpot
            {
                TradeReportId = proto.TradeReportId,
                TradeId = proto.TradeId,
                TimeStamp =  DateUtils.ToDateTimeUtc(proto.TimeStamp),
                Symbol = proto.Symbol,
                Price = proto.Price,
                LastSpotPrice = proto.LastSpotPrice,
                TradingPartyInfo = ToLocalPartyInfo(proto.TradingParty),
                CounterPartyInfo = ToLocalPartyInfo(proto.CounterParty)
            };
        }

        public SharedContracts.DataContracts.TradingPlatform.PartyInfo ToLocalPartyInfo(PartyInfo proto)
        {
            return new SharedContracts.DataContracts.TradingPlatform.PartyInfo
            {
                DealOperation = (SharedContracts.Enums.EnDealOperation) proto.DealOperation,
                PartyId = proto.PartyId,
                PartySubId = proto.PartySubId,
                OrderId = proto.OrderId,
                ClientOrderId = proto.ClientOrderId,
                PartyRole = proto.PartyRole,
                PartyIdSource = proto.PartyIdSource,
                SettlCurAmount = proto.SettlCurAmount,
                Currency = proto.Currency
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.TRADEAIRSPOT; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.TradeAirSpot); }
        }
    }
}