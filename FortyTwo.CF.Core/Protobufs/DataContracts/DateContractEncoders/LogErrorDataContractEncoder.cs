﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class LogErrorDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var logError = dataContract as SharedContracts.DataContracts.TradingPlatform.LogError;
            if (logError == null)
            {
                return ByteString.Empty;
            }

            var proto = LogError.CreateBuilder()
                                        .SetLog(logError.Log ?? string.Empty)
                                        .SetTradingPlatform(logError.TradingPlatform)
                                        .SetGuid(logError.Guid)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.LogError>("Contract type is not a Log Error!");
            }

            var proto = LogError.ParseFrom(memStream);

            return
                new SharedContracts.DataContracts.TradingPlatform.LogError(proto.TradingPlatform, proto.Log, proto.Guid);
        }

        public EnDataContractType DataContractType { get { return EnDataContractType.LOGERROR; } }
        public Type PayloadType { get { return typeof(SharedContracts.DataContracts.TradingPlatform.LogError); } }
    }
}
