﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using SharedContracts.DataContracts.TradingPlatform;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class SwapEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var swap = dataContract as Swap;
            if (swap == null)
            {
                return ByteString.Empty;
            }

            var proto = PosSwap.CreateBuilder()
                                        .SetAmountCurrency((EnCurrency)swap.AmountCurrency)
                                        .SetAmount(swap.Amount)
                                        .SetAmountTotal(swap.AmountTotal)
                                        .SetBaseCurrency((EnCurrency)swap.BaseCurrency)
                                        .SetBaseCurrencyAmount(swap.BaseCurrencyAmount)
                                        .SetBaseCurrencyAmountTotal(swap.BaseCurrencyAmountTotal)
                                        .SetSwapDateTime(DateUtils.ToJavaTimeUtc(swap.TimeStamp))
                                        .SetSwapDateTimeMt4(DateUtils.ToJavaTimeUtc(swap.TimeStampMt4))
                                        .SetSenderDateTime(DateUtils.ToJavaTimeUtc(swap.SenderDateTime))
                                        .SetLogin(swap.Login)
                                        .SetSymbol(swap.Symbol)
                                        .SetUserGroup(swap.UserGroup)
                                        .SetTradingPlatform(swap.TradingPlatform)
                                        .SetIsClosed(swap.IsClosed)
                                        .SetTicket(swap.Ticket)
                                        .SetGuid(swap.Guid)
                                        .SetDealId(swap.DealId ?? string.Empty)
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);

        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<Swap>("Contract type is not a swap!");
            }

            var proto = PosSwap.ParseFrom(memStream);

            return new Swap
            {
                Amount = proto.Amount,
                AmountTotal = proto.AmountTotal,
                AmountCurrency = (SharedContracts.Enums.EnCurrency)proto.AmountCurrency,
                BaseCurrencyAmount = proto.BaseCurrencyAmount,
                BaseCurrencyAmountTotal = proto.BaseCurrencyAmountTotal,
                BaseCurrency = (SharedContracts.Enums.EnCurrency)proto.BaseCurrency,
                TimeStamp = DateUtils.ToDateTimeUtc(proto.SwapDateTime),
                TimeStampMt4 = DateUtils.ToDateTimeUtc(proto.SwapDateTimeMt4),
                Login = proto.Login,
                Symbol = proto.Symbol,
                TradingPlatform = proto.TradingPlatform,
                UserGroup = proto.UserGroup,
                IsClosed = proto.IsClosed,
                Ticket = proto.Ticket,
                SenderDateTime = DateUtils.ToDateTimeUtc(proto.SenderDateTime),
                Guid = proto.Guid,
                DealId = proto.DealId ?? string.Empty
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.SWAP; }
        }

        public Type PayloadType
        {
            get { return typeof(Swap); }
        }
    }
}
