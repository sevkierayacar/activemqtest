﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class Mt4StartUpEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var mt4StartUp = dataContract as SharedContracts.DataContracts.TradingPlatform.Mt4StartUp;
            if (mt4StartUp == null)
            {
                return ByteString.Empty;
            }

            var proto = Mt4StartUp.CreateBuilder()
                            .SetTradingPlatform(mt4StartUp.TradingPlatform)
                            .SetDate(DateUtils.ToJavaTimeUtc(mt4StartUp.Date))
                            .SetGuid(mt4StartUp.Guid)
                            .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.Mt4StartUp>("Contract type is not a Mt4StartUp!");
            }

            var proto = Mt4StartUp.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.Mt4StartUp(proto.TradingPlatform, DateUtils.ToDateTimeUtc(proto.Date), proto.Guid);
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.MT4STARTUP; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.Mt4StartUp); }
        }
    }
}
