﻿using System.Linq;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class TradingPlatformUserEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var platformUser = dataContract as SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser;
            if (platformUser == null)
            {
                return ByteString.Empty;
            }

            var proto = TradingPlatformUser
                .CreateBuilder()
                .SetAccountCcy((EnCurrency)platformUser.AccountCcy)
                .SetComment(platformUser.Comment)
                .SetEmail(platformUser.Email)
                .SetGroup(platformUser.Group)
                .SetId(platformUser.Id)
                .SetLeverage(platformUser.Leverage)
                .SetLogin(platformUser.Login)
                .SetName(platformUser.Name)
                .SetBalance(platformUser.Balance)
                .SetCredit(platformUser.Credit)
                .SetEquity(platformUser.Equity)
                .SetMargin(platformUser.Margin)
                .SetTradingPlatform(platformUser.TradingPlatform)
                .AddCommentBytes(platformUser.CommentBytes != null ? ByteString.CopyFrom(platformUser.CommentBytes) : ByteString.Empty)
                .SetCountry(platformUser.Country)
                .SetPhone(platformUser.Phone)
                .SetCity(platformUser.City)
                .SetState(platformUser.State)
                .SetStatus(platformUser.Status)
                .SetZipcode(platformUser.ZipCode)
                .SetLastip(platformUser.LastIp)
                .SetAddress(platformUser.Address)
                .SetRegdate(DateUtils.ToJavaTimeUtc(platformUser.RegDate))
                .SetRegdatemt4(DateUtils.ToJavaTimeUtc(platformUser.RegDateMt4))
                .SetLastdate(DateUtils.ToJavaTimeUtc(platformUser.LastDate))
                .SetEnable(platformUser.Enable)
                .SetGuid(platformUser.Guid)
                .SetReadonly(platformUser.Readonly)
                .SetAgent(platformUser.Agent)
                .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);
            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser>("Contract type is not a TradingPlatformUser!");
            }

            var proto = TradingPlatformUser.ParseFrom(memStream);
            return new SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser(
                proto.TradingPlatform,
                proto.Login,
                proto.Group,
                proto.Name,
                proto.Email,
                proto.Comment,
                proto.Id,
                proto.Leverage,
                (SharedContracts.Enums.EnCurrency)proto.AccountCcy,
                proto.Country,
                proto.Phone, proto.City, proto.State, proto.Status, proto.Zipcode, proto.Lastip, proto.Address, DateUtils.ToDateTimeUtc(proto.Regdate), DateUtils.ToDateTimeUtc(proto.Regdatemt4), DateUtils.ToDateTimeUtc(proto.Lastdate), proto.Enable, proto.Readonly, proto.Agent, proto.Guid, DateUtils.ToDateTimeUtc(proto.SenderDateTime))
            {
                Balance = proto.Balance,
                Credit = proto.Credit,
                Equity = proto.Equity,
                Margin = proto.Margin,
                CommentBytes = proto.CommentBytesList.SelectMany(x => x.ToByteArray()).ToArray()
            };
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.USERRECORD; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser); }
        }
    }
}
