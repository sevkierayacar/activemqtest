﻿using System;
using System.IO;
using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class QuoteDataContractEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var quote = dataContract as SharedContracts.DataContracts.Ticks.Quote;
            if (quote == null)
            {
                return ByteString.Empty;
            }

            var proto = Quote.CreateBuilder()
                                        .SetTickSource(quote.TickSource)
                                        .SetSymbol(quote.Symbol)
                                        .SetAsk(quote.Ask)
                                        .SetBid(quote.Bid)
                                        .SetOrigTime(DateUtils.ToJavaTimeUtc(quote.TimeStamp))
                                        .Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.Ticks.Quote>("Contract type is not a quote!");
            }

            var proto = Quote.ParseFrom(memStream);

            return new SharedContracts.DataContracts.Ticks.Quote(proto.TickSource,
                                                                proto.Symbol,
                                                                proto.Bid,
                                                                proto.Ask,
                                                                DateUtils.ToDateTimeUtc(proto.OrigTime));
        }

        public EnDataContractType DataContractType { get { return EnDataContractType.QUOTE;} }
        public Type PayloadType { get { return typeof (SharedContracts.DataContracts.Ticks.Quote); } }
    }
}
