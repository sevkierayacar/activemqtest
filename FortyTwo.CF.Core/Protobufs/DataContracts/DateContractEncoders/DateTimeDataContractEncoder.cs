﻿using System;
using System.IO;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{

    public class DateTimeDataContractEncoder : ValueTypeDataContractEncoderBase<DateTime>
    {
        public override EnDataContractType DataContractType
        {
            get { return EnDataContractType.DateTime; }
        }

        public override byte[] GetBytesForValue(object value)
        {
            return BitConverter.GetBytes(DateUtils.ToJavaTimeUtc((DateTime)value));
        }

        public override object GetObjectFromBytes(MemoryStream memStream)
        {
            return DateUtils.ToDateTimeUtc(new BinaryReader(memStream).ReadInt64());
        }
    }
}
