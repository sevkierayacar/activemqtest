﻿using FortyTwo.CF.Core.Utils;
using FortyTwo.CF.Encoders.Protobuf.DataContracts.Exceptions;
using Google.ProtocolBuffers;
using System;
using System.IO;
using System.Linq;
using SharedContracts.DataContracts.TradingPlatform;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    public class SymbolSecurityGroupDataEncoder : IDataContractEncoder
    {
        public ByteString EncodeDataContract(object dataContract)
        {
            var symbolSecurityGroup = dataContract as SharedContracts.DataContracts.TradingPlatform.SymbolSecurityGroup;
            if (symbolSecurityGroup == null)
            {
                return ByteString.Empty;
            }

            var protoBuilder = SymbolSecurityGroup.CreateBuilder()
                .SetTradingPlatform(symbolSecurityGroup.TradingPlatform)
                .SetIndex(symbolSecurityGroup.Index)
                .SetName(symbolSecurityGroup.Name)
                .SetDescription(symbolSecurityGroup.Description)
                .SetGuid(symbolSecurityGroup.Guid);

            var proto = protoBuilder.Build();

            var buffer = new byte[proto.SerializedSize + StreamExtension.TypeDataContractTypeSize];
            var memStream = new MemoryStream(buffer);
            memStream.WriteMessageType((long)DataContractType);
            proto.WriteTo(memStream);

            return ByteString.CopyFrom(buffer);
        }

        public object DecodeDataContract(ByteString encodedContract)
        {
            if (encodedContract.Length == 0)
            {
                return null;
            }

            var buffer = new byte[encodedContract.Length];
            var memStream = new MemoryStream(buffer);

            encodedContract.WriteTo(memStream);
            memStream.Position = 0;
            var contractType = (EnDataContractType)memStream.ReadDataContractType();

            if (contractType != DataContractType)
            {
                throw new DataContractDecodingException<SharedContracts.DataContracts.TradingPlatform.SymbolSecurityGroup>("Contract type is not a symbolSecurityGroup!");
            }

            var proto = SymbolSecurityGroup.ParseFrom(memStream);

            return new SharedContracts.DataContracts.TradingPlatform.SymbolSecurityGroup(proto.TradingPlatform,proto.Index,proto.Name,proto.Description, proto.Guid);
        }

        public EnDataContractType DataContractType
        {
            get { return EnDataContractType.SYMBOLSECURITYGROUP; }
        }

        public Type PayloadType
        {
            get { return typeof(SharedContracts.DataContracts.TradingPlatform.SymbolSecurityGroup); }
        }
    }
}
