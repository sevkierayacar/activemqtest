﻿using Google.ProtocolBuffers;
using System;

namespace FortyTwo.CF.Encoders.Protobuf.DataContracts.DateContractEncoders
{
    /// <summary>
    /// Data contract encoder for a specific data contract type
    /// </summary>
    public interface IDataContractEncoder
    {
        /// <summary>
        /// Encodes the data contract.
        /// </summary>
        /// <param name="dataContract">The data contract.</param>
        /// <returns>
        /// The encoded ByteString
        /// </returns>
        ByteString EncodeDataContract(object dataContract);

        /// <summary>
        /// Decodes the data contract.
        /// </summary>
        /// <param name="encodedContract">The encoded contract.</param>
        /// <returns>
        /// The decoded DataContract
        /// </returns>
        object DecodeDataContract(ByteString encodedContract);

        /// <summary>
        /// Gets the type of the data contract that can be encoded with this encoder.
        /// </summary>
        /// <value>
        /// The type of the data contract.
        /// </value>
        EnDataContractType DataContractType { get; }

        /// <summary>
        /// Gets the C# type of the payload.
        /// </summary>
        /// <value>
        /// The type of the payload.
        /// </value>
        Type PayloadType { get; }
    }
}
