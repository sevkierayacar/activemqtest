﻿using System;

namespace FortyTwo.CF.Encoders.Protobuf
{
    public static class DateUtils
    {
        /// <summary>
        /// The start of the Windows epoch
        /// </summary>
        public static readonly DateTime WindowsEpoch = new DateTime(1601, 1, 1, 0, 0, 0, 0);
        /// <summary>
        /// The start of the Java epoch
        /// </summary>
        public static readonly DateTime JavaEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        /// <summary>
        /// The difference between the Windows epoch and the Java epoch
        /// in milliseconds.
        /// </summary>
        public static readonly long EpochDiff; /* 1164447360000L */

        static DateUtils()
        {
            EpochDiff = (JavaEpoch.ToFileTimeUtc() - WindowsEpoch.ToFileTimeUtc())
                            / TimeSpan.TicksPerMillisecond;
        }

        public static long ToJavaTimeUtc(DateTime dateTime)
        {
            return (dateTime.ToFileTimeUtc() / TimeSpan.TicksPerMillisecond) - EpochDiff;
        }

        public static DateTime ToDateTimeUtc(long javaTime)
        {
            return DateTime.FromFileTimeUtc((javaTime + EpochDiff) * TimeSpan.TicksPerMillisecond);
        }
    }
}
