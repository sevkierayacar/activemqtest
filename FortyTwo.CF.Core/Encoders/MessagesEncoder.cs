﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FortyTwo.CF.Core.Encoders.Exceptions;
using FortyTwo.CF.Core.Messages;
using FortyTwo.CF.Core.Utils;

namespace FortyTwo.CF.Core.Encoders
{
    public abstract class MessagesEncoder : IMessagesEncoder
    {
        public byte[] Encode(MessageBase message)
        {
            return Encode(new[] { message });
        }

        public byte[] Encode(IEnumerable<MessageBase> messageList)
        {
            var buffers = new List<byte[]>();

            foreach (var message in messageList)
            {
                var dcTypeLengthSize = StreamExtension.TypeDataContractLengthSize;
                var encodedMessage = EncodeSingle(message);
                var buffer = new byte[dcTypeLengthSize + encodedMessage.Length];
                var messageStream = new MemoryStream(buffer);
                messageStream.WriteMessageLength(encodedMessage.Length);
                messageStream.Write(encodedMessage, 0, encodedMessage.Length);
                buffers.Add(buffer);
            }

            var totalBuffer = new byte[buffers.Sum(buffer => buffer.Length)];
            var offset = 0;

            foreach (var buffer in buffers)
            {
                Buffer.BlockCopy(buffer, 0, totalBuffer, offset, buffer.Length);
                offset += buffer.Length;
            }

            return totalBuffer;
        }

        public IEnumerable<MessageBase> Decode(byte[] data)
        {
            var position = 0;
            while (position < data.Length)
            {
                int payloadLength;
                try
                {
                    payloadLength = data.ReadLength(position);
                    position += 4;
                }
                catch (Exception e)
                {
                    throw new MessageDecodingException<MessageBase>(e.Message, e);
                }

                var message = DecodeSingle(data, position, payloadLength);
                yield return message;
                position += payloadLength;
            }
        }

        protected abstract byte[] EncodeSingle(MessageBase message);
        protected abstract MessageBase DecodeSingle(byte[] messagebuffer, int index, int count);
    }
}