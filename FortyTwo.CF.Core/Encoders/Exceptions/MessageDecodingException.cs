﻿using FortyTwo.CF.Core.Messages;
using System;

namespace FortyTwo.CF.Core.Encoders.Exceptions
{
    public class MessageDecodingException<TMessage> : Exception where TMessage : MessageBase
    {
        public MessageDecodingException(string msg) : base(msg) { }
        public MessageDecodingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
