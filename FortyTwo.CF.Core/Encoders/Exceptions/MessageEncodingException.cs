﻿using FortyTwo.CF.Core.Messages;
using System;

namespace FortyTwo.CF.Core.Encoders.Exceptions
{
    public class MessageEncodingException<TMessage> : Exception where TMessage : MessageBase
    {
        public MessageEncodingException(string msg) : base(msg) { }
        public MessageEncodingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
