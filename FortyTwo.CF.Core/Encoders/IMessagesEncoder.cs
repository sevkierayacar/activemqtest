﻿using FortyTwo.CF.Core.Messages;
using System.Collections.Generic;

namespace FortyTwo.CF.Core.Encoders
{
    public interface IMessagesEncoder
    {
        byte[] Encode(MessageBase message);
        byte[] Encode(IEnumerable<MessageBase> messageList);

        IEnumerable<MessageBase> Decode(byte[] data);
    }
}
