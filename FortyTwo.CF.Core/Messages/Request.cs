﻿namespace FortyTwo.CF.Core.Messages
{
    public class Request : MessageBase
    {
        public string CorrelationId { get; set; }
        public string ActionName { get; set; }
        public object[] ActionParameters { get; set; }
    }
}
