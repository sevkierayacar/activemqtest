﻿using System;

namespace FortyTwo.CF.Core.Messages
{
    public class BytesMessage
    {
        public BytesMessage(byte[] data)
        {
            Data = data;
        }

        public BytesMessage(byte[] data, int index, int length)
        {
            Data = new byte[length];
            Array.Copy(data, index, Data, 0, length);
        }

        public byte[] Data { get; private set; }
    }
}
