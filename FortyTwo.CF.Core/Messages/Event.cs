﻿namespace FortyTwo.CF.Core.Messages
{
    public class Event : MessageBase
    {
        public object Payload { get; set; }
        public string EventType { get; set; }
    }
}
