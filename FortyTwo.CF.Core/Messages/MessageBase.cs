﻿using System;

namespace FortyTwo.CF.Core.Messages
{
    public abstract class MessageBase
    {
        public DateTime TimeStamp { get; set; }
    }
}
