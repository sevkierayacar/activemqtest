﻿namespace FortyTwo.CF.Core.Messages
{
    public class Response : MessageBase
    {
        public bool Success { get; set; }
        public object Payload { get; set; }
        public string ErrorMessage { get; set; }
        public string CorrelationId { get; set; }
    }
}
