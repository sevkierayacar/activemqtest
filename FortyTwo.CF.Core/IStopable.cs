﻿namespace FortyTwo.CF.Core
{
    public interface IStopable
    {
        void Stop();
    }
}
