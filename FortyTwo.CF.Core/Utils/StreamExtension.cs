﻿using System;
using System.IO;

namespace FortyTwo.CF.Core.Utils
{
    public static class StreamExtension
    {
        public static void WriteMessageLength(this MemoryStream memStream, Int32 contentLenght)
        {
            memStream.Write(BitConverter.GetBytes(contentLenght), 0, sizeof(Int32));
        }

        public static int TypeDataContractLengthSize { get { return sizeof(Int32); } }

        public static Int32 ReadLength(this MemoryStream memStream)
        {
            var result = new BinaryReader(memStream).ReadInt32();
            return result;
        }

        public static int ReadLength(this byte[] data, int position)
        {
            return BitConverter.ToInt32(data, position);
        }

        public static void WriteMessageType(this MemoryStream memStream, long messageType)
        {
            memStream.Write(BitConverter.GetBytes((Byte)messageType), 0, sizeof(Byte));
        }

        public static int TypeDataContractTypeSize { get { return sizeof(Byte); } }

        public static Byte ReadDataContractType(this MemoryStream memStream)
        {
            return new BinaryReader(memStream).ReadByte();
        }
    }
}
