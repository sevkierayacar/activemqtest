﻿using System;

namespace FortyTwo.CF.Core.Utils
{
    public interface IChunkedMessageChannelListener
    {
        void BeforeSend(string channelName, int itemsInChunk);

        void AfterSend(string channelName, int itemsInChunk);

        void OnError(string channelName, Exception ex);
    }
}
