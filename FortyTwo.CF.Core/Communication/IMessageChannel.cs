﻿using FortyTwo.CF.Core.Messages;
using System;

namespace FortyTwo.CF.Core.Communication
{
    /// <summary>
    /// Messages channel used for sending messages to remove listeners on this channel
    /// </summary>
    public interface IMessageChannel
    {
        /// <summary>
        /// Sends a message in the channel.
        /// </summary>
        /// <param name="message">The message.</param>
        void SendMessage(MessageBase message);

        /// <summary>
        /// Occurs when an Request arrives on the channel.
        /// </summary>
        event EventHandler<Request> OnRequest;

        /// <summary>
        /// Occurs when an Response arrives on the channel.
        /// </summary>
        event EventHandler<Response> OnResponse;

        /// <summary>
        /// Occurs when an event arrives on the channel.
        /// </summary>
        event EventHandler<Event> OnEvent;

        /// <summary>
        /// Gets the type of the messages encoder.
        /// </summary>
        /// <value>
        /// The type of the messages encoder.
        /// </value>
        Type EncoderType { get; }

        /// <summary>
        /// Gets the channel direction.
        /// </summary>
        /// <value>
        /// The channel direction.
        /// </value>
        EnChannelDirection ChannelDirection { get; }

        /// <summary>
        /// Gets the name of the channel.
        /// </summary>
        /// <value>
        /// The name of the channel.
        /// </value>
        string ChannelName { get; }
    }
}
