﻿using FortyTwo.CF.Core.Messages;
using System;

namespace FortyTwo.CF.Core.Communication
{
    /// <summary>
    /// Binary channel providing low level transport for encoded messages
    /// </summary>
    public interface IBinaryChannel : IStartable, IStopable
    {
        /// <summary>
        /// Sends an bytes(encoded) message.
        /// </summary>
        /// <param name="message">The message.</param>
        void SendMessage(BytesMessage message);

        /// <summary>
        /// Occurs when an bytes(encoded) message arrives.
        /// </summary>
        event EventHandler<BytesMessage> OnMessage;

        /// <summary>
        /// Gets the channel direction.
        /// </summary>
        /// <value>
        /// The channel direction.
        /// </value>
        EnChannelDirection ChannelDirection { get; }

        /// <summary>
        /// Gets the name of the channel.
        /// </summary>
        /// <value>
        /// The name of the channel.
        /// </value>
        string ChannelName { get; }
    }
}
