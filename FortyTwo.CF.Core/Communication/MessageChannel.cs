﻿using FortyTwo.CF.Core.Encoders;
using FortyTwo.CF.Core.Messages;
using System;
using System.Collections.Generic;

namespace FortyTwo.CF.Core.Communication
{
    /// <summary>
    /// General messages channel. Takes an encoder and binary channel and wraps the communication process in both directions
    /// </summary>
    public class MessageChannel : IMessageChannel
    {
        #region Private fields

        private readonly IBinaryChannel _binaryChannel;

        private readonly IMessagesEncoder _messagesEncoder;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates new instance of MessagesChannel
        /// </summary>
        /// <param name="binaryChannel">Implementation of binary channel</param>
        /// <param name="messagesEncoder">Implementation of messages encoder</param>
        public MessageChannel(IBinaryChannel binaryChannel, IMessagesEncoder messagesEncoder)
        {
            _binaryChannel = binaryChannel;
            _messagesEncoder = messagesEncoder;

            _binaryChannel.OnMessage += binaryChannel_OnMessage;
        }

        #endregion

        #region IMessageChannel implementation

        /// <summary>
        /// Triggers when a request message is received
        /// </summary>
        public event EventHandler<Request> OnRequest;

        /// <summary>
        /// Triggers when a response message is received
        /// </summary>
        public event EventHandler<Response> OnResponse;

        /// <summary>
        /// Triggers when an event is received
        /// </summary>
        public event EventHandler<Event> OnEvent;

        /// <summary>
        /// Encodes and sends any base message into the binary channel
        /// </summary>
        /// <param name="message">The message to encode and send</param>
        public void SendMessage(MessageBase message)
        {
            var messageBuffer = _messagesEncoder.Encode(message);
            var encodedMessage = new BytesMessage(messageBuffer);
            _binaryChannel.SendMessage(encodedMessage);
        }

        public Type EncoderType => _messagesEncoder.GetType();

        public EnChannelDirection ChannelDirection => _binaryChannel.ChannelDirection;

        public string ChannelName => _binaryChannel.ChannelName;

        #endregion

        #region Private methods

        private void binaryChannel_OnMessage(object sender, BytesMessage e)
        {
            IEnumerable<MessageBase> baseMessages;

            try
            {
                //byte[] buffer = e.Data;
                //char[] chars = new char[buffer.Length / sizeof(char)];
                //System.Buffer.BlockCopy(buffer, 0, chars, 0, buffer.Length);
                //string str = new string(chars);

                baseMessages = _messagesEncoder.Decode(e.Data);
            }
            catch (NotImplementedException)
            {
                // skip since I can't decode
                return;
            }

            foreach (var baseMessage in baseMessages)
            {
                var req = baseMessage as Request;
                if (req != null)
                {
                    var onRequest = OnRequest;
                    onRequest?.Invoke(this, req);
                    continue;
                }

                var resp = baseMessage as Response;
                if (resp != null)
                {
                    var onResponse = OnResponse;
                    onResponse?.Invoke(this, resp);
                    continue;
                }

                var ev = baseMessage as Event;
                if (ev != null)
                {
                    var onEvent = OnEvent;
                    onEvent?.Invoke(this, ev);
                    continue;
                }

                throw new InvalidOperationException("The message is of unknown origin");
            }
        }

        #endregion
    }
}
