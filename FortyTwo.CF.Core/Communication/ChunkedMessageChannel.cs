﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using FortyTwo.CF.Core.Encoders;
using FortyTwo.CF.Core.Messages;
using FortyTwo.CF.Core.Utils;

namespace FortyTwo.CF.Core.Communication
{
    public class ChunkedMessageChannel : IMessageChannel
    {
        #region Private fields

        private readonly IBinaryChannel _binaryChannel;
        private readonly IMessagesEncoder _messagesEncoder;
        private readonly int _maxChunkSize;
        private readonly IChunkedMessageChannelListener _chunkedMessageChannelListener;
        private readonly Queue<MessageBase> _messageQueue;
        private readonly object _syncRoot = new object();
        private readonly Timer _timer = new Timer();

        #endregion

        #region Constructor

        public ChunkedMessageChannel(IBinaryChannel binaryChannel, IMessagesEncoder messagesEncoder, int maxChunkSize,
            TimeSpan sendTimeout, IChunkedMessageChannelListener chunkedMessageChannelListener)
        {
            _binaryChannel = binaryChannel;
            _messagesEncoder = messagesEncoder;
            _maxChunkSize = maxChunkSize;
            _chunkedMessageChannelListener = chunkedMessageChannelListener;
            _messageQueue = new Queue<MessageBase>();
            _timer.Elapsed += TimerAdapterOnTimerElapsed;
            _timer.AutoReset = true;
            _timer.Interval = sendTimeout.TotalMilliseconds;
            _timer.Start();
        }

        #endregion

        #region IMessageChannel implementation

        public event EventHandler<Request> OnRequest;

        public event EventHandler<Response> OnResponse;

        public event EventHandler<Event> OnEvent;


        public void SendMessage(MessageBase message)
        {
            lock (_syncRoot)
            {
                _messageQueue.Enqueue(message);
                if (_messageQueue.Count > _maxChunkSize)
                    SendMessagesInQueue();
            }
        }

        public Type EncoderType => _messagesEncoder.GetType();

        public EnChannelDirection ChannelDirection => _binaryChannel.ChannelDirection;

        public string ChannelName => _binaryChannel.ChannelName;

        #endregion

        #region Private Methods

        private void SendChunkMessages(IEnumerable<MessageBase> messages)
        {
            try
            {
                _chunkedMessageChannelListener?.BeforeSend(_binaryChannel.ChannelName, messages.Count());
                var messageBuffer = _messagesEncoder.Encode(messages);
                var encodedMessage = new BytesMessage(messageBuffer);
                _binaryChannel.SendMessage(encodedMessage);
                _chunkedMessageChannelListener?.AfterSend(_binaryChannel.ChannelName, messages.Count());
            }
            catch (Exception ex)
            {
                _chunkedMessageChannelListener?.OnError(_binaryChannel.ChannelName, ex);
            }
        }

        private void SendMessagesInQueue()
        {
            IEnumerable<MessageBase> messages;
            lock (_syncRoot)
            {
                if (!_messageQueue.Any())
                    return;
                messages = new Queue<MessageBase>(_messageQueue);
                _messageQueue.Clear();
            }
            SendChunkMessages(messages);
        }

        private void TimerAdapterOnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            SendMessagesInQueue();
        }

        #endregion
    }
}
