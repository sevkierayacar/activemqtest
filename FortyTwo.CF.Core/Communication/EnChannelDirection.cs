﻿
namespace FortyTwo.CF.Core.Communication
{
    /// <summary>
    /// Enumeration for different channel communication direction
    /// </summary>
    public enum EnChannelDirection
    {
        /// <summary>
        /// Incoming messages channel
        /// </summary>
        In,

        /// <summary>
        /// Outgoing messages channel
        /// </summary>
        Out,

        /// <summary>
        /// Incoming and outgoing messages channel
        /// </summary>
        InOut
    }
}
