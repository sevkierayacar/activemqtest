﻿using System.Reflection.Emit;

namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Class for creating a module builder.
    /// </summary>
    internal static class DynamicModuleBuilder
    {
        /// <summary>
        /// Creates a module builder using the cached assembly.
        /// </summary>
        public static ModuleBuilder Create()
        {
            var assemblyName = DynamicAssemblyCache.Get.GetName().Name;
            var moduleBuilder = DynamicAssemblyCache.Get.DefineDynamicModule(assemblyName);

            DynamicModuleCache.Add(moduleBuilder);

            return moduleBuilder;
        }
    }
}