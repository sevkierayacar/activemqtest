﻿using FortyTwo.CF.Core.Communication;
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;

namespace FortyTwo.CF.Core.ChannelFactory
{
    public static class DynamicProxyBuilder<TService>
    {
        public static Type CreateProxy()
        {
            var proxyInterface = typeof(TService);
            // Define proxy base type
            var baseProxyType = typeof(DynamicProxyBase<TService>);
            // Define typebuilder
            var typeBuilder = DynamicModuleCache.Get.DefineType(
                string.Format("{0}.{1}ClientProxy", proxyInterface.Namespace, proxyInterface.FullName),
                TypeAttributes.Public,
                baseProxyType,
                new[] { proxyInterface });

            // Define empty constructor for the proxy
            var constructorBuilder = typeBuilder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                new[] { typeof(IMessageChannel), typeof(int), typeof(bool) });

            var baseConstructor = baseProxyType.GetConstructor(new[] { typeof(IMessageChannel), typeof(int), typeof(bool) });

            if (baseConstructor == null)
            {
                throw new MissingMethodException(string.Format("DynamicProxyBase<{0}>", proxyInterface.Name), "Constructor");
            }

            var constrcutorILGenerator = constructorBuilder.GetILGenerator();

            // Get this constructor's parameter and send it to the base constructor
            constrcutorILGenerator.Emit(OpCodes.Ldarg_0);
            constrcutorILGenerator.Emit(OpCodes.Ldarg_1);
            constrcutorILGenerator.Emit(OpCodes.Ldarg_2);
            constrcutorILGenerator.Emit(OpCodes.Ldarg_3);
            constrcutorILGenerator.Emit(OpCodes.Call, baseConstructor);
            constrcutorILGenerator.Emit(OpCodes.Ret);

            // Creating mocked functions for every function in the interface
            var methodInfos = proxyInterface.GetMethods();
            foreach (var methodInfo in methodInfos.Where(methodInfo => !methodInfo.IsSpecialName))
            {
                CreateProxyMethod(typeBuilder, methodInfo, proxyInterface);
            }

            // Create mocked events for every event in the interface
            var eventInfos = proxyInterface.GetEvents();
            foreach (var eventInfo in eventInfos)
            {
                CreateEventProxy(typeBuilder, eventInfo);
            }

            return typeBuilder.CreateType();
        }

        #region Private methods

        /// <summary>
        /// Emits a proxy method for the provided methodinfo from the provided service interface
        /// </summary>
        /// <param name="typeBuilder"></param>
        /// <param name="methodInfo"></param>
        /// <param name="serviceInterface"></param>
        private static void CreateProxyMethod(TypeBuilder typeBuilder, MethodInfo methodInfo, Type serviceInterface)
        {
            // Define dynamic methods for every method in the interface
            var methodBuilder = typeBuilder.DefineMethod(
                string.Format("{0}.{1}", serviceInterface.FullName, methodInfo.Name),
                MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual | MethodAttributes.Final,
                CallingConventions.HasThis,
                methodInfo.ReturnType,
                methodInfo.GetParameters().Select(p => p.ParameterType).ToArray());

            var methodILGenerator = methodBuilder.GetILGenerator();

            // Define local variables
            var arrLocal = methodILGenerator.DeclareLocal(typeof(object[]));

            // Get method parameters (arguments)
            var parameters = methodInfo.GetParameters();

            methodILGenerator.Emit(OpCodes.Nop);

            // Set name of the method(including parameters list as convention) in local variable and put it in the stack
            var methodFootprint = NamingConventions.GenerateMethodFootprint(
                methodInfo.Name,
                methodInfo.GetParameters().Select(param => param.ParameterType));

            methodILGenerator.Emit(OpCodes.Ldarg_0);
            methodILGenerator.Emit(OpCodes.Ldstr, methodFootprint);

            // Load arraySize and create new array with that size
            methodILGenerator.Emit(OpCodes.Ldc_I4, parameters.Length);
            methodILGenerator.Emit(OpCodes.Newarr, typeof(Object));
            methodILGenerator.Emit(OpCodes.Stloc, arrLocal);

            // Put every argument's value into the array that will be passed to send request
            for (UInt16 paramIndex = 0; paramIndex < parameters.Length; paramIndex++)
            {
                methodILGenerator.Emit(OpCodes.Ldloc, arrLocal);
                methodILGenerator.Emit(OpCodes.Ldc_I4, paramIndex);
                methodILGenerator.Emit(OpCodes.Ldarg, paramIndex + 1);

                if (parameters[paramIndex].ParameterType.IsValueType || parameters[paramIndex].ParameterType.IsEnum)
                {
                    methodILGenerator.Emit(OpCodes.Box, parameters[paramIndex].ParameterType);
                }

                methodILGenerator.Emit(OpCodes.Stelem_Ref);
            }

            // Put name and the parameter values in the stack
            methodILGenerator.Emit(OpCodes.Ldloc, arrLocal);

            MethodInfo sendReqMethodInfo;

            // Get method info of the send request function
            if (methodInfo.ReturnType == typeof(void))
            {
                // ReSharper disable once PossibleNullReferenceException
                // Type is emitted by inheriting DynamicProxyBase<TService>
                sendReqMethodInfo = typeBuilder.BaseType
                                               .GetMethod("SendVoidRequest", new[] { typeof(string), typeof(object[]) });
            }
            else if (methodInfo.ReturnType == typeof(Task))
            {
                // ReSharper disable once PossibleNullReferenceException
                // Type is emitted by inheriting DynamicProxyBase<TService>
                sendReqMethodInfo = typeBuilder.BaseType
                                               .GetMethod("SendVoidRequestAsync", new[] { typeof(string), typeof(object[]) });
            }
            else if (methodInfo.ReturnType.BaseType == typeof(Task))
            {
                // ReSharper disable once PossibleNullReferenceException
                // Type is emitted by inheriting DynamicProxyBase<TService>
                sendReqMethodInfo = typeBuilder.BaseType
                                               .GetMethod("SendRequestAsync", new[] { typeof(string), typeof(object[]) })
                                               .MakeGenericMethod(methodInfo.ReturnType.GenericTypeArguments[0]);
            }
            else
            {
                // ReSharper disable once PossibleNullReferenceException
                // Type is emitted by inheriting DynamicProxyBase<TService>
                sendReqMethodInfo = typeBuilder.BaseType
                                               .GetMethod("SendRequest", new[] { typeof(string), typeof(object[]) })
                                               .MakeGenericMethod(methodInfo.ReturnType);
            }

            // Call the send request method
            methodILGenerator.Emit(OpCodes.Call, sendReqMethodInfo);

            // Return the send request method result
            methodILGenerator.Emit(OpCodes.Ret);

            // Add it to the type
            typeBuilder.DefineMethodOverride(methodBuilder, methodInfo);
        }

        private static void CreateEventProxy(TypeBuilder typeBuilder, EventInfo eventInfo)
        {
            var eventField = typeBuilder.DefineField(eventInfo.Name, eventInfo.EventHandlerType, FieldAttributes.Private);
            var eventBuilder = typeBuilder.DefineEvent(eventInfo.Name, EventAttributes.None, eventInfo.EventHandlerType);

            eventBuilder.SetAddOnMethod(CreateAddRemoveMethod(typeBuilder, eventField, eventInfo, true));
            eventBuilder.SetRemoveOnMethod(CreateAddRemoveMethod(typeBuilder, eventField, eventInfo, false));
            CreateRaiseEvent(typeBuilder, eventField, eventInfo);
        }

        private static MethodBuilder CreateAddRemoveMethod(TypeBuilder typeBuilder, FieldBuilder eventField, EventInfo eventInfo, bool isAdd)
        {
            var prefix = isAdd ? "add_" : "remove_";
            var delegateAction = isAdd ? "Combine" : "Remove";

            var addremoveMethod =
            typeBuilder.DefineMethod(prefix + eventInfo.Name,
               MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.NewSlot | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Final,
               null, new[] { eventInfo.EventHandlerType });

            const MethodImplAttributes eventMethodFlags = MethodImplAttributes.Synchronized;
            addremoveMethod.SetImplementationFlags(eventMethodFlags);

            var ilGen = addremoveMethod.GetILGenerator();

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldfld, eventField);
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.EmitCall(OpCodes.Call, typeof(Delegate).GetMethod(delegateAction, new[] { typeof(Delegate), typeof(Delegate) }), null);
            ilGen.Emit(OpCodes.Castclass, eventInfo.EventHandlerType);
            ilGen.Emit(OpCodes.Stfld, eventField);
            ilGen.Emit(OpCodes.Ret);

            return addremoveMethod;
        }

        private static void CreateRaiseEvent(TypeBuilder typeBuilder, FieldBuilder eventField, EventInfo eventInfo)
        {
            var parametersType = eventInfo.EventHandlerType.IsGenericType
                ? new[] { eventInfo.EventHandlerType.GenericTypeArguments[0] }
                : Type.EmptyTypes;

            var raiseEventBuilder = typeBuilder.DefineMethod(
                NamingConventions.RaiseEventMethodName(eventField.Name),
                MethodAttributes.Public | MethodAttributes.Virtual,
                null, parametersType);

            var raisePropertyChangedIl = raiseEventBuilder.GetILGenerator();
            var labelExit = raisePropertyChangedIl.DefineLabel();

            raisePropertyChangedIl.Emit(OpCodes.Ldarg_0);
            raisePropertyChangedIl.Emit(OpCodes.Ldfld, eventField);
            raisePropertyChangedIl.Emit(OpCodes.Ldnull);
            raisePropertyChangedIl.Emit(OpCodes.Ceq);
            raisePropertyChangedIl.Emit(OpCodes.Brtrue, labelExit);

            raisePropertyChangedIl.Emit(OpCodes.Ldarg_0);
            raisePropertyChangedIl.Emit(OpCodes.Ldfld, eventField);
            raisePropertyChangedIl.Emit(OpCodes.Ldarg_0);

            raisePropertyChangedIl.Emit(
                eventInfo.EventHandlerType.IsGenericType
                ? OpCodes.Ldarg_1
                : OpCodes.Ldc_I4_0);

            raisePropertyChangedIl.EmitCall(OpCodes.Callvirt, eventInfo.EventHandlerType.GetMethod("Invoke"), null);

            raisePropertyChangedIl.MarkLabel(labelExit);
            raisePropertyChangedIl.Emit(OpCodes.Ret);
        }

        #endregion
    }
}
