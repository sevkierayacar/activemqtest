﻿using FortyTwo.CF.Core.ChannelFactory.RpcProxies;
using FortyTwo.CF.Core.Communication;
using FortyTwo.CF.Core.Messages;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace FortyTwo.CF.Core.ChannelFactory
{
    public abstract class DynamicProxyBase<TService>
    {
        private readonly IMessageChannel _messageChannel;
        private readonly int _timeout;
        private readonly bool _useInternalQueue;

        private readonly BlockingCollection<Event> _eventQueue = new BlockingCollection<Event>();// shall we have a bound queue?
        private readonly IDictionary<string, MethodInfo> _eventRaiseMethods;

        public DynamicProxyBase(IMessageChannel messageChannel, int timeout, bool useInternalQueue = true)
        {
            _messageChannel = messageChannel;
            _timeout = timeout;
            _useInternalQueue = useInternalQueue;

            if (_useInternalQueue)
            {
                Task.Factory.StartNew(() =>
                {
                    var ev = _eventQueue.Take();
                    while (ev != null)
                    {
                        ProcessEvent(ev);
                        ev = _eventQueue.Take();
                    }
                }, TaskCreationOptions.LongRunning);
            }

            _eventRaiseMethods = typeof(TService)
                .GetEvents()
                .ToDictionary(
                    evnt => NamingConventions.RaiseEventMethodName(evnt.Name),
                    evnt => GetType().GetMethod(NamingConventions.RaiseEventMethodName(evnt.Name)));

            messageChannel.OnEvent += provider_OnEvent;
        }

        private void ProcessEvent(Event ev)
        {
            try
            {
                RaiseEvent(ev.EventType, ev.Payload);
            }
            catch (Exception)
            {
                if (!_useInternalQueue)
                {
                    throw;
                }
            }
        }

        #region Public methods
        /// <summary>
        /// [NOT RECOMMENDED!] Sends direct response to the service channel with action to execute and the necessary parameters and return the response.
        /// </summary>
        /// <typeparam name="TResponse">The expected type of the service response</typeparam>
        /// <param name="actionName">The name of the action to execute</param>
        /// <param name="parameters">Parameters of the action</param>
        /// <returns>The result of the service operation</returns>
        public Task<TResponse> SendRequestAsync<TResponse>(string actionName, params object[] parameters)
        {
            var rpcProxy = new RpcProxy(_messageChannel, _timeout);
            return rpcProxy.RequestAsync<TResponse>(actionName, parameters);
        }

        public TResponse SendRequest<TResponse>(string actionName, params object[] parameters)
        {
            var rpcProxy = new RpcProxy(_messageChannel, _timeout);
            return rpcProxy.Request<TResponse>(actionName, parameters);
        }

        public void SendVoidRequest(string actionName, params object[] parameters)
        {
            var rpcProxy = new RpcProxy(_messageChannel, _timeout);
            rpcProxy.Request(actionName, parameters);
        }

        public Task SendVoidRequestAsync(string actionName, params object[] parameters)
        {
            var rpcProxy = new RpcProxy(_messageChannel, _timeout);
            return rpcProxy.RequestAsync(actionName, parameters);
        }

        #endregion

        #region Private methods

        private void provider_OnEvent(object sender, Event evnt)
        {
            if (_useInternalQueue)
            {
                _eventQueue.Add(evnt);
            }
            else
            {
                ProcessEvent(evnt);
            }
        }

        /// <summary>
        /// Raises the requested event with the payload provided
        /// </summary>
        /// <param name="eventName">Name of the event.</param>
        /// <param name="payload">Something</param>
        /// <exception cref="System.MissingMethodException">There is no definition for event:  + eventName</exception>
        private void RaiseEvent(string eventName, object payload)
        {
            if (string.IsNullOrEmpty(eventName))
            {
                throw new ArgumentException("Something is really wrong - event name is empty or null", nameof(eventName));
            }

            MethodInfo raiseEventMethod;
            if (_eventRaiseMethods.TryGetValue(NamingConventions.RaiseEventMethodName(eventName), out raiseEventMethod))
            {
                if (payload == null)
                {
                    raiseEventMethod.Invoke(this, null);
                    return;
                }

                raiseEventMethod.Invoke(this, new[] { payload });
            }

            // logging if there is no event definition ?

        }

        #endregion
    }
}
