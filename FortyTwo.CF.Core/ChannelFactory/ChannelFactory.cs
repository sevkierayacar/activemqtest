﻿using FortyTwo.CF.Core.ChannelFactory.Exceptions;
using FortyTwo.CF.Core.Communication;
using System;

namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Factory for creation of service channels.
    /// </summary>
    /// <typeparam name="TService">The type of the service contract</typeparam>
    public class ChannelFactory<TService> : IChannelFactory<TService> where TService : class
    {
        #region Private members

        private readonly Type _serviceContractType;
        private readonly IMessageChannel _messageChannel;
       
        #endregion

        #region Constructor

        public ChannelFactory(IMessageChannel messageChannel)
        {
            _serviceContractType = typeof(TService);
            if (!_serviceContractType.IsInterface)
            {
                throw new DynamicProxyAssertionException("Provided type is not an interface!");
            }

            if (messageChannel == null)
            {
                throw new ArgumentNullException("messageChannel");
            }

            _messageChannel = messageChannel;
        }

        #endregion

        /// <summary>
        /// Creates channel for the provided service contract with the passed Message Channel
        /// </summary>
        /// <param name="timeout">Timeout of the request in milliseconds</param>
        /// <param name="useInternalQueue"></param>
        /// <returns>
        /// Communication channel for the provided service contract
        /// </returns>
        public TService CreateChannel(int timeout = 60000, bool useInternalQueue = true)
        {         
            var proxyType = DynamicTypeCache.TryGetProxyForType(_serviceContractType, true);
            if (proxyType == null)
            {
                proxyType = DynamicProxyBuilder<TService>.CreateProxy();
                DynamicTypeCache.AddProxyForType(_serviceContractType, proxyType, true);
            }

            var instance = Activator.CreateInstance(proxyType, _messageChannel, timeout,useInternalQueue);
            return (TService)instance;
        }

        /// <summary>
        /// Create and runs a service host with the provided Message Channel, using the provided implementation
        /// </summary>
        /// <param name="implementator">The contract implementation</param>
        public void CreateServiceHost(TService implementator)
        {
            var proxyType = DynamicTypeCache.TryGetProxyForType(_serviceContractType, false);

            if (proxyType == null)
            {
                proxyType = DynamicServiceHostBuilder<TService>.CreateServiceHostType();
                DynamicTypeCache.AddProxyForType(_serviceContractType, proxyType, false);
            }

            var serviceHostIstance = Activator.CreateInstance(proxyType, _messageChannel, implementator);
            DynamicServiceHostBuilder<TService>.AssignHandlersToInstanciatedHost(serviceHostIstance, implementator);
        }
    }
}
