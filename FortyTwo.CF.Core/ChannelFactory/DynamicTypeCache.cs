﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Cache for storing proxy types.
    /// </summary>
    internal static class DynamicTypeCache
    {
        #region Declarations

        private static readonly object SyncRoot = new object();
        private static readonly Dictionary<string, Type> TypeCache = new Dictionary<string, Type>();

        #endregion

        /// <summary>
        /// Adds a proxy to the type cache.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="proxy">The proxy.</param>
        /// <param name="isClientChannel">if set to <c>true</c> [is client channel].</param>
        public static void AddProxyForType(Type type, Type proxy, bool isClientChannel)
        {
            lock (SyncRoot)
            {
                TypeCache.Add(GetHashCode(type.AssemblyQualifiedName) + (isClientChannel ? "client" : "host"), proxy);
            }
        }

        /// <summary>
        /// Tries the type of the get proxy for.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="isClientChannel">if set to <c>true</c> [is client channel].</param>
        /// <returns></returns>
        public static Type TryGetProxyForType(Type type, bool isClientChannel)
        {
            lock (SyncRoot)
            {
                Type proxyType;
                TypeCache.TryGetValue(GetHashCode(type.AssemblyQualifiedName) + (isClientChannel ? "client" : "host"), out proxyType);
                return proxyType;
            }
        }

        #region Private Methods

        private static string GetHashCode(string fullName)
        {
            var provider = new SHA1CryptoServiceProvider();
            var buffer = Encoding.UTF8.GetBytes(fullName);
            var hash = provider.ComputeHash(buffer, 0, buffer.Length);
            
            return Convert.ToBase64String(hash);
        }

        #endregion
    }
}