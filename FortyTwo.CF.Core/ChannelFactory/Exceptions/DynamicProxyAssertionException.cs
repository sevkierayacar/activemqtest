﻿using System;

namespace FortyTwo.CF.Core.ChannelFactory.Exceptions
{
    /// <summary>
    /// Exception thrown when dynamic proxy assertion is incorrect
    /// </summary>
    public class DynamicProxyAssertionException : Exception
    {
        public DynamicProxyAssertionException(string message) : base(message) { }
    }
}
