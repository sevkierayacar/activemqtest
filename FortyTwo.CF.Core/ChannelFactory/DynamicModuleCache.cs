﻿using System.Reflection.Emit;

namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Class for storing the module builder.
    /// </summary>
    internal static class DynamicModuleCache
    {
        #region Declarations

        private static readonly object SyncRoot = new object();
        private static ModuleBuilder _cache;

        #endregion

        #region Add

        /// <summary>
        /// Adds a dynamic module builder to the cache.
        /// </summary>
        /// <param name="moduleBuilder">The module builder.</param>
        public static void Add(ModuleBuilder moduleBuilder)
        {
            lock (SyncRoot)
            {
                _cache = moduleBuilder;
            }
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets the cached module builder.
        /// </summary>
        /// <returns></returns>
        public static ModuleBuilder Get
        {
            get
            {
                lock (SyncRoot)
                {
                    return _cache ?? (_cache = DynamicModuleBuilder.Create());
                }
            }
        }

        #endregion
    }
}