﻿using FortyTwo.CF.Core.Communication;
using FortyTwo.CF.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace FortyTwo.CF.Core.ChannelFactory
{
    public class DynamicServiceHostBase<TService> where TService : class
    {
        private readonly IMessageChannel _messagesChannel;
        private readonly TService _serviceImplementation;
        private readonly IDictionary<string, MethodInfo> _implementationMethods;

        private long _numberOfSimultaneousReqvests;

        public DynamicServiceHostBase(IMessageChannel messagesChannel, TService serviceImplementation)
        {
            _messagesChannel = messagesChannel;
            _serviceImplementation = serviceImplementation;
            _implementationMethods =
                typeof(TService)
                    .GetMethods()
                    .Where(method => !method.IsSpecialName)
                    .ToDictionary(
                        method => NamingConventions.GenerateMethodFootprint(method.Name, method.GetParameters().Select(p => p.ParameterType)),
                        method => method);

            messagesChannel.OnRequest += InvokeRequest;
        }

        public void SendEvent(string eventName, object payload)
        {
            var eventMessage = new Event
            {
                EventType = eventName,
                Payload = payload,
                TimeStamp = DateTime.Now
            };

            _messagesChannel.SendMessage(eventMessage);
        }

        #region Private Methods

        private void InvokeRequest(object sender, Request request)
        {
            Interlocked.Increment(ref _numberOfSimultaneousReqvests);

            var number = Interlocked.Read(ref _numberOfSimultaneousReqvests);

            // here we start as many parallel tasks as there are requests 
            // concurrency level can be controlled here(semaphoreslim or taskscheduler)
            System.Threading.Tasks.Task.Run(async () =>
            {
                var response = new Response
                {
                    CorrelationId = request.CorrelationId
                };

                try
                {
                    MethodInfo requestMethod;
                    if (!_implementationMethods.TryGetValue(request.ActionName, out requestMethod))
                    {
                        var errorMessage =
                            string.Format("There is no action with name '{0}' defined in '{1}' interface!",
                                request.ActionName, GenerateTypeName(typeof(TService)));
                        throw new InvalidOperationException(errorMessage);
                    }

                    var result = requestMethod.Invoke(_serviceImplementation, request.ActionParameters);


                    if (result != null && requestMethod.ReturnType == typeof(Task))
                    {
                        await (Task)result;
                    }
                    else if (result != null && requestMethod.ReturnType.BaseType == typeof(Task))
                    {
                        response.Payload = await (dynamic)result;
                    }
                    else
                    {
                        response.Payload = result;
                    }

                    response.TimeStamp = DateTime.Now;
                    response.Success = true;
                    _messagesChannel.SendMessage(response);
                }
                catch (Exception ex)
                {
                    response.TimeStamp = DateTime.Now;
                    response.Success = false;
                    response.ErrorMessage = ex.Message;
                    response.Payload = null;

                    _messagesChannel.SendMessage(response);
                }
                finally
                {
                    DecrementCounter();
                }
            });
        }

        private void DecrementCounter()
        {
            Interlocked.Decrement(ref _numberOfSimultaneousReqvests);
        }

        private static string GenerateTypeName(Type type)
        {
            if (!type.IsGenericType)
                return type.Name;

            return string.Format("{0}<{1}>", type.Name.Substring(0, type.Name.IndexOf("`", StringComparison.Ordinal)),
                string.Join(", ", type.GetGenericArguments().Select(GenerateTypeName).ToArray()));
        }

        #endregion
    }
}
