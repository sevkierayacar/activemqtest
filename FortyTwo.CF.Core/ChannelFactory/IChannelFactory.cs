﻿namespace FortyTwo.CF.Core.ChannelFactory
{
    public interface IChannelFactory<TService> where TService : class
    {
        TService CreateChannel(int timeout = 60000, bool useInternalQueue = true);
        void CreateServiceHost(TService implementator);
    }
}
