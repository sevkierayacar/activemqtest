﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FortyTwo.CF.Core.ChannelFactory
{
    internal static class NamingConventions
    {
        internal static string GenerateMethodFootprint(string methodName, IEnumerable<Type> methodParameterTypes)
        {
            return string.Format("{0}_{1}", methodName, string.Join("_", methodParameterTypes.Select(type => type.Name)));
        }

        internal static string RaiseEventMethodName(string eventName)
        {
            return string.Format("Raise_{0}", eventName);
        }
    }
}
