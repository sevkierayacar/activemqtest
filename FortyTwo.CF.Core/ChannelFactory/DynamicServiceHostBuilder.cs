﻿using FortyTwo.CF.Core.Communication;
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace FortyTwo.CF.Core.ChannelFactory
{
    public static class DynamicServiceHostBuilder<TService> where TService : class
    {
        public static Type CreateServiceHostType() 
        {
            var proxyInterface = typeof(TService);
            // Define proxy base type
            var baseProxyType = typeof(DynamicServiceHostBase<TService>);
            // Define typebuilder
            var typeBuilder = DynamicModuleCache.Get.DefineType(
                string.Format("{0}.{1}ServiceProxy", proxyInterface.Namespace, proxyInterface.FullName),
                TypeAttributes.Public,
                baseProxyType);

            // Define empty constructor for the proxy
            var constructorBuilder = typeBuilder.DefineConstructor(
                MethodAttributes.Public, 
                CallingConventions.Standard, 
                new[] { typeof(IMessageChannel), typeof(TService) });

            var baseConstructor = baseProxyType.GetConstructor(new[] { typeof(IMessageChannel), typeof(TService) });

            if (baseConstructor == null)
            {
                throw new MissingMemberException(string.Format("DynamicServiceHostBase<{0}>", proxyInterface.Name), "Constructor");
            }

            var constrcutorILGenerator = constructorBuilder.GetILGenerator();

            // Get this constructor's parameter and send it to the base constructor
            constrcutorILGenerator.Emit(OpCodes.Ldarg_0);
            constrcutorILGenerator.Emit(OpCodes.Ldarg_1);
            constrcutorILGenerator.Emit(OpCodes.Ldarg_2);
            constrcutorILGenerator.Emit(OpCodes.Call, baseConstructor);
            constrcutorILGenerator.Emit(OpCodes.Ret);

            var events = typeof(TService).GetEvents();

            // ReSharper disable once PossibleNullReferenceException
            // Type emitted with base type DynamicServiceHostBase<TService>
            var sendEventMethodInfo = typeBuilder.BaseType.GetMethod("SendEvent", new[] { typeof(string), typeof(object) });

            // For every event create event handler and emit call to the SendEvent method with the necessary parameters
            foreach (var serviceEvent in events)
            {
                var invokeMethod = serviceEvent.EventHandlerType.GetMethod("Invoke");
                var handlerParams = invokeMethod.GetParameters().Select(x => x.ParameterType).ToArray();
                
                var methodBuilder = typeBuilder.DefineMethod(
                    serviceEvent.Name + "Handler", MethodAttributes.Public,  
                    invokeMethod.ReturnType, handlerParams);

                var handlerILGenerator = methodBuilder.GetILGenerator();

                // Put "this" in stack
                handlerILGenerator.Emit(OpCodes.Ldarg_0);

                // Put the event name and the parameter in the stack
                handlerILGenerator.Emit(OpCodes.Ldstr, serviceEvent.Name);

                handlerILGenerator.Emit(OpCodes.Ldarg_2);
                if (serviceEvent.EventHandlerType.IsGenericType && (handlerParams[1].IsEnum || handlerParams[1].IsValueType))
                {
                    handlerILGenerator.Emit(OpCodes.Box, handlerParams[1]);
                }

                // Call void send event method and simply return
                handlerILGenerator.Emit(OpCodes.Call, sendEventMethodInfo);
                handlerILGenerator.Emit(OpCodes.Ret);
            }

            var createdType = typeBuilder.CreateType();

            return createdType;
        }

        public static void AssignHandlersToInstanciatedHost(object serviceHost, TService implementator) 
        {
            var serviceEvents = typeof(TService).GetEvents();

            foreach (var serviceEvent in serviceEvents)
            {
                var eventHandler = serviceHost.GetType().GetMethod(serviceEvent.Name + "Handler").CreateDelegate(serviceEvent.EventHandlerType, serviceHost);
                serviceEvent.AddEventHandler(implementator, eventHandler);
            }
        }
    }
}
