﻿using System.Reflection.Emit;
namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Cache for storing the dynamic assembly builder.
    /// </summary>
    internal static class DynamicAssemblyCache
    {
        #region Declarations

        private static readonly object SyncRoot = new object();
        private static AssemblyBuilder _cache;

        #endregion

        #region Adds a dynamic assembly to the cache.

        /// <summary>
        /// Adds a dynamic assembly builder to the cache.
        /// </summary>
        /// <param name="assemblyBuilder">The assembly builder.</param>
        public static void Add(AssemblyBuilder assemblyBuilder)
        {
            lock (SyncRoot)
            {
                _cache = assemblyBuilder;
            }
        }

        #endregion

        #region Gets the cached assembly

        /// <summary>
        /// Gets the cached assembly builder.
        /// </summary>
        /// <returns></returns>
        public static AssemblyBuilder Get
        {
            get
            {
                lock (SyncRoot)
                {
                    return _cache ?? (_cache = DynamicAssemblyBuilder.Create());
                }
            }
        }

        #endregion
    }
}