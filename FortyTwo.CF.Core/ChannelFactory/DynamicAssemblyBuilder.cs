﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace FortyTwo.CF.Core.ChannelFactory
{
    /// <summary>
    /// Class for creating an assembly builder.
    /// </summary>
    internal static class DynamicAssemblyBuilder
    {
        #region Create

        public static AssemblyBuilder Create()
        {
            return Create("FortyTwoFCProxies");
        }

        /// <summary>
        /// Creates an assembly builder.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        public static AssemblyBuilder Create(string assemblyName)
        {
            var name = new AssemblyName(assemblyName);
            var assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.Run);
            DynamicAssemblyCache.Add(assembly);

            return assembly;
        }

        /// <summary>
        /// Creates an assembly builder and saves the assembly to the passed in location.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="filePath">The file path.</param>
        public static AssemblyBuilder Create(string assemblyName, string filePath)
        {
            var name = new AssemblyName(assemblyName);
            var assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave, filePath);
            DynamicAssemblyCache.Add(assembly);

            return assembly;
        }

        #endregion
    }
}