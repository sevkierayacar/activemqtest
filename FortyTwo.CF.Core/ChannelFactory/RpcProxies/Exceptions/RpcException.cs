﻿using System;

namespace FortyTwo.CF.Core.ChannelFactory.RpcProxies.Exceptions
{
    public class RpcException : Exception
    {
        public RpcException(string message) : base(message) { }
    }
}
