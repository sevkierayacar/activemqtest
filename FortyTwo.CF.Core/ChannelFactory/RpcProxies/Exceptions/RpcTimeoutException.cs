﻿using System;

namespace FortyTwo.CF.Core.ChannelFactory.RpcProxies.Exceptions
{
    public class RpcTimeoutException : Exception
    {
        public RpcTimeoutException(string message) : base(message) { }
    }
}
