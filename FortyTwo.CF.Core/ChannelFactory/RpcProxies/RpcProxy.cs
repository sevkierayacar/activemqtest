﻿using FortyTwo.CF.Core.ChannelFactory.RpcProxies.Exceptions;
using FortyTwo.CF.Core.Communication;
using FortyTwo.CF.Core.Messages;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FortyTwo.CF.Core.ChannelFactory.RpcProxies
{
    public class RpcProxy
    {
        private readonly IMessageChannel _messageChannel;
        private Exception _exception;
        private readonly string _correlationId;
        private object _responsePayload;
        private readonly int _timeout;
        private readonly AutoResetEvent _msgsWsgEvent = new AutoResetEvent(false);

        public RpcProxy(IMessageChannel messageChannel, int timeout)
        {
            _messageChannel = messageChannel;
            _timeout = timeout;
            _exception = null;
            _correlationId = Guid.NewGuid().ToString();
        }

        public Task<TResponse> RequestAsync<TResponse>(string name, object[] parameters)
        {
            return Task.Run(() =>
            {
                ExecuteRequest(name, parameters);
                return HandleResultCasting<TResponse>(_responsePayload);
            });
        }

        public TResponse Request<TResponse>(string name, object[] parameters)
        {
            ExecuteRequest(name, parameters);
            return HandleResultCasting<TResponse>(_responsePayload);
        }

        internal void Request(string name, object[] parameters)
        {
            ExecuteRequest(name, parameters);
        }

        internal Task RequestAsync(string name, object[] parameters)
        {
            return Task.Run(() => ExecuteRequest(name, parameters));
        }

        private void OnResponseReceived(object sender, Response response)
        {
            if (response != null && response.CorrelationId == _correlationId)
            {
                if (!response.Success)
                {
                    _exception = new RpcException(response.ErrorMessage);
                    _msgsWsgEvent.Set();
                    return;
                }

                _responsePayload = response.Payload;
                _msgsWsgEvent.Set();
            }
        }

        private void ExecuteRequest(string name, object[] parameters)
        {
            _messageChannel.OnResponse += OnResponseReceived;
            try
            {
                var request = new Request
                {
                    ActionName = name,
                    ActionParameters = parameters,
                    CorrelationId = _correlationId,
                    TimeStamp = DateTime.Now
                };

                _messageChannel.SendMessage(request);

                if (!_msgsWsgEvent.WaitOne(_timeout))
                {
                    throw new RpcTimeoutException(string.Format("Timeout while waiting for Response for action type: {0}", name));
                }

                if (_exception != null)
                {
                    throw _exception;
                }
            }
            finally
            {
                _messageChannel.OnResponse -= OnResponseReceived;
            }
        }

        private static T HandleResultCasting<T>(object value)
        {
            var valueType = value.GetType();
            return valueType.IsValueType && !valueType.IsEnum
                ? (T)Convert.ChangeType(value, typeof(T))
                : (T)value;
        }
    }
}
