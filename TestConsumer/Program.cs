﻿using Castle.Facilities.Startable;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Common;
using FortyTwo.CastleIntegration;
using FortyTwo.CF.Core.Encoders;
using FortyTwo.CF.Encoders.Json;
using FortyTwo.CF.Infrastructure.ActiveMQ;
using FortyTwo.CF.Infrastructure.ActiveMQ.CastleIntegration;
using System;
using System.Threading;
using FortyTwo.CF.Core.Messages;
using FortyTwo.CF.Encoders.Protobuf;

namespace TestConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var container = new WindsorContainer())
            {
                container.AddFacility<StartableFacility>(facility => facility.DeferredStart());
                container.AddFacility<CommunicationFrameworkFacility>();
                container.Install(new ActiveMqChannelInstaller());
                container.Register(
                    Component.For<IMessagesEncoder>().ImplementedBy<JsonEncoder>().Named("json"),
                    //Component.For<IMessagesEncoder>().ImplementedBy<ProtoBufMessagesEncoder>().Named("proto"),
                    Component.For<IConnectionListener>().ImplementedBy<ConnectionListener>(),
                    Component.For<ICreadorInterface>().AsClient(new ClientConfiguration
                    {
                        BrokerUri = "failover:(tcp://10.55.0.130:61616)",
                        FileServerUrl = "http://10.55.0.130:8161/fileserver/",
                        Encoder = "json",
                        Timeout = TimeSpan.FromSeconds(30),
                        //UseReliableMessageProcessing = true
                    })
                );

                Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Started..");
                //Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Calling function...");

                var obj = container.Resolve<ICreadorInterface>();
                //obj.OnMessage += Message;
                //obj.OnRequest += RequestMessage;
                //obj.OnResponse += ResponseMessage;

                Console.ReadKey();
                var result = obj.MakeRequest(new RequestObj
                {
                    Age = 12,
                    Name = "asdsada"
                });
                Console.WriteLine(result.Id);
                Console.WriteLine(result.Result);
                //var result = obj.ConvertToObjectTwo(new TestObjectOne
                //{
                //    TestIntOne = 123,
                //});

                // Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Client(Consumer) Returned {result.TestIntTwo} {result.TestStringTwo}");

                //Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Client(Consumer) Stopping...");
                Console.ReadKey();
            }
        }

        private static void ResponseMessage(object sender, Response e)
        {
            Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Response CATCHED!!!: " + e.Payload);
        }

        private static void RequestMessage(object sender, Request e)
        {
            Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Request CATCHED!!!: " + e.ActionName);
        }

        private static void Message(object sender, string e)
        {
            Console.WriteLine($"{DateTime.Now.ToString(Utility.DatetimeFormat)} Message CATCHED!!!: " + e);
        }
    }
}