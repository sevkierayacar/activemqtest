﻿using System;

namespace SharedContracts.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Enum | AttributeTargets.Field, AllowMultiple = false)]
    public class ContractMetaAttribute : Attribute
    {
        private readonly string _displayName;
        private readonly bool _isDisplayed;

        public ContractMetaAttribute(string displayName, bool isDisplayed = true)
        {
            _displayName = displayName;
            _isDisplayed = isDisplayed;
        }

        public string DisplayName { get { return _displayName; } }

        public bool IsDisplayed { get { return _isDisplayed; } }
    }
}
