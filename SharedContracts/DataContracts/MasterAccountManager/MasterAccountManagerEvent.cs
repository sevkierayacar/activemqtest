﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.MasterAccountManager
{
    public class MasterAccountManagerEvent 
    {
        public int TradingPlatform { get; set; }
        public long MasterLogin { get; set; }
        public long ClientLogin { get; set; }
        public long MasterTicket { get; set; }
        public long ClientTicket { get; set; }
        public DateTime Date { get; set; }
        public string Log { get; set; }
    }
}
