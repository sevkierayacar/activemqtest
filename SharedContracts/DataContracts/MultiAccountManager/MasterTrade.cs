﻿using System.Collections.Generic;

namespace SharedContracts.DataContracts.MultiAccountManager
{
    public class MasterTrade
    {
        public double ClientVolumes;
        public double ClientProfits;
        public double ClientSwaps;
        public double ClientCommissions;
        public Trade Trade;
        public IList<Trade> SlaveTrades;
    }
}
