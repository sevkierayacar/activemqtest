﻿namespace SharedContracts.DataContracts.MultiAccountManager
{
    public class Account
    {
        public int Login { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public double Balance { get; set; }
        public double Equity { get; set; }
        public double Margin { get; set; }
    }
}
