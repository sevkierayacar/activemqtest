﻿using System;

namespace SharedContracts.DataContracts.MultiAccountManager
{
    public class Trade
    {
        public int Login { get; set; }
        public int OrderNumber { get; set; }
        public int MasterOrderNumber { get; set; }
        public string SymbolName { get; set; }
        public double Volume { get; set; }
        public int OpenTime { get; set; }
        public int CloseTime { get; set; }
        public double OpenPrice { get; set; }
        public double CurrentPrice { get; set; }
        public double Profit { get; set; }
        public double Swap { get; set; }
        public double Commission { get; set; }
        public string Comment { get; set; }
        public int Command { get; set; }
    }
}

