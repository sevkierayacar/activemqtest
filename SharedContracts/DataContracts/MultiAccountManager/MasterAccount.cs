﻿using System.Collections.Generic;

namespace SharedContracts.DataContracts.MultiAccountManager
{
    public class MasterAccount
    {
        public Account Master { get; set; }
        public string Allocation { get; set; }
        public IList<Account> SlaveList { get; set; }
    }
}
