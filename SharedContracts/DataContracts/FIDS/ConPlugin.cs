// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConPlugin
    {
        public string file { get; set; }
        public PluginInfo info { get; set; }
        public int enabled { get; set; }
        public int configurable { get; set; }
        public int manager_access { get; set; }
    }
}