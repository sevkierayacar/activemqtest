// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConSymbol
    {
        public string symbol { get; set; }
        public string description { get; set; }
        public string source { get; set; }
        public string currency { get; set; }
        public int type { get; set; }
        public int digits { get; set; }
        public int trade { get; set; }
        public uint background_color { get; set; }
        public int count { get; set; }
        public int count_original { get; set; }
        public int external_unused { get; set; }
        public int realtime { get; set; }
        public DateTime starting { get; set; }
        public DateTime expiration { get; set; }
        public ConSessions sessions { get; set; }
        public int profit_mode { get; set; }
        public int profit_reserved { get; set; }
        public int filter { get; set; }
        public int filter_counter { get; set; }
        public double filter_limit { get; set; }
        public int filter_smoothing { get; set; }
        public float filter_reserved { get; set; }
        public int logging { get; set; }
        public int spread { get; set; }
        public int spread_balance { get; set; }
        public int exemode { get; set; }
        public int swap_enable { get; set; }
        public int swap_type { get; set; }
        public double swap_long { get; set; }
        public double swap_short { get; set; }
        public int swap_rollover3days { get; set; }
        public double contract_size { get; set; }
        public double tick_value { get; set; }
        public double tick_size { get; set; }
        public int stops_level { get; set; }
        public int gtc_pendings { get; set; }
        public int margin_mode { get; set; }
        public double margin_initial { get; set; }
        public double margin_maintenance { get; set; }
        public double margin_hedged { get; set; }
        public double margin_divider { get; set; }
        public double point { get; set; }
        public double multiply { get; set; }
        public double bid_tickvalue { get; set; }
        public double ask_tickvalue { get; set; }
        public int long_only { get; set; }
        public int instant_max_volume { get; set; }
        public string margin_currency { get; set; }
        public int freeze_level { get; set; }
        public int margin_hedged_strong { get; set; }
        public DateTime value_date { get; set; }
        public int quotes_delay { get; set; }
        public int swap_openprice { get; set; }
        public int swap_variation_margin { get; set; }
    }
}