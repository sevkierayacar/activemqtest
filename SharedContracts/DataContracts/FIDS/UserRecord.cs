// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class UserRecord
    {
        public int login { get; set; }
        public string group { get; set; }
        public string password { get; set; }
        public int enable { get; set; }
        public int enable_change_password { get; set; }
        public int enable_read_only { get; set; }
        public int enable_otp { get; set; }
        public int enable_reserved { get; set; }
        public string password_investor { get; set; }
        public string password_phone { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string address { get; set; }
        public string lead_source { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string comment { get; set; }
        public string id { get; set; }
        public string status { get; set; }
        public DateTime regdate { get; set; }
        public DateTime lastdate { get; set; }
        public int leverage { get; set; }
        public int agent_account { get; set; }
        public DateTime timestamp { get; set; }
        public int last_ip { get; set; }
        public double balance { get; set; }
        public double prevmonthbalance { get; set; }
        public double prevbalance { get; set; }
        public double credit { get; set; }
        public double interestrate { get; set; }
        public double taxes { get; set; }
        public double prevmonthequity { get; set; }
        public double prevequity { get; set; }
        public double reserved2 { get; set; }
        public string otp_secret { get; set; }
        public string secure_reserved { get; set; }
        public int send_reports { get; set; }
        public uint mqid { get; set; }
        public uint user_color { get; set; }
        public string api_data { get; set; }
    }
}