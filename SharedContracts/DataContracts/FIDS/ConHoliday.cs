// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConHoliday
    {
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
        public int from { get; set; }
        public int to { get; set; }
        public string symbol { get; set; }
        public string description { get; set; }
        public int enable { get; set; }
    }
}