// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConSymbolGroup
    {
        public string name { get; set; }
        public string description { get; set; }
    }
}