// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConSessions
    {
        public ConSession quote { get; set; }
        public ConSession trade { get; set; }
        public int quote_overnight { get; set; }
        public int trade_overnight { get; set; }
    }
}