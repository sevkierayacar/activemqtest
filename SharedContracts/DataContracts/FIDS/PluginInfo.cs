// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class PluginInfo
    {
        public string name { get; set; }
        public uint version { get; set; }
        public string copyright { get; set; }
    }
}