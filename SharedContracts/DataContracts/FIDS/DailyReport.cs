// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class DailyReport
    {
        public int login { get; set; }
        public DateTime ctm { get; set; }
        public string group { get; set; }
        public string bank { get; set; }
        public double balance_prev { get; set; }
        public double balance { get; set; }
        public double deposit { get; set; }
        public double credit { get; set; }
        public double profit_closed { get; set; }
        public double profit { get; set; }
        public double equity { get; set; }
        public double margin { get; set; }
        public double margin_free { get; set; }
        public int next { get; set; }
    }
}