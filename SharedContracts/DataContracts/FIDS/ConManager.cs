// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConManager
    {
        public int login { get; set; }
        public int manager { get; set; }
        public int money { get; set; }
        public int online { get; set; }
        public int riskman { get; set; }
        public int broker { get; set; }
        public int admin { get; set; }
        public int logs { get; set; }
        public int reports { get; set; }
        public int trades { get; set; }
        public int market_watch { get; set; }
        public int email { get; set; }
        public int user_details { get; set; }
        public int see_trades { get; set; }
        public int news { get; set; }
        public int plugins { get; set; }
        public int server_reports { get; set; }
        public int techsupport { get; set; }
        public int market { get; set; }
        public int notifications { get; set; }
        public int ipfilter { get; set; }
        public uint ip_from { get; set; }
        public uint ip_to { get; set; }
        public string mailbox { get; set; }
        public string groups { get; set; }
        public ConManagerSec secgroups { get; set; }
        public uint exp_time { get; set; }
        public string name { get; set; }
        public int info_depth { get; set; }
    }
}