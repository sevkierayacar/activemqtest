// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGroup
    {
        public string group { get; set; }
        public int enable { get; set; }
        public int timeout { get; set; }
        public int otp_mode { get; set; }
        public string company { get; set; }
        public string signature { get; set; }
        public string support_page { get; set; }
        public string smtp_server { get; set; }
        public string smtp_login { get; set; }
        public string smtp_password { get; set; }
        public string support_email { get; set; }
        public string templates { get; set; }
        public int copies { get; set; }
        public int reports { get; set; }
        public int default_leverage { get; set; }
        public double default_deposit { get; set; }
        public int maxsecurities { get; set; }
        public ConGroupSec secgroups { get; set; }
        public ConGroupMargin secmargins { get; set; }
        public int secmargins_total { get; set; }
        public string currency { get; set; }
        public double credit { get; set; }
        public int margin_call { get; set; }
        public int margin_mode { get; set; }
        public int margin_stopout { get; set; }
        public double interestrate { get; set; }
        public int use_swap { get; set; }
        public int news { get; set; }
        public int rights { get; set; }
        public int check_ie_prices { get; set; }
        public int maxpositions { get; set; }
        public int close_reopen { get; set; }
        public int hedge_prohibited { get; set; }
        public int close_fifo { get; set; }
        public int hedge_largeleg { get; set; }
        public int unused_rights { get; set; }
        public string securities_hash { get; set; }
        public int margin_type { get; set; }
        public int archive_period { get; set; }
        public int archive_max_balance { get; set; }
        public int stopout_skip_hedged { get; set; }
        public int archive_pending_period { get; set; }
        public uint news_languages { get; set; }
        public uint news_languages_total { get; set; }
    }
}