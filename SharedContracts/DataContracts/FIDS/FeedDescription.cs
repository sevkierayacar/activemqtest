// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class FeedDescription
    {
        public int version { get; set; }
        public string name { get; set; }
        public string copyright { get; set; }
        public string web { get; set; }
        public string email { get; set; }
        public string server { get; set; }
        public string username { get; set; }
        public string userpass { get; set; }
        public int modes { get; set; }
        public string description { get; set; }
        public string module { get; set; }
    }
}