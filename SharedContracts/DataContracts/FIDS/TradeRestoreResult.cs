// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TradeRestoreResult
    {
        public int order { get; set; }
        public byte res { get; set; }
    }
}