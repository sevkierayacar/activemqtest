// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class BackupInfo
    {
        public string file { get; set; }
        public int size { get; set; }
        public DateTime time { get; set; }
    }
}