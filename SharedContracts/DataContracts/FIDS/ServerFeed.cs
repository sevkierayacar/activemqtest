// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ServerFeed
    {
        public string file { get; set; }
        public FeedDescription feed { get; set; }
    }
}