// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class BalanceDiff
    {
        public int login { get; set; }
        public double diff { get; set; }
    }
}