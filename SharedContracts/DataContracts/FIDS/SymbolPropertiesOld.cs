// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class SymbolPropertiesOld
    {
        public string symbol { get; set; }
        public uint color { get; set; }
        public int spread { get; set; }
        public int spread_balance { get; set; }
        public int stops_level { get; set; }
        public int exemode { get; set; }
    }
}