// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TickRecord
    {
        public DateTime ctm { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
        public int datafeed { get; set; }
        public char flags { get; set; }
    }
}