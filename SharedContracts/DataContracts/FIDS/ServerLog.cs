// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ServerLog
    {
        public int code { get; set; }
        public string time { get; set; }
        public string ip { get; set; }
        public string message { get; set; }
    }
}