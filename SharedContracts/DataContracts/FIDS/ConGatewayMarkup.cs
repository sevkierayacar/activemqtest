// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGatewayMarkup
    {
        public int enable { get; set; }
        public string source { get; set; }
        public string symbol { get; set; }
        public string account_name { get; set; }
        public int account_id { get; set; }
        public int bid_markup { get; set; }
        public int ask_markup { get; set; }
    }
}