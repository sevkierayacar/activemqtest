// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class MarginLevel
    {
        public int login { get; set; }
        public string group { get; set; }
        public int leverage { get; set; }
        public int updated { get; set; }
        public double balance { get; set; }
        public double equity { get; set; }
        public int volume { get; set; }
        public double margin { get; set; }
        public double margin_free { get; set; }
        public double margin_level { get; set; }
        public int margin_type { get; set; }
        public int level_type { get; set; }
    }
}