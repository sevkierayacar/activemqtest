// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class PerformanceInfo
    {
        public DateTime ctm { get; set; }
        public short users { get; set; }
        public short cpu { get; set; }
        public int freemem { get; set; }
        public int network { get; set; }
        public int sockets { get; set; }
    }
}