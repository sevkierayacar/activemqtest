// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConBackup
    {
        public string fullbackup_path { get; set; }
        public int fullbackup_period { get; set; }
        public int fullbackup_store { get; set; }
        public DateTime fullbackup_lasttime { get; set; }
        public short fullbackup_shift { get; set; }
        public string external_path { get; set; }
        public int archive_period { get; set; }
        public int archive_store { get; set; }
        public DateTime archive_lasttime { get; set; }
        public string export_securities { get; set; }
        public string export_path { get; set; }
        public int export_period { get; set; }
        public DateTime export_lasttime { get; set; }
        public int watch_role { get; set; }
        public string watch_password { get; set; }
        public string watch_opposite { get; set; }
        public int watch_ip { get; set; }
        public char archive_shift { get; set; }
        public char watch_state { get; set; }
        public char watch_failover { get; set; }
        public byte watch_timeout { get; set; }
        public int watch_login { get; set; }
        public DateTime watch_timestamp { get; set; }
    }
}