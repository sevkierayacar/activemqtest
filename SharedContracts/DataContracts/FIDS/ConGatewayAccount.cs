// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGatewayAccount
    {
        public int enable { get; set; }
        public string name { get; set; }
        public int id { get; set; }
        public int type { get; set; }
        public int login { get; set; }
        public string address { get; set; }
        public string password { get; set; }
        public int notify_logins { get; set; }
        public int flags { get; set; }
    }
}