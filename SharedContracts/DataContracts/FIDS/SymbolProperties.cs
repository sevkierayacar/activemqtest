// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class SymbolProperties
    {
        public string symbol { get; set; }
        public uint color { get; set; }
        public int spread { get; set; }
        public int spread_balance { get; set; }
        public int stops_level { get; set; }
        public int smoothing { get; set; }
        public int exemode { get; set; }
    }
}