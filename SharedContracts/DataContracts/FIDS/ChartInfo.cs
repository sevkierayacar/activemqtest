// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ChartInfo
    {
        public string symbol { get; set; }
        public int period { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public DateTime timesign { get; set; }
        public int mode { get; set; }
    }
}