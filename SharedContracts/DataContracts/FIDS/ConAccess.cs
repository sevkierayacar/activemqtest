// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConAccess
    {
        public int action { get; set; }
        public uint from { get; set; }
        public uint to { get; set; }
        public string comment { get; set; }
    }
}