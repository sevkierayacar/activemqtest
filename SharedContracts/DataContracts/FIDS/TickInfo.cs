// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TickInfo
    {
        public string symbol { get; set; }
        public DateTime ctm { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
    }
}