// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGroupMargin
    {
        public string symbol { get; set; }
        public double swap_long { get; set; }
        public double swap_short { get; set; }
        public double margin_divider { get; set; }
    }
}