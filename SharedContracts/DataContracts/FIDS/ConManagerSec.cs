// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConManagerSec
    {
        public int enable { get; set; }
        public int minimum_lots { get; set; }
        public int maximum_lots { get; set; }
    }
}