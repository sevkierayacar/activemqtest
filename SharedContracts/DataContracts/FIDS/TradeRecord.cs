// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TradeRecord
    {
        public int order { get; set; }
        public int login { get; set; }
        public string symbol { get; set; }
        public int digits { get; set; }
        public int cmd { get; set; }
        public int volume { get; set; }
        public DateTime open_time { get; set; }
        public int state { get; set; }
        public double open_price { get; set; }
        public double sl { get; set; }
        public double tp { get; set; }
        public DateTime close_time { get; set; }
        public int gw_volume { get; set; }
        public DateTime expiration { get; set; }
        public char reason { get; set; }
        public string conv_reserv { get; set; }
        public double conv_rates { get; set; }
        public double commission { get; set; }
        public double commission_agent { get; set; }
        public double storage { get; set; }
        public double close_price { get; set; }
        public double profit { get; set; }
        public double taxes { get; set; }
        public int magic { get; set; }
        public string comment { get; set; }
        public int gw_order { get; set; }
        public int activation { get; set; }
        public short gw_open_price { get; set; }
        public short gw_close_price { get; set; }
        public double margin_rate { get; set; }
        public DateTime timestamp { get; set; }
        public int api_data { get; set; }
    }
}