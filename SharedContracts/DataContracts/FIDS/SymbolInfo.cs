// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class SymbolInfo
    {
        public string symbol { get; set; }
        public int digits { get; set; }
        public int count { get; set; }
        public int visible { get; set; }
        public int type { get; set; }
        public double point { get; set; }
        public int spread { get; set; }
        public int spread_balance { get; set; }
        public int direction { get; set; }
        public int updateflag { get; set; }
        public DateTime lasttime { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double commission { get; set; }
        public int comm_type { get; set; }
    }
}