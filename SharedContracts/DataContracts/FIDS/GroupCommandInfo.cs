// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class GroupCommandInfo
    {
        public int len { get; set; }
        public char command { get; set; }
        public string newgroup { get; set; }
        public int leverage { get; set; }
    }
}