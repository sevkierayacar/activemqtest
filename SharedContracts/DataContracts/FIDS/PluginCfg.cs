// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class PluginCfg
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}