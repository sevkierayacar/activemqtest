// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGroupSec
    {
        public int show { get; set; }
        public int trade { get; set; }
        public int execution { get; set; }
        public double comm_base { get; set; }
        public int comm_type { get; set; }
        public int comm_lots { get; set; }
        public double comm_agent { get; set; }
        public int comm_agent_type { get; set; }
        public int spread_diff { get; set; }
        public int lot_min { get; set; }
        public int lot_max { get; set; }
        public int lot_step { get; set; }
        public int ie_deviation { get; set; }
        public int confirmation { get; set; }
        public int trade_rights { get; set; }
        public int ie_quick_mode { get; set; }
        public int autocloseout_mode { get; set; }
        public double comm_tax { get; set; }
        public int comm_agent_lots { get; set; }
        public int freemargin_mode { get; set; }
    }
}