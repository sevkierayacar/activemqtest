// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConFeeder
    {
        public string name { get; set; }
        public string file { get; set; }
        public string server { get; set; }
        public string login { get; set; }
        public string pass { get; set; }
        public string keywords { get; set; }
        public int enable { get; set; }
        public int mode { get; set; }
        public int timeout { get; set; }
        public int timeout_reconnect { get; set; }
        public int timeout_sleep { get; set; }
        public int attemps_sleep { get; set; }
        public int news_langid { get; set; }
    }
}