// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ExposureValue
    {
        public string currency { get; set; }
        public double clients { get; set; }
        public double coverage { get; set; }
    }
}