// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConDataServer
    {
        public string server { get; set; }
        public uint ip { get; set; }
        public string description { get; set; }
        public int isproxy { get; set; }
        public int priority { get; set; }
        public uint loading { get; set; }
        public uint ip_internal { get; set; }
    }
}