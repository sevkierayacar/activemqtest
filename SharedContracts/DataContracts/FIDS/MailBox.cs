// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class MailBox
    {
        public DateTime time { get; set; }
        public int sender { get; set; }
        public string from { get; set; }
        public int to { get; set; }
        public string subject { get; set; }
        public int readed { get; set; }
        public int bodylen { get; set; }
        public short build_min { get; set; }
        public short build_max { get; set; }
    }
}