// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TickRequest
    {
        public string symbol { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public char flags { get; set; }
    }
}