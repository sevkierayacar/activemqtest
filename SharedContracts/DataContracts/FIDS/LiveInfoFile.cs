// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class LiveInfoFile
    {
        public string file { get; set; }
        public int size { get; set; }
        public string hash { get; set; }
    }
}