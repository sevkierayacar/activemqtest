// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConGatewayRule
    {
        public int enable { get; set; }
        public string name { get; set; }
        public string request_symbol { get; set; }
        public string request_group { get; set; }
        public int request_reserved { get; set; }
        public string exe_account_name { get; set; }
        public int exe_account_id { get; set; }
        public int exe_max_deviation { get; set; }
        public int exe_max_profit_slippage { get; set; }
        public int exe_max_profit_slippage_lots { get; set; }
        public int exe_max_losing_slippage { get; set; }
        public int exe_max_losing_slippage_lots { get; set; }
        public int exe_account_pos { get; set; }
        public int exe_volume_percent { get; set; }
        public int exe_reserved { get; set; }
    }
}