// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class TradeTransInfo
    {
        public byte type { get; set; }
        public char flags { get; set; }
        public short cmd { get; set; }
        public int order { get; set; }
        public int orderby { get; set; }
        public string symbol { get; set; }
        public int volume { get; set; }
        public double price { get; set; }
        public double sl { get; set; }
        public double tp { get; set; }
        public int ie_deviation { get; set; }
        public string comment { get; set; }
        public DateTime expiration { get; set; }
        public int crc { get; set; }
    }
}