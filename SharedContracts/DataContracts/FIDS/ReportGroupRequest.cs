// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ReportGroupRequest
    {
        public string name { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public int total { get; set; }
    }
}