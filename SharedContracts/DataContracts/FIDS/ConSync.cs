// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConSync
    {
        public string server { get; set; }
        public int unusedport { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public int enable { get; set; }
        public int mode { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public string securities { get; set; }
    }
}