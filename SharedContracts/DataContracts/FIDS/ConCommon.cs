// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConCommon
    {
        public string owner { get; set; }
        public string name { get; set; }
        public uint address { get; set; }
        public int port { get; set; }
        public uint timeout { get; set; }
        public int typeofdemo { get; set; }
        public int timeofdemo { get; set; }
        public int daylightcorrection { get; set; }
        public int timezone_real { get; set; }
        public int timezone { get; set; }
        public string timesync { get; set; }
        public int minclient { get; set; }
        public int minapi { get; set; }
        public uint feeder_timeout { get; set; }
        public int keepemails { get; set; }
        public int endhour { get; set; }
        public int endminute { get; set; }
        public int optimization_time { get; set; }
        public int optimization_lasttime { get; set; }
        public int optimization_counter { get; set; }
        public int antiflood { get; set; }
        public int floodcontrol { get; set; }
        public int liveupdate_mode { get; set; }
        public int lastorder { get; set; }
        public int lastlogin { get; set; }
        public int lostlogin { get; set; }
        public int rollovers_mode { get; set; }
        public string path_database { get; set; }
        public string path_history { get; set; }
        public string path_log { get; set; }
        public DateTime overnight_last_day { get; set; }
        public DateTime overnight_last_time { get; set; }
        public DateTime overnight_prev_time { get; set; }
        public DateTime overmonth_last_month { get; set; }
        public string adapters { get; set; }
        public uint bind_adresses { get; set; }
        public short server_version { get; set; }
        public short server_build { get; set; }
        public uint web_adresses { get; set; }
        public int statement_mode { get; set; }
        public int monthly_state_mode { get; set; }
        public int keepticks { get; set; }
        public int statement_weekend { get; set; }
        public DateTime last_activate { get; set; }
        public DateTime stop_last { get; set; }
        public int stop_delay { get; set; }
        public int stop_reason { get; set; }
        public string account_url { get; set; }
    }
}