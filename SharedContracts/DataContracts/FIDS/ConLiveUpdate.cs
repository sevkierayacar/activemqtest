// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConLiveUpdate
    {
        public string company { get; set; }
        public string path { get; set; }
        public int version { get; set; }
        public int build { get; set; }
        public int maxconnect { get; set; }
        public int connections { get; set; }
        public int type { get; set; }
        public int enable { get; set; }
        public int totalfiles { get; set; }
        public LiveInfoFile files { get; set; }
    }
}