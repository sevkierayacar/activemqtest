// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class RequestInfo
    {
        public int id { get; set; }
        public char status { get; set; }
        public uint time { get; set; }
        public int manager { get; set; }
        public int login { get; set; }
        public string group { get; set; }
        public double balance { get; set; }
        public double credit { get; set; }
        public double prices { get; set; }
        public TradeTransInfo trade { get; set; }
        public int gw_volume { get; set; }
        public int gw_order { get; set; }
        public short gw_price { get; set; }
    }
}