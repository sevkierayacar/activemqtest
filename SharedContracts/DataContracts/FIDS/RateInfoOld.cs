// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class RateInfoOld
    {
        public DateTime ctm { get; set; }
        public int open { get; set; }
        public short high { get; set; }
        public short low { get; set; }
        public short close { get; set; }
        public double vol { get; set; }
    }
}