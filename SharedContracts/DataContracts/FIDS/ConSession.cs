// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConSession
    {
        public short open_hour { get; set; }
        public short open_min { get; set; }
        public short close_hour { get; set; }
        public short close_min { get; set; }
        public int open { get; set; }
        public int close { get; set; }
        public short align { get; set; }
    }
}