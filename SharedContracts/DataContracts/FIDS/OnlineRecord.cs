// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class OnlineRecord
    {
        public int counter { get; set; }
        public int login { get; set; }
        public uint ip { get; set; }
        public string group { get; set; }
    }
}