// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class SymbolSummary
    {
        public string symbol { get; set; }
        public int count { get; set; }
        public int digits { get; set; }
        public int type { get; set; }
        public int orders { get; set; }
        public long buylots { get; set; }
        public long selllots { get; set; }
        public double buyprice { get; set; }
        public double sellprice { get; set; }
        public double profit { get; set; }
        public int covorders { get; set; }
        public long covbuylots { get; set; }
        public long covselllots { get; set; }
        public double covbuyprice { get; set; }
        public double covsellprice { get; set; }
        public double covprofit { get; set; }
    }
}