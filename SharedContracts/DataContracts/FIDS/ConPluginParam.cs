// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class ConPluginParam
    {
        public ConPlugin plugin { get; set; }
        public int total { get; set; }
    }
}