// THIS DATA CONTRACT WAS AUTOMATICALLY GENERATED.
// DO NOT MODIFY!

using System;

namespace SharedContracts.DataContracts.FIDS
{
    public class NewsTopic
    {
        public uint key { get; set; }
        public DateTime time { get; set; }
        public string ctm { get; set; }
        public string topic { get; set; }
        public string category { get; set; }
        public string keywords { get; set; }
        public int bodylen { get; set; }
        public int readed { get; set; }
        public int priority { get; set; }
        public int langid { get; set; }
    }
}