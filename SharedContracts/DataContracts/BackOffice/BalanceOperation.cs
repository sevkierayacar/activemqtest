﻿using SharedContracts.Enums;
using System;

namespace SharedContracts.DataContracts.BackOffice
{
    public class BalanceOperation
    {
        public long Ticket { get; set; }
        public long Login { get; set; }
        public DateTime BalOpDateTime { get; set; }
        public DateTime BalOpDateTimeMt4 { get; set; }
        public EnBalanceOperationType BallanceOperationType { get; set; }
        public double Amount { get; set; }
        public double BaseCurrencyAmount { get; set; }
        public EnCurrency AmountCurrency { get; set; }
        public EnCurrency BaseCurrency { get; set; }
        public string Comment { get; set; }
        public string UserGroup { get; set; }
        public int TradingPlatform { get; set; }
        public DateTime SenderDateTime { get; set; }
        public byte[] CommentBytes { get; set; }
        public DateTime ExpireDateTime { get; set; }
        public DateTime ExpireDateTimeMt4 { get; set; }
        public string Guid { get; set; }
    }
}
