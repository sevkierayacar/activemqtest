﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.PrimeXM
{
    public class PrimeXMOrderClose
    {
        public long orderID { get; set; }
        public double fillSize { get; set; }
        public double fillPrice { get; set; }
        public string message { get; set; }
        public long milli { get; set; }
        public long micro { get; set; }
    }
}
