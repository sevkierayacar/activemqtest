﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.PrimeXM
{
    public class PrimeXMOrderOpen
    {
        public long orderID { get; set; }
        public string connector { get; set; }
        public string protocol { get; set; }
        public string account { get; set; }
        public string clOrdID { get; set; }
        public string subID1 { get; set; }
        public string subID2 { get; set; }
        public string subID3 { get; set; }
        public string side { get; set; }
        public string type { get; set; }
        public string instrument { get; set; }
        public string symbol { get; set; }
        public long digits { get; set; }
        public string currency { get; set; }
        public double size { get; set; }
        public double minsize { get; set; }
        public double price { get; set; }
        public double deviation { get; set; }
        public long ttl { get; set; }
        public double mid { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
        public string pool { get; set; }
        public string profile { get; set; }
        public double minspread { get; set; }
        public double maxspread { get; set; }
        public double markupbid { get; set; }
        public double markupask { get; set; }
        public long milli { get; set; }
        public long micro { get; set; }
    }
}
