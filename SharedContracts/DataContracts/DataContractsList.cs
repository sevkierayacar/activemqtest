﻿using System.Collections.Generic;

namespace SharedContracts.DataContracts
{
    public class DataContractsList
    {
        private List<object> _dataContracts = new List<object>();

        public IEnumerable<object> DataContracts
        {
            get { return _dataContracts; }
            set { _dataContracts = new List<object>(value); }
        }
    }
}
