﻿
using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.Instruments
{
    [Obsolete("This class will be removed soon")]
    public class Instrument
    {
        public string Symbol { get; set; }

        public string GroupName { get; set; }

        public int TradingPlatform { get; set; }

        public int Digits { get; set; }

        public int ContractSize { get; set; }

        public EnInstrumentType InstrumentType { get; set; }

        public string UnderlyingCcy { get; set; }
        
        public string Country { get; set; }
        
        public double TickSize { get; set; }
        
        public double TickValue { get; set; }

        public decimal? DefaultSpread { get; set; }
        
        public string ProfitCcy { get; set; }

        public double PriceMultiplier { get; set; }

        public string TradingPlatformInstrumentType { get; set; }

        public int? TradingPlatformInstrumentId { get; set; }

        public string Guid { get; set; }
    }
}
