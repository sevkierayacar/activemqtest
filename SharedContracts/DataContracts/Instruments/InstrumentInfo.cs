﻿using System;
using SharedContracts.Enums;
using System.Collections.Generic;
using SharedContracts.DataContracts.Ticks;

namespace SharedContracts.DataContracts.Instruments
{
    public class InstrumentInfo
    {
        public InstrumentInfo() { }

        public InstrumentInfo(int tradingPlatform,
            string symbol,
            EnInstrumentType instrumentType,
            int contractSize,
            string underlyingCcy,
            string country,
            double tickSize,
            double tickValue,
            int digits,
            string tradingPlatformInstrumentType,
            int tradingPlatformInstrumentId,
            EnSymbolMarginCalculation marginCalculation,
            double initialMargin,
            int limitStopLevel,
            double percentage,
            string marginCurrency,
            EnProfitCalculationType profitCalculation,
            string description,
            string source,
            long trade,
            long backgroundColor,
            long count,
            long countOriginal,
            long realTime,
            DateTime startingDateTime,
            DateTime expirationDateTime,
            ICollection<DailySession> quoteSessions,
            ICollection<DailySession> tradeSessions,
            long profitReserved,
            long filter,
            long filterCounter,
            double filterLimit,
            long filterSmoothing,
            double filterReserved,
            long logging,
            long spread,
            long spreadBalance,
            long exemode,
            long swapEnable,
            long swapType,
            double swapLong,
            double swapShort,
            long swapRollover3day,
            long gtcPendings,
            double marginMaintenance,
            double marginHedged,
            double point,
            double multiply,
            double bidTickvalue,
            double askTickvalue,
            long longOnly,
            long instantMaxVolumeset,
            long freezeLevel,
            long marginHedgedStrong,
            DateTime valueDateTime,
            long quotesDelay,
            long swapOpenprice,
            long swapVariationMargin,
            string guid)
        {
            TradingPlatform = tradingPlatform;
            Symbol = symbol;
            InstrumentType = instrumentType;
            ContractSize = contractSize;
            UnderlyingCcy = underlyingCcy;
            Country = country;
            TickSize = tickSize;
            TickValue = tickValue;
            Digits = digits;
            TradingPlatformInstrumentType = tradingPlatformInstrumentType;
            TradingPlatformInstrumentId = tradingPlatformInstrumentId;
            MarginCalculation = marginCalculation;
            InitialMargin = initialMargin;
            LimitStopLevel = limitStopLevel;
            Percentage = percentage;
            MarginCurrency = marginCurrency;
            ProfitCalculationType = profitCalculation;
            Description = description;
            Source = source;
            Trade = trade;
            BackgroundColor = backgroundColor;
            Count = count;
            CountOriginal = countOriginal;
            Realtime = realTime;
            StartingDateTime = startingDateTime;
            ExpirationDateTime = expirationDateTime;
            QuoteSessions = quoteSessions;
            TradeSessions = tradeSessions;
            ProfitReserved = profitReserved;
            Filter = filter;
            FilterCounter = filterCounter;
            FilterLimit = filterLimit;
            FilterSmoothing = filterSmoothing;
            FilterReserved = filterReserved;
            Logging = logging;
            Spread = spread;
            SpreadBalance = spreadBalance;
            Exemode = exemode;
            SwapEnable = swapEnable;
            SwapType = swapType;
            SwapLong = swapLong;
            SwapShort = swapShort;
            SwapRollover3Day = swapRollover3day;
            GtcPendings = gtcPendings;
            MarginMaintenance = marginMaintenance;
            MarginHedged = marginHedged;
            Point = point;
            Multiply = multiply;
            BidTickvalue = bidTickvalue;
            AskTickvalue = askTickvalue;
            LongOnly = longOnly;
            InstantMaxVolumeset = instantMaxVolumeset;
            FreezeLevel = freezeLevel;
            MarginHedgedStrong = marginHedgedStrong;
            ValueDateTime = valueDateTime;
            QuotesDelay = quotesDelay;
            SwapOpenprice = swapOpenprice;
            SwapVariationMargin = swapVariationMargin;
            Guid = guid;
        }

        public int TradingPlatform { get; set; }
        public string Symbol { get; set; }
        public EnInstrumentType InstrumentType { get; set; }
        public int ContractSize { get; set; }
        public string UnderlyingCcy { get; set; }
        public string Country { get; set; }
        public double TickSize { get; set; }
        public double TickValue { get; set; }
        public int Digits { get; set; }
        public string TradingPlatformInstrumentType { get; set; }
        public int TradingPlatformInstrumentId { get; set; }
        public EnSymbolMarginCalculation MarginCalculation { get; set; }
        public double InitialMargin { get; set; }
        public int LimitStopLevel { get; set; }
        public double Percentage { get; set; }
        public string MarginCurrency { get; set; }
        public EnProfitCalculationType ProfitCalculationType { get; set; }
        public string Description { get; set; }
        public string Source{ get; set; }
        public long Trade { get; set; }
        public long BackgroundColor { get; set; }
        public long Count { get; set; }
        public long CountOriginal { get; set; }
        public long Realtime { get; set; }
        public DateTime StartingDateTime { get; set; }
        public DateTime ExpirationDateTime { get; set; }
        public ICollection<DailySession> QuoteSessions { get; set; }
        public ICollection<DailySession> TradeSessions { get; set; }
        public long ProfitReserved { get; set; }
        public long Filter { get; set; }
        public long FilterCounter { get; set; }
        public double FilterLimit { get; set; }
        public long FilterSmoothing { get; set; }
        public double FilterReserved { get; set; }
        public long Logging { get; set; }
        public long Spread { get; set; }
        public long SpreadBalance { get; set; }
        public long Exemode { get; set; }
        public long SwapEnable{ get; set; }
        public long SwapType { get; set; }
        public double SwapLong { get; set; }
        public double SwapShort { get; set; }
        public long SwapRollover3Day { get; set; }
        public long GtcPendings { get; set; }
        public double MarginMaintenance{ get; set; }
        public double MarginHedged { get; set; }
        public double Point { get; set; }
        public double Multiply { get; set; }
        public double BidTickvalue { get; set; }
        public double AskTickvalue{ get; set; }
        public long LongOnly { get; set; }
        public long InstantMaxVolumeset { get; set; }
        public long FreezeLevel { get; set; }
        public long MarginHedgedStrong { get; set; }
        public DateTime ValueDateTime { get; set; }
        public long QuotesDelay { get; set; }
        public long SwapOpenprice { get; set; }
        public long SwapVariationMargin { get; set; }
        public string Guid { get; set; }

        public double GetFilterLimitInPercent() { return FilterLimit * 100; }
        public double GetInstantMaxVolumeInLot() { return InstantMaxVolumeset / 100; }
    }
}
