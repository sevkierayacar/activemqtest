﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.PartnersPortal
{
    public class Partner
    {
        public Partner() { }

        public Partner(long partnerId)
        {
            PartnerId = partnerId;
        }

        public long PartnerId { get; set; }
    }
}
