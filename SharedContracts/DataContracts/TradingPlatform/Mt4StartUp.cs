﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class Mt4StartUp
    {
        public Mt4StartUp() { }

        public Mt4StartUp(int tradingPlatform, DateTime date, string guid)
        {
            TradingPlatform = tradingPlatform;
            Date = date;
            Guid = guid;
        }

        public int TradingPlatform { get; set; }
        public DateTime Date { get; set; }
        public string Guid { get; set; }
    }
}
