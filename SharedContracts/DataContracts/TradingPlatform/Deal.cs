﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class Deal
    {
        public Deal() { }

        public Deal(int tradingPlatform, 
                    long ticket, 
                    long login,
                    DateTime timeStamp,
                    DateTime timeStampMt4,
                    string symbol, 
                    string userGroup, 
                    double volume, 
                    EnDealType dealType, 
                    double price, 
                    double profit, 
                    EnCurrency profitCurrency, 
                    double baseCurrencyProfit, 
                    EnCurrency baseCurrency, 
                    EnDealOperation dealOperation,
                    int magicNumber, 
                    string comment, 
                    double swap, 
                    double commission, 
                    double stopLoss, 
                    double takeProfit,
                    EnOrderReason orderReason,
                    string guid,
                    string dealId,
                    bool isFifo,
                    long closedByTicket)
        {
            TradingPlatform = tradingPlatform;
            Ticket = ticket;
            Login = login;
            TimeStamp = timeStamp;
            TimeStampMt4 = timeStampMt4;
            Symbol = symbol;
            UserGroup = userGroup;
            Volume = volume;
            DealType = dealType;
            Price = price;
            Profit = profit;
            ProfitCurrency = profitCurrency;
            BaseCurrencyProfit = baseCurrencyProfit;
            BaseCurrency = baseCurrency;
            DealOperation = dealOperation;
            MagicNumber = magicNumber;
            Comment = comment;
            Swap = swap;
            Commission = commission;
            StopLoss = stopLoss;
            TakeProfit = takeProfit;
            OrderReason = orderReason;
            Guid = guid;
            DealId = dealId;
            IsFifo = isFifo;
            ClosedByTicket = closedByTicket;
        }

        public string Symbol { get; set; }
        public long Ticket { get; set; }
        public double Volume { get; set; }
        public EnDealType DealType { get; set; }
        public double Price { get; set; }
        public double Profit { get; set; }
        public EnCurrency ProfitCurrency { get; set; }
        public double BaseCurrencyProfit { get; set; }
        public EnDealOperation DealOperation { get; set; }
        public int MagicNumber { get; set; }
        public string Comment { get; set; }
        public double Swap { get; set; }
        public double Commission { get; set; }
        public double StopLoss { get; set; }
        public double TakeProfit { get; set; }
        public int TradingPlatform { get; set; }
        public long Login { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime TimeStampMt4 { get; set; }
        public DateTime SenderDateTime { get; set; }
        public EnCurrency BaseCurrency { get; set; }
        public string UserGroup { get; set; }
        public double MarginRate { get; set; }
        public byte[] CommentBytes { get; set; }
        public double VolumeLots { get; set; }
        public EnOrderReason OrderReason { get; set; }
        public string Guid { get; set; }

        public string DealId { get; set; }

        public bool IsFifo { get; set; }

        public long ClosedByTicket { get; set; }
    }
}
