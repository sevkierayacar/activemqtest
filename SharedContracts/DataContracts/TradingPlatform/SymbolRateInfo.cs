﻿namespace SharedContracts.DataContracts.TradingPlatform
{
    public class SymbolRateInfo
    {
        public SymbolRateInfo() { }

        public SymbolRateInfo(string symbol, double rate)
        {
            Symbol = symbol;
            Rate = rate;
        }

        public string Symbol { get; set; }
        public double Rate { get; set; }
    }
}
