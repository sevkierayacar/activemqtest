﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class TradeAirSpot
    {
        public DateTime TimeStamp { get; set;}
        public string TradeReportId { get; set; }
        public string Symbol { get; set; }
        public string TradeId { get; set; }
        public double Price { get; set; }
        public double LastSpotPrice { get; set; }
        public PartyInfo TradingPartyInfo { get; set; } 
        public PartyInfo CounterPartyInfo { get; set; } 
    }
}
