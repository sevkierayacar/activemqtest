﻿using System.Collections.Generic;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class MarginCallUsersDetailed
    {
        public int TradingPlatform { get; set; }

        public ICollection<TradingPlatformUser> Logins { get; set; }

        public string Guid { get; set; }
    }
}