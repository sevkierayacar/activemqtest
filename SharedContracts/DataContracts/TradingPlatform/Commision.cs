﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class Commision
    {
        public Commision() { }

        public Commision(int tradingPlatform, 
                        long login, 
                        DateTime timeStamp,
                        DateTime timeStampMt4,
                        string userGroup, 
                        long ticket,
                        string dealId, 
                        EnCommisionType commisionType, 
                        bool isClosed, 
                        double amount, 
                        double baseCurrencyAmount, 
                        EnCurrency amountCurrency, 
                        EnCurrency baseCurrency, 
                        string symbol,
                        string guid)
        {

            TradingPlatform = tradingPlatform;
            Login = login;
            TimeStamp = timeStamp;
            TimeStampMt4 = timeStampMt4;
            UserGroup = userGroup;
            Ticket = ticket;
            DealId = dealId;
            CommisionType = commisionType;
            IsClosed = isClosed;
            Amount = amount;
            BaseCurrencyAmount = baseCurrencyAmount;
            AmountCurrency = amountCurrency;
            BaseCurrency = baseCurrency;
            Symbol = symbol;
            Guid = guid;
        }

        public long Ticket { get; set; }
        public string DealId { get; set; }
        public EnCommisionType CommisionType { get; set; }
        public bool IsClosed { get; set; }
        public double Amount { get; set; }
        public double BaseCurrencyAmount { get; set; }
        public EnCurrency AmountCurrency { get; set; }
        public string Symbol { get; set; }
        public EnCurrency BaseCurrency { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime TimeStampMt4 { get; set; }
        public long Login { get; set; }
        public string UserGroup { get; set; }
        public int TradingPlatform { get; set; }
        public DateTime SenderDateTime { get; set; }
        public string Guid { get; set; }
    }
}
