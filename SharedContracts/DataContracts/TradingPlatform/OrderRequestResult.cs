﻿using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class OrderRequestResult
    {
        public int TradingPlatform { get; set; }
        public long Login { get; set; }
        public long RequestId { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public long Ticket { get; set; }
        public string Guid { get; set; }
    }
}
