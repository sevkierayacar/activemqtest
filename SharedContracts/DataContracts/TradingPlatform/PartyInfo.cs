﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class PartyInfo
    {
        public EnDealOperation DealOperation { get; set; } 
        public string PartyId  { get; set; } 
        public string PartySubId  { get; set; } 
        public string OrderId  { get; set; } 
        public string ClientOrderId  { get; set; } 
        public long PartyRole  { get; set; } 
        public string PartyIdSource  { get; set; } 
        public double SettlCurAmount  { get; set; } 
        public string Currency  { get; set; } 
    }
}
