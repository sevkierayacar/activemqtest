﻿using SharedContracts.DataContracts.Instruments;
using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class Position
    {
        public string UserGroup { get; set; }
        public string Symbol { get; set; }
        public double Volume { get; set; }
        public double VolumeLots { get; set; }
        public EnPositionType PositionType { get; set; }
        public double Price { get; set; }
        public double Profit { get; set; }
        public double CurrentPrice { get; set; }
        public double Swap { get; set; }
        public double Commision { get; set; }
        public long? PositionId { get; set; }
        public bool IsClosed { get; set; }
        public DateTime? OpenTime { get; set; }
        public InstrumentInfo IntrumentInfo { get; set; }
        public long Login { get; set; }
        public int TradingPlatform { get; set; }
        public string Guid { get; set; }
    }
}
