﻿using System.Collections.Generic;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform.TradingGw
{
    public class TradingGroup
    {
        public int TradingPlatform { get; set; }
        public string Name { get; set; }
        public EnCurrency Currency { get; set; }
        public ICollection<SecuritySetting> SecuritySettings { get; set; }
        public int MarginCall { get; set; }
        public int MarginStopOut { get; set; }
        public EnMarginType MarginType { get; set; }
        public string Guid { get; set; }
        public int Leverage { get; set; }
        public int Enable { get; set; }
    }
}
