﻿using System.Collections.Generic;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform.TradingGw
{
    public class SecuritySetting 
    {
        public string Name { get; set; }
        public int Index { get; set; }
        public bool Enable { get; set; }
        public double CommBase { get; set; }
        public EnSecurityCommissionType CommType { get; set; }
        public EnSecurityCommissionLotMode CommLots { get; set; }
        public double CommAgent { get; set; }
        public EnSecurityCommissionType CommAgentType { get; set; }
        public EnSecurityCommissionLotMode CommAgentLots { get; set; }
        public int SpreadDifference { get; set; }
        public long TradingGroupId { get; set; }
        public double MinLots { get; set; }
        public double MaxLots { get; set; }
        public double LotsStep { get; set; }
        public IEnumerable<string> Symbols { get; set; } 
    }
}
