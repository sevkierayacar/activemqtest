﻿using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class BalanceChange
    {
        public int TradingPlatform { get; set; }
        public long Login { get; set; }
        public double CurrentBalance { get; set; }
        public long Ticket { get; set; }
        public double CurrentMargin { get; set; }
        public string Guid { get; set; }
        public double FreeMargin{ get; set; }
        public double Equity{ get; set; }
        public double Credit{ get; set; }
    }
}
