﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class SymbolSecurityGroup
    {
        public SymbolSecurityGroup() { }

        public SymbolSecurityGroup(int tradingPlatform, int index, string name, string description, string guid)
        {
            TradingPlatform = tradingPlatform;
            Index = index;
            Name = name;
            Description = description;
            Guid = guid;
        }

        public int TradingPlatform { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
    }
}