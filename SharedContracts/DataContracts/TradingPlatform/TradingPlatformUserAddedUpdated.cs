﻿namespace SharedContracts.DataContracts.TradingPlatform
{
    public class TradingPlatformUserAddedUpdated
    {
        public TradingPlatformUserAddedUpdated() { }
        public TradingPlatformUserAddedUpdated(TradingPlatformUser currState, TradingPlatformUser prevState)
        {
            CurrState = currState;
            PrevState = prevState;
        }

        public TradingPlatformUser CurrState { get; set; }
        public TradingPlatformUser PrevState { get; set; }
    }
}