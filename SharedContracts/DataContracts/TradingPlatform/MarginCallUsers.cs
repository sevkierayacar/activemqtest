﻿using System.Collections.Generic;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class MarginCallUsers
    {
        public int TradingPlatform { get; set; }

        public ICollection<long> Logins { get; set; }

        public string Guid { get; set; }
    }
}
