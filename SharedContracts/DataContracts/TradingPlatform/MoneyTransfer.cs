﻿using SharedContracts.Enums;
using System;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class MoneyTransfer
    {
        public long AccountId { get; set; }

        public int TradingPlatformType { get; set; }
        public int Login { get; set; }

        public int Ticket { get; set; }
        public decimal Amount { get; set; }

        public string Currency { get; set; }
        public DateTime Date { get; set; }

        public string Description { get; set; }
    }
}
