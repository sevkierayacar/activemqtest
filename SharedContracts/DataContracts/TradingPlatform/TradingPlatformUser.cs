﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class TradingPlatformUser
    {
        public TradingPlatformUser() { }

        public TradingPlatformUser(int tradingPlatform, long login, string group, string name,
            string email, string comment, string id, Int32 leverage, EnCurrency accountCcy, string country, string phone, string city, string state, string status, string zipCode, int lastIp, string address, DateTime regDate, DateTime regDateMt4, DateTime lastDate, Int32 enable, Int32 readonlyuser, long agent, string guid, DateTime senderDateTime)
        {
            TradingPlatform = tradingPlatform;
            Login = login;
            Group = group;
            Name = name;
            Email = email;
            Comment = comment;
            Id = id;
            Leverage = leverage;
            AccountCcy = accountCcy;
            Country = country;
            Phone = phone;
            City = city;
            State = state;
            Status = status;
            ZipCode = zipCode;
            LastIp = lastIp;
            Address = address;
            RegDate = regDate;
            RegDateMt4 = regDateMt4;
            LastDate = lastDate;
            Enable = enable;
            Readonly = readonlyuser;
            Agent = agent;
            Guid = guid;
            SenderDateTime = senderDateTime;
        }

        public int TradingPlatform { get; set; }
        public Int64 Login { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public string Id { get; set; }
        public int Leverage { get; set; }
        public EnCurrency AccountCcy { get; set; }
        public double Balance { get; set; }
        public double Credit { get; set; }
        public double Equity { get; set; }
        public double Margin { get; set; }
        public byte[] CommentBytes { get; set; }
        public string Status { get; set; }
        public int Enable { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int LastIp { get; set; }
        public string Address { get; set; }
        public string Guid { get; set; }
        public int Readonly { get; set; }
        public Int64 Agent { get; set; }        
        public DateTime RegDate { get; set; }
        public DateTime RegDateMt4 { get; set; }
        public DateTime LastDate { get; set; }


        public double FreeMargin { get { return Equity - Margin; } }
        public double MarginLevel { get { return Margin == 0 ? 0 : (Equity / Margin) * 100; }

        }
        public DateTime SenderDateTime { get; set; }
    }
}
