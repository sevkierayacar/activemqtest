﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class Swap
    {
        public string Symbol { get; set; }
        public EnCurrency BaseCurrency { get; set; }
        public bool IsClosed { get; set; }
        public double Amount { get; set; }
        public double AmountTotal { get; set; }
        public double BaseCurrencyAmount { get; set; }
        public double BaseCurrencyAmountTotal { get; set; }
        public EnCurrency AmountCurrency { get; set; }
        public long Ticket { get; set; }
        public string DealId { get; set; }
        public int TradingPlatform { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime TimeStampMt4 { get; set; }
        public long Login { get; set; }
        public string UserGroup { get; set; }
        public DateTime SenderDateTime { get; set; }
        public string Guid { get; set; }
    }
}
