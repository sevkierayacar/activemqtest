﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class PendingOrder
    {
        public int TradingPlatform { get; set; }
        public EnPendingOrderType PendingType { get; set; }
        public long Ticket { get; set; }
        public long Login { get; set; }
        public double Price { get; set; }
        public string Symbol { get; set; }
        public double Volume { get; set; }
        public double StopLoss { get; set; }
        public double TakeProfit { get; set; }
        public DateTime PendingCreation { get; set; }
        public DateTime PendingExpiration { get; set; }
        public string Comment { get; set; }
        public double VolumeLots { get; set; }
        public byte[] CommentBytes { get; set; }
        public string Guid { get; set; }
        public string UserGroup { get; set; }
        public DateTime PendingCreationMt4 { get; set; }
        public DateTime PendingExpirationMt4 { get; set; }
        public DateTime PendingClose { get; set; }
        public DateTime PendingCloseMt4{ get; set; }
    }
}
