﻿using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class LogError
    {
        public LogError() { }

        public LogError(int tradingPlatform, string log, string guid)
        {
            TradingPlatform = tradingPlatform;
            Log = log;
            Guid = guid;
        }

        public int TradingPlatform { get; set; }
        public string Log { get; set; }
        public string Guid { get; set; }
    }
}
