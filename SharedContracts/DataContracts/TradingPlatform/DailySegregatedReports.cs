﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class DailySegregatedReports
    {
        public int TradingPlatform { get; set; }

        public ICollection<DailySegregatedReport> Reports { get; set; }

        public string Guid { get; set; }
    }
}
