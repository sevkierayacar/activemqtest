﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class DailySegregatedReport
    {
        public DailySegregatedReport() {}
        
        public Int64 Login { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }
        public double Credit { get; set; }
        public double Commission { get; set; }
        public double Taxes { get; set; }
        public double Storage { get; set; }
        public double Profit { get; set; }
        public double Interest { get; set; }
        public double Tax { get; set; }
        public double PL { get; set; }
        public double Equity { get; set; }
    }
}