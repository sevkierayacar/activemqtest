﻿using SharedContracts.Enums;

namespace SharedContracts.DataContracts.TradingPlatform
{
    public class TradeApiData
    {
        public TradeApiData() { }

        public TradeApiData(int tradingPlatform, long login, long ticket, int apiData, string guid)
        {
            TradingPlatform = tradingPlatform;
            Login = login;
            Ticket = ticket;
            ApiData = apiData;
            Guid = guid;
        }

        public int TradingPlatform { get; set; }
        public long Login { get; set; }
        public long Ticket { get; set; }
        public int ApiData { get; set; }
        public string Guid { get; set; }
    }
}
