﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.Ticks
{
    public class Quote 
    {
        public Quote() { }

        public Quote(int tickSource, string symbol, long bid, long ask, DateTime timeStamp)
        {
            TickSource = tickSource;
            Symbol = symbol;
            Bid = bid;
            Ask = ask;
            TimeStamp = timeStamp;
        }

        public int TickSource { get; set; }
        public string Symbol { get; set; }
        public long Bid { get; set; }
        public long Ask { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
