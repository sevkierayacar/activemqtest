﻿using System.Collections.Generic;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.Ticks
{
    public class SymbolQuotesSessions 
    {
        public int TicksSource { get; set; }
        public string Symbol { get; set; }
        public string Guid { get; set; }
        public ICollection<DailySession> DailySessions { get; set; }
    }
}
