﻿using System;
using SharedContracts.Enums;

namespace SharedContracts.DataContracts.Ticks
{
    public class TickChartCandle
    {
        public int TradingPlatform { get; set; }
        public string Symbol { get; set; }
        public DateTime Date { get; set; }
        public int TotalIndex { get; set; }
        public int CurIndex { get; set; }
        public double OpenPrice { get; set; }
        public double ClosePrice { get; set; }
        public double MinPrice { get; set; }
        public double MaxPrice { get; set; }
        public double Volume { get; set; }
        public bool IsClosed { get; set; }
        public string Guid { get; set; }
    }
}
