﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.Ticks
{
    public class MultiLevelQuote
    {
        public MultiLevelQuote() { }

        public MultiLevelQuote(string symbol, long bid, long ask, long bidVolume, long askVolume, long issuerID, string issuerName, long level, string action)
        {
            Symbol = symbol;
            Bid = bid;
            Ask = ask;
            BidVolume = bidVolume;
            AskVolume = askVolume;
            IssuerID = issuerID;
            IssuerName = issuerName;
            Level = level;
            Action = action;
        }

        public string Symbol { get; set; }
        public long Bid { get; set; }
        public long Ask { get; set; }
        public long BidVolume { get; set; }
        public long AskVolume { get; set; }
        public long IssuerID { get; set; }
        public string IssuerName { get; set; }
        public long Level { get; set; }
        public string Action { get; set; }
    }
}
