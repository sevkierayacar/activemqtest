﻿using System;

namespace SharedContracts.DataContracts.Ticks
{
    public class DailySession
    {
        public long SymbolQuotesSessionsId { get; set; }
        public SymbolQuotesSessions SymbolQuotesSessions { get; set; }
        public int OpenHour { get; set; }
        public int OpenMin { get; set; }
        public int CloseHour { get; set; }
        public int CloseMin { get; set; }
        public int? TimeZoneCorrection { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
    }
}