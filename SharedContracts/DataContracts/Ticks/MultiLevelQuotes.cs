﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedContracts.DataContracts.Ticks
{
    public class MultiLevelQuotes
    {
        public int TickSource { get; set; }
        public ICollection<MultiLevelQuote> Quotes { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
