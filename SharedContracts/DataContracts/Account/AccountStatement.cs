﻿using SharedContracts.DataContracts.TradingPlatform;
using SharedContracts.Enums;
using System;
using System.Collections.Generic;

namespace SharedContracts.DataContracts.Account
{
    public class AccountStatement
    {
        public long Id { get; set; }
        public int TradingPlatform { get; set; }
        public long Login { get; set; }
        public DateTime DateTime { get; set; }
        public EnCurrency AccountCurrency { get; set; }
        public string Group { get; set; }
        public double Balance { get; set; }
        public double Equity { get; set; }
        public double ClosedPnl { get; set; }
        public double FloatingPnl { get; set; }
        public double Credit { get; set; }
        public string Guid { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
    }
}
