﻿namespace SharedContracts.Enums
{
    public enum EnMarginType
    {
        MARGINTYPE_PERCENT = 0,
        MARGINTYPE_CURRENCY = 1
    }
}
