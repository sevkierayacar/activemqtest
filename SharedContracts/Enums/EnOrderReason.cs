﻿namespace SharedContracts.Enums
{
    public enum EnOrderReason
    {
        TR_REASON_CLIENT = 0,
        TR_REASON_EXPERT = 1,
        TR_REASON_DEALER = 2,
        TR_REASON_SIGNAL = 3,
        TR_REASON_GATEWAY = 4,
        TR_REASON_MOBILE = 5,
        TR_REASON_WEB = 6,
        TR_REASON_API = 7
    }
}
