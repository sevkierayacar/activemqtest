﻿namespace SharedContracts.Enums
{
    public enum EnSecurityCommissionType
    {
        COMM_TYPE_MONEY = 0,
        COMM_TYPE_PIPS = 1,
        COMM_TYPE_PERCENT = 2
    }
}
