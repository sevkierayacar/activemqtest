﻿using SharedContracts.Attributes;

namespace SharedContracts.Enums
{
    public enum EnTradingPlatform
    {
        [ContractMeta("MetaTrader 4 Main")]
        MT4_GKFX = 0,

        [ContractMeta("MetaTrader 4 IshikFX")]
        MT4_ISHIKFX = 1,

        [ContractMeta("MetaTrader 4 Asia")]
        MT4_ASIA = 2,

        [ContractMeta("MetaTrader 4 Spread Betting")]
        MT4_SB = 3,

        [ContractMeta("MetaTrader 4 Demo")]
        MT4_DEMO = 4,

        [ContractMeta("MetaTrader 4 Test")]
        MT4_TEST = 5,

        [ContractMeta("MetaTrader 4 Standart")]
        MT4_STD = 6,

        [ContractMeta("MetaTrader 4 Live 1")]
        MT4_LIVE1 = 7,

        [ContractMeta("MetaTrader 4 Demo 2")]
        MT4_DEMO2 = 8,

        [ContractMeta("MetaTrader 4 Spread Betting 2")]
        MT4_SB2 = 9,

        [ContractMeta("MetaTrader 4 Live 5")]
        MT4_LIVE5 = 10,

        [ContractMeta("MetaTrader 4 Kapital")]
        MT4_KAPITAL = 11,

        [ContractMeta("MetaTrader 4 Prime")]
        MT4_PRIME = 12,

        [ContractMeta("MetaTrader 4 Ishik demo")]
        MT4_ISIKDEMO = 13,

        [ContractMeta("Tradair")]
        TRADAIR = 100,

        [ContractMeta("PrimeXM Drop Copy")]
        PRIMEXMDROPCOPY = 200,
    }
}