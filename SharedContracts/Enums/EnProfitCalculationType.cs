﻿namespace SharedContracts.Enums
{
    public enum EnProfitCalculationType
    {
        Forex = 0,
        Cfds = 1,
        Futures = 2
    }
}
