﻿namespace SharedContracts.Enums
{
    public enum EnDealOperation
    {
        BUY = 0,
        SELL = 1
    }
}
