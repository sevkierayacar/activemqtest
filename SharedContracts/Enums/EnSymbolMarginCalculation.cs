﻿namespace SharedContracts.Enums
{
    public enum EnSymbolMarginCalculation
    {
        Forex = 0,
        Cfd = 1,
        Futures = 2,
        CfdIndex = 3,
        CfdLeverage = 4,
    }
}
