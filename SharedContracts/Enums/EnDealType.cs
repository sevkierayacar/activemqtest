﻿namespace SharedContracts.Enums
{
    public enum EnDealType
    {
        IN = 0,
        OUT = 1,
        AUTOIN = 3,
        AUTOOUT = 4,
        INOUT = 5,
        OUTBY = 6,
    }
}
