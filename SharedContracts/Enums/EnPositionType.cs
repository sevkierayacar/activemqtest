﻿namespace SharedContracts.Enums
{
    public enum EnPositionType
    {
        LONG = 0,
        SHORT = 1
    };
}
