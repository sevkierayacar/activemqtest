﻿
namespace SharedContracts.Enums
{
    public enum EnInstrumentType
    {
        Fx = 0,
        Commodity = 1,
        Index = 2,
        Equity = 3
    }
}
