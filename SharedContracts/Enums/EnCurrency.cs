﻿namespace SharedContracts.Enums
{
    public enum EnCurrency
    {
        EUR = 0,
        USD = 1,
        CHF = 2,
        GBP = 3,
        JPY = 4,
        TRY = 5,
        CAD = 6,
        DKK = 7,
        HKD = 8,
        MXN = 9,
        NOK = 10,
        NZD = 11,
        SEK = 12,
        SGD = 13,
        THB = 14,
        ZAR = 15,
        AUD = 16,
        CZK = 17,
        HUF = 18,
        PLN = 19,
        SAR = 20,
        INR = 21,
        CNY = 22,
        TWD = 23,
        SWE = 24,
        RUB = 25,
        CNH = 26,
        NGN = 27,
        RUR = 28
    }
}