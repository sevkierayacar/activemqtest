﻿namespace SharedContracts.Enums
{
    public enum EnSecurityCommissionLotMode
    {
        COMMISSION_PER_LOT = 0,
        COMMISSION_PER_DEAL = 1
    }
}
