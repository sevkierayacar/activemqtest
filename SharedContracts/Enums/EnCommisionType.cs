﻿namespace SharedContracts.Enums
{
    public enum EnCommisionType
    {
        HOUSE = 0,
        REBATE = 1
    }
}
