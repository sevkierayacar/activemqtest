﻿namespace SharedContracts.Enums
{
    public enum EnBalanceOperationType
    {
        DEPOSIT = 0,
        WITHDRAWAL = 1,
        CREDIT = 2,
        CHARGE = 3,
        CORRECTION = 4,
        BONUS = 5,
        COMMISSION = 6,
        COMMISSION_DAILY = 7,
        COMMISSION_MONTHLY = 8,
        AGENT_DAILY = 9,
        AGENT_MONTHLY = 10,
        INTERESTRATE = 11,
        DIVIDEND = 12,
        DIVIDEND_FRANKED = 13,
        TAX = 14,
    }
}
