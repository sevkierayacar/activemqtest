﻿using SharedContracts.Attributes;

namespace SharedContracts.Enums
{
    public enum EnPendingOrderType
    {
        [ContractMeta("Buy Limit")]
        PendingBuyLimit = 1,

        [ContractMeta("Sell Limit")]
        PendingSellLimit = 2,

        [ContractMeta("Buy Stop")]
        PendingBuyStop = 3,

        [ContractMeta("Sell Stop")]
        PendingSellStop = 4
    }
}
