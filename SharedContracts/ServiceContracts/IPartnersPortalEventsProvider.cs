﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedContracts.DataContracts.PartnersPortal;

namespace SharedContracts.ServiceContracts
{
    public interface IPartnersPortalEventsProvider
    {
        event EventHandler<Partner> OnPartnerAddedUpdated;
    }
}
