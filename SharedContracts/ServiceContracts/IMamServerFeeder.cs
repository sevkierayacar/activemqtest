﻿using System;
using SharedContracts.DataContracts.MasterAccountManager;

namespace SharedContracts.ServiceContracts
{
    public interface IMamServerFeeder
    {
        event EventHandler<MasterAccountManagerEvent> MamServerEvent;
    }
}
