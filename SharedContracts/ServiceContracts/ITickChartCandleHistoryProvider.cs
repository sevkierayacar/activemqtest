﻿using System;
using SharedContracts.DataContracts;

namespace SharedContracts.ServiceContracts
{
    public interface ITickChartCandleHistoryProvider
    {
        DataContractsList GetTickChartHistoryRequestsProcessor(string period, int nLast, string symbol);
    }
}
