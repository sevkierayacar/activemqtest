﻿using System;
using SharedContracts.DataContracts;
using SharedContracts.DataContracts.Instruments;

namespace SharedContracts.ServiceContracts
{
    public interface ITradingPlatformDataProvider
    {
        DataContractsList GetAllInstrumentInfoes();
        InstrumentInfo GetInstrumentInfo(string instrument);
        DataContractsList GetHistoricalTradingEvents(DateTime date);
        DataContractsList GetHistoricalTradingEventsFromTo(DateTime from, DateTime to);
        DataContractsList GetTradingGroups();
        DataContractsList GetSymbolsQuotesSessions();
        DataContractsList GetHistoricalBalanceOperations(DateTime fromDate, DateTime toDate);
        long GetServerTimeShift();
        DataContractsList GetSymbolSecurities();       
        DataContractsList GetHistoricalOrder(long orderID);
    }
}
