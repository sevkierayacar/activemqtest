﻿using System;
using SharedContracts.DataContracts;

namespace SharedContracts.ServiceContracts
{
    public interface IBalanceOperations
    {
        DataContractsList CreateBalanceOperation(int balanceOperationType, Int64 login, double amount, string comment);
        DataContractsList CreateBalanceOperationWithoutCredit(int balanceOperationType, Int64 login, double amount, string comment);
    }
}
