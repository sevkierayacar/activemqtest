﻿using System;
using SharedContracts.DataContracts;

namespace SharedContracts.ServiceContracts
{
    public interface IExchRatesSnapshotProvider
    {
        DataContractsList GetHistoricalExchRates(DateTime atDateTime);
        DataContractsList GetLatestExchRates();
    }
}
