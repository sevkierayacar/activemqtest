﻿using SharedContracts.DataContracts;
using SharedContracts.DataContracts.TradingPlatform;

namespace SharedContracts.ServiceContracts
{
    public interface ITradingPlatformUserDetailsProvider
    {
        TradingPlatformUser UserLogin(long login, string password);
        TradingPlatformUser GetUserDetails(long login);
        DataContractsList GetAllUserDetails();
    }
}
