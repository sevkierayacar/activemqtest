﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedContracts.DataContracts.PrimeXM;

namespace SharedContracts.ServiceContracts
{
    public interface IPrimeXMEventsProvider
    {
        event EventHandler<PrimeXMOrderOpen> OnPrimeXMOrderOpen;
        event EventHandler<PrimeXMOrderClose> OnPrimeXMOrderClose;
    }
}
