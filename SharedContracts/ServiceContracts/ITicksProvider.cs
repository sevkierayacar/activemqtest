﻿using System;
using SharedContracts.DataContracts.Ticks;

namespace SharedContracts.ServiceContracts
{
    public interface ITicksProvider
    {
        event EventHandler<Quote> OnQuote;
        event EventHandler<MultiLevelQuotes> OnMultiLevelQuotes;
    }
}
