﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SharedContracts.DataContracts.Instruments;

namespace SharedContracts.ServiceContracts
{
    [Obsolete("This class will be removed soon")]
    public interface IInstrumentProvider
    {
        Task<IList<Instrument>> Get();
    }
}
