﻿using System;
using SharedContracts.DataContracts;
using SharedContracts.DataContracts.TradingPlatform;

namespace SharedContracts.ServiceContracts
{
    public interface IWebRegistrationProvider
    {
        DataContractsList CreateUserAccount(string group, string email, string password, string name,
            int leverage, long agent, bool read_only, bool sendReports, bool enable,
            bool enableChangePassword, long userColor, string passInvestor, string country,
            string state, string city, string address, string comment, string phone,
            string passPhone, string zipcode, string id);
        TradingPlatformUser UpdateUserAccount(long login, string group, string email, string password, string name,
            int leverage, long agent, int read_only, int sendreports, int enable,
            int enableChangePassword, long userColor, string passInvestor, string country,
            string state, string city, string address, string comment, string phone,
            string passPhone, string zipcode, string id);
        DataContractsList CreateBalanceOperation(int opType, long login, double amount, string comment);
        DataContractsList CreateBalanceOperationWithoutCredit(int opType, long login, double amount, string comment);
        DataContractsList AddBonus(long login, int opType, double amount, DateTime expiration, string comment);
        DataContractsList GetUsersFreeMargin(long login, string password);
        DataContractsList GetUsersFreeMarginWithoutCredit(long login, string password);
    }
}
