﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedContracts.DataContracts.TradingPlatform;

namespace SharedContracts.ServiceContracts
{
    public interface ITradeAirEventsProvider
    {
        event EventHandler<TradeAirSpot> OnTradeAirSpot;
    }
}
