﻿using System.Collections.Generic;
using SharedContracts.DataContracts;
using SharedContracts.DataContracts.MultiAccountManager;

namespace SharedContracts.ServiceContracts
{
    public interface IMultiAccountManagerProvider
    {
        string IsLoginValid(string text);
        MasterAccount QueryMaster(long login, bool includeSlaves);
        IList<MasterTrade> QueryTrades(long login);
    }
}
