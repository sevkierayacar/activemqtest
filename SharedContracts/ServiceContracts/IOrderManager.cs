﻿using SharedContracts.DataContracts;
using SharedContracts.DataContracts.TradingPlatform;

namespace SharedContracts.ServiceContracts
{
    public interface IOrderManager
    {
        long OrderOpen(long login, int dealOperation, string symbol, double openPrice, double volume, string comment);
        long OrderClose(long orderId, long login, double volume, double closePrice);
        DataContractsList OrderUserOrders(long login);
        Deal OrderGetStatus(long orderId);
        long OrderChangeStatus(long orderId, double price, double stopLoss, double takeProfit, string comment);

        DataContractsList GetPendingOrders(long login);
        long AddPendingOrder(long login, int pendingOrderType, string symbol, double price, double volume, string comment);
        long DeletePendingOrder(long orderId, long login);
    }
}
