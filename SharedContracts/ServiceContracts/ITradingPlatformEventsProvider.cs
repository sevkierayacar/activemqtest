﻿using SharedContracts.DataContracts.Account;
using SharedContracts.DataContracts.BackOffice;
using SharedContracts.DataContracts.Instruments;
using SharedContracts.DataContracts.Ticks;
using SharedContracts.DataContracts.TradingPlatform;
using SharedContracts.DataContracts.TradingPlatform.TradingGw;
using System;

namespace SharedContracts.ServiceContracts
{
    public interface ITradingPlatformEventsProvider
    {
        event EventHandler<Deal> OnDeal;
        event EventHandler<Deal> OnDealUpdate;
        event EventHandler<Deal> OnDealDelete;

        event EventHandler<Swap> OnSwap;
        event EventHandler<Swap> OnSwapUpdate;
        event EventHandler<Commision> OnCommision;
        event EventHandler<Commision> OnCommisionUpdate;
        event EventHandler<AccountStatement> OnAccountStatement;
        event EventHandler<InstrumentInfo> OnInstrumentInfo;
        event EventHandler<InstrumentInfo> OnInstrumentInfoDeleted;
        event EventHandler<TradingPlatformUserAddedUpdated> OnUserAddedUpdated;
        event EventHandler<TradingPlatformUser> OnUserDeleted;
        event EventHandler<TradingGroup> OnGroupUpdated;
        event EventHandler<TradingGroup> OnGroupDeleted;
        event EventHandler<SymbolQuotesSessions> OnSymbolQuotesSessions;
        event EventHandler<BalanceOperation> OnBalanceOperation;
        event EventHandler<BalanceOperation> OnBalanceUpdate;
        event EventHandler<BalanceOperation> OnBalanceDelete;
        event EventHandler<LogError> OnLogError;
        event EventHandler<TradeApiData> OnTradeApiData;
        event EventHandler<OrderRequestResult> OnOrderRequestExecution;
        event EventHandler<BalanceChange> OnBalanceChange;
        event EventHandler<MarginCallUsers> OnMarginCallUsers;
        event EventHandler<MarginCallUsersDetailed> OnMarginCallUsersDetailed;
        event EventHandler<Mt4StartUp> OnMt4StartUp; 

        event EventHandler<PendingOrder> OnPendingOrderAdd;
        event EventHandler<PendingOrder> OnPendingOrderUpdate;
        event EventHandler<PendingOrder> OnPendingOrderClose;
        event EventHandler<PendingOrder> OnPendingOrderDelete;

        event EventHandler<SymbolSecurityGroup> OnSymbolSecurityGroupUpdate;

        event EventHandler<TradingPlatformUser> OnUserLogin;
        event EventHandler<TradingPlatformUser> OnUserLogout;
        event EventHandler<Deal> OnDealReopened;
        event EventHandler<DailySegregatedReports> OnDailySegregatedReportsGenerated;
    }
}
