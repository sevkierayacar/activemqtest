﻿using SharedContracts.CentralizedDB.DataContracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace SharedContracts.CentralizedDB.ServiceContracts
{
    public interface ICentralizedStorageDefaultDataProvider
    {
        [Obsolete("This is going to be deleted soon. Please use GetDealsForPeriodWithFilter with empty filter.")]
        Task<IEnumerable<Deal>> GetDealsForPeriod(DateTime from, DateTime to, long take, long skip);
        [Obsolete("This is going to be deleted soon. Please use GetDealsForPeriodWithFilter with using Platform filter.")]
        Task<IEnumerable<Deal>> GetDealsForPeriod(long tradingPlatformType, DateTime from, DateTime to, long take, long skip);
        [Obsolete("This is going to be deleted soon. Please use GetSwapsForPeriodWithFilter with empty filter.")]
        Task<IEnumerable<Swap>> GetSwapsForPeriod(DateTime from, DateTime to, long take, long skip);
        [Obsolete("This is going to be deleted soon. Please use GetCommissionsForPeriodWithFilter with empty filter.")]
        Task<IEnumerable<Commission>> GetCommissionsForPeriod(DateTime from, DateTime to, long take, long skip);
        Task<IEnumerable<Deal>> GetDealsFromOpenedPositionsWithinPeriod(List<CentralizedFilter> dealFilterList, DateTime from, DateTime to, long take, long skip);
        Task<IEnumerable<Sentiment>> GetSentiments();
        [Obsolete("This is going to be deleted soon. Please use GetMoneyTransfersWithinPeriodWithFilter with empty filter.")]
        Task<IEnumerable<MoneyTransfer>> GetMoneyTransfersWithinPeriod(DateTime from, DateTime to);
        [Obsolete("This is going to be deleted soon. Please use GetMoneyTransfersWithinPeriodWithFilter with TradingPlatformType filter.")]
        Task<IEnumerable<MoneyTransfer>> GetMoneyTransfersWithinPeriod(long tradingPlatformType, DateTime from, DateTime to, long take, long skip);
        
        Task<IEnumerable<Swap>> GetSwapsForPeriodWithFilter(List<CentralizedFilter> centralizedFilterList, DateTime from, DateTime to, long take, long skip);
        Task<IEnumerable<Commission>> GetCommissionsForPeriodWithFilter(List<CentralizedFilter> centralizedFilterList, DateTime from, DateTime to, long take, long skip);
        Task<IEnumerable<MoneyTransfer>> GetMoneyTransfersWithinPeriodWithFilter(List<CentralizedFilter> centralizedFilterList, DateTime from, DateTime to, long take, long skip);
        Task<IEnumerable<Deal>> GetDealsForPeriodWithFilter(List<CentralizedFilter> dealFilterList, DateTime from, DateTime to, long take, long skip);

        Task<SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser> GetAccount(long login, long tradingPlatformType);
        Task<IEnumerable<SharedContracts.DataContracts.TradingPlatform.TradingPlatformUser>> GetAllAccounts(long take, long skip);

        Task<IEnumerable<Position>> GetPositionsWithinPeriod(CentralizedFilterExpression filterExpression, DateTime from, DateTime to, long take, long skip);

        Task<List<Reconciliation>> GetReconciliationResults(DateTime from, DateTime to);

    }
}
