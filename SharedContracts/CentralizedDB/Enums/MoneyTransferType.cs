﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum MoneyTransferType
    {
        Deposit = 0,
        Withdrawal = 1,
        Credit = 2
    }
}
