﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum CommissionType
    {
        House = 0,
        Rebate = 1
    }
}
