﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum DealType
    {
        IN = 0,
        OUT = 1,
        AUTOIN = 3,
        AUTOOUT = 4
    }
}
