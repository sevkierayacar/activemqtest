﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum AccountType
    {
        Client,
        Stp,
        Hedge,
        Test
    }
}
