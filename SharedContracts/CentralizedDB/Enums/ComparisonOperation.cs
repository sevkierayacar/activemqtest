﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum ComparisonOperation
    {
        Equals = 1,
        NotEqual = 2,
        GreaterOrEqual = 3,
        LessOrEqual = 4,
        GreaterThan = 5,
        LessThan = 6,
        Contains = 7,
        DoNotContain = 8,
        StartsWith = 9,
        EndsWith = 10,
        In = 11,
        NotIn = 12,
        ContainsAny = 13,
        NotContainsAny = 14,
        NotStartsWith = 15,
        NotEndsWith = 16,
    }
}
