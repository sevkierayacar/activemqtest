﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum DealOperationType
    {
        Buy = 0,
        Sell = 1
    }
}
