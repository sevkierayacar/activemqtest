﻿
namespace SharedContracts.CentralizedDB.Enums
{
    public enum InstrumentType
    {
        Fx = 0,
        Commodity = 1,
        Index = 2,
        Equity = 3,
    }
}
