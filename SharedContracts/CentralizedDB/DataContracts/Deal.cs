﻿using System;
using SharedContracts.CentralizedDB.Enums;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Deal
    {
        public int TradingPlatformType { get; set; }

        public int Login { get; set; }

        public int Ticket { get; set; }

        public DealType Type { get; set; }
        public DealOperationType OperationType { get; set; }

        public decimal Volume { get; set; }
        public decimal Price { get; set; }
        public DateTime Date { get; set; }
        public DateTime? EventDate { get; set; }

        public string Description { get; set; }

        public bool IsClosed { get; set; }

        public decimal ProfitLossAmount { get; set; }
        public string ProfitLossCurrency { get; set; }

        public decimal ProfitLossInBaseAmount { get; set; }
        public string ProfitLossInBaseCurrency { get; set; }

        public string Symbol { get; set; }
        public string UserGroup { get; set; }
    }

}
