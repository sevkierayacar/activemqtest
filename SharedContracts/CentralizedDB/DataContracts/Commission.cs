﻿using SharedContracts.CentralizedDB.Enums;
using System;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Commission
    {

        public bool IsClosed { get; set; }
        public decimal Amount { get; set; }
        public string  AmountCurrency { get; set; }
        
        public decimal BaseCurrencyAmount { get; set; }
        public string BaseCurrency { get; set; }
        
        public DateTime Date { get; set; }
        public CommissionType Type { get; set; }

        public int TradingPlatformType { get; set; }

        public string Symbol { get; set; }
        public int Login { get; set; }
        public string UserGroup { get; set; }

        public int Ticket { get; set; }
        public long DealId { get; set; }
        public string Guid { get; set; }
    }
}
