﻿using SharedContracts.CentralizedDB.Enums;
using System;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Sentiment
    {
        public string InstrumentGroupName { get; set; }
        public int NumberOfBuy { get; set; }
        public int NumberOfSell { get; set; }
        public decimal VolumeOfBuy { get; set; }
        public decimal VolumeOfSell { get; set; }
        public DateTime LastTradeDate { get; set; }
        public InstrumentType InstrumentType { get; set; }

    }
}
