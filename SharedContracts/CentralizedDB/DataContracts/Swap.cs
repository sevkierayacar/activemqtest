﻿using System;
using SharedContracts.CentralizedDB.Enums;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Swap
    {
        public bool IsClosed { get; set; }
        public decimal Amount { get; set; }
        public string AmountCurrency { get; set; }
        public decimal BaseCurrencyAmount { get; set; }
        public string  BaseCurrency { get; set; }

        public DateTime Date { get; set; }
        
        public string Symbol { get; set; }
        public int Login { get; set; }
        public string UserGroup { get; set; }

        public int Ticket { get; set; }

        public int TradingPlatformType { get; set; }
    }
}
