﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SharedContracts.CentralizedDB.DataContracts
{
    [ComplexType]
    public class ConversionRate
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal Rate { get; set; }
    }
}
