﻿using SharedContracts.CentralizedDB.Enums;
using System;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Position
    {
        public int TradingPlatformType { get; set; }
        public int Login { get; set; }

        public int Ticket { get; set; }

        public string Symbol { get; set; }
        public string UserGroup { get; set; }

        public DealType Type { get; set; }
        public DealOperationType OperationType { get; set; }

        public decimal OpenVolume { get; set; }
        public decimal? CloseVolume { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal? ClosePrice { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime? EventDate { get; set; }

        public string Description { get; set; }

        public bool IsClosed { get; set; }

        public decimal? ProfitLossAmount { get; set; }
        public string ProfitLossCurrency { get; set; }
        public decimal? ProfitLossInBaseAmount { get; set; }
        public string ProfitLossInBaseCurrency { get; set; }

        public decimal? Swap { get; set; }
        public decimal? Commission { get; set; }
    }
}
