﻿using System;
using SharedContracts.CentralizedDB.Enums;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class MoneyTransfer
    {
        public long AccountId { get; set; }

        public int TradingPlatformType { get; set; }

        public int Login { get; set; }

        public int Ticket { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public ConversionRate ToUsd { get; set; }
        public DateTime Date { get; set; }
        public MoneyTransferType Type { get; set; }

        public string Description { get; set; }
    }
}
