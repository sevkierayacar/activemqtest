﻿using System;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class Reconciliation
    {
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int? Ticket { get; set; }
        public int? TradingPlatformType { get; set; }

        public DateTime Date { get; set; }

        public ReconciliationType ReconciliationType { get; set; }
        public TradingEventType TradingEventType { get; set; }
    }
    public enum ReconciliationType
    {
        Daily = 0,
        Hourly = 1,
        Weekly = 2
    }

    public enum TradingEventType
    {
        Deal = 0,
        Swap = 1,
        Commission = 2,
        MoneyTransfer = 3
    }
}
