﻿using SharedContracts.CentralizedDB.Enums;
using System;
using System.Collections.Generic;
using System.Security.AccessControl;

namespace SharedContracts.CentralizedDB.DataContracts
{
    public class CentralizedFilter
    {
        public int Id { get; set; }
        public string FilterName { get; set; }
        public object FilterValue { get; set; }
        public string FilterExpression { get; set; }
        public Type FilterType { get; set; }
        public ComparisonOperation Comparison { get; set; }
    }

    public class CentralizedFilterExpression
    {
        public string Expression { get; set; }
        public List<CentralizedFilter> CentralizedFilters { get; set; }
    }
}
